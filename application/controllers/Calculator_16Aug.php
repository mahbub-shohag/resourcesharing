<?php
class Calculator extends CI_Controller
{
	public function __construct()
	 {
	     parent::__construct();
	     $this->load->model('Calculator_model');
	     //$this->load->library('Upload');
	 }

	/*Commun API Starts */
	 public function getlists(){
	    //echo "<pre>";
	    //print_r($_POST);
	    //$table = $_REQUEST['table']; 
	    $data = $this->Calculator_model->getlist($_POST['table']);
	    echo json_encode($data);
	}
 
 	/*Commun API Ends */


 	public function getCustomDropDownlists($dropdown_for=null){
	    //echo "<pre>";
	    //print_r($_SESSION);
	    //exit;
	    $user = $_SESSION['user_id']; 
	    $dropdown_for = $_POST['dropdown_for'];
	    $data = $this->Calculator_model->getCustomDropDownlists($user,$dropdown_for);
	    echo json_encode($data);
	}

	public function index()
	{
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->model('mship');

		$data['ports'] = $this->mship->port();
		$data['activity'] = $this->mship->load_activity();
		$data['cargo'] = $this->mship->load_cargo();

		$this->load->view('dashboard/calculator', $data);

	}


	public function ship_search()
	{

		$this->load->model('mship');

		if (isset($_GET['term'])) {			
			$result = $this->mship->ship_search($_GET['term']);
			if (count($result)>0) {
				foreach ($result as $sp )
					$arr_result[] = $sp->name;
				echo json_encode($arr_result);
			}
		}
		
	}


	public function ship_details()
	{

		$this->load->model('mship');

		if (isset($_POST['ship_name'])) {			
			$result = $this->mship->ship_details($_POST['ship_name']);

			echo json_encode($result);		
		
		}
		
	}	

	public function save_activity()
	{
		//echo $_POST['dropdown_for'];
		//exit;
		$user = $_SESSION['user_id'];
		$data = array(
			'dropdown_for'=>$_POST['dropdown_for'],
			'dropdown_text'=>$_POST['dropdown_text'],
			'created_by_user_id'=>$user,
		);		
		
		$this->db->insert('custom_dropdown_data', $data);
		$activitylists = $this->Calculator_model->getCustomDropDownlists($user,$_POST['dropdown_for']);
		echo json_encode($activitylists);			
	}

	public function save_dropdown()
	{		
		$user = $_SESSION['user_id'];
		$data = array(
			'dropdown_for'=>$_POST['dropdown_for'],
			'dropdown_text'=>$_POST['dropdown_text'],
			'created_by_user_id'=>$user,
		);		
		
		$this->db->insert('custom_dropdown_data', $data);
		$dropdowns = $this->Calculator_model->getCustomDropDownlists($user,$_POST['dropdown_for']);
		echo json_encode($dropdowns);		
	}

	public function save_description()
	{
		
		$data = array(
			'dropdown_for'=>$_POST['dropdown_for'],
			'dropdown_text'=>$_POST['dropdown_text'],
			'created_by_user_id'=>$_POST['created_by_user_id'],
		);		
		
		$this->db->insert('custom_dropdown_data', $data);

	}

	public function savepdf(){
		$data['ship_name'] = $_POST['info']['ship_name']['name'];
		$data['loa'] = $_POST['info']['loa'];
		$data['imo'] = $_POST['info']['imo'];
		$data['dwt'] = $_POST['info']['dwt'];
		$this->db->insert('ship_info', $data);
		echo json_encode($this->db->insert_id());
	}
/*	public function infopdf($id)
    {   
    	//echo "<pre>";
    	//print_r($id);
    	//exit;
    	$ship_info = $this->Calculator_model->get_by_id('ship_info',$id);
    	//print_r($ship_info);
    	//exit;
    	ob_start();
    	$data['ship_info'] = $ship_info[0];
        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('reports/invoice',$data,true);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
        ob_end_flush();
    }
*/

	public function infopdf($id)
    {   
    	//echo "<pre>";
    	//print_r($id);
    	//exit;
    	ob_start();
    	$ship_info = $this->Calculator_model->get_by_id('ship_info',$id);
    	$data['ship_info'] = $ship_info[0];
        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('reports/invoice',$data,true);
        $mpdf->WriteHTML($html);
        //$mpdf->Output('mahbub.pdf','F');
        ob_clean();
        $mpdf->Output('itinery/filename1.pdf','F');
    }



	

}