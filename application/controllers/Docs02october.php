<?php
class Docs extends CI_Controller
{
	public function __construct()
	 {
	     parent::__construct();
	     $this->load->model('DocsModel');
	     $this->load->library('Upload');
	     $this->load->helper('url'); 
		 $this->load->helper('html');
	 }

	 public function index(){
	 	$this->load->view('docs/files');
	 }

	 /*Commun API Starts */
	 public function getlists(){
	    //echo "<pre>";
	    //print_r($_POST);
	    //$table = $_REQUEST['table']; 
	    $data = $this->DocsModel->getlist($_POST['parent_id']);
	    echo json_encode($data);
	}
        
    public function get_by_id($table=null,$id=null){
	   $data = $this->DocsModel->get_by_id($_POST['table'],$_POST['id']);
	   echo json_encode($data);
	}
	 public function get_by_vessel_id($table=null,$id=null){
	   $data = $this->DocsModel->get_by_vessel_id($_POST['table'],$_POST['id']);
	   echo json_encode($data);
	}


	public function get_by_parent_id($parent_id){
		$data = $this->DocsModel->get_by_parent_id($parent_id);
	   	return $data;	
	}

	/*Commun API Ends */

	public function makeDirectory(){
		if (!file_exists($_POST['folder_path'])) {
    			mkdir($_POST['folder_path'], 0777, true);
			}
		$directory['parent_id'] = $_POST['parent_id'];;
		$directory['type_id'] = 8;
		$directory['type_name'] = "folder";
		$directory['name'] = $_POST['directory_name'];
		$directory['item_link'] = "/".$_POST['folder_path'];	
		$directory['created_by'] = "";
		$directory['created_date'] = "";
		if(isset($_POST['vessel_id'])){
			$directory['vessel_id'] = $_POST['vessel_id'];
		}
		$result = $this->DocsModel->add_directory($directory);	
		if($result){
			$files = $this->get_by_parent_id($directory['parent_id']);
        	echo json_encode($files);
		}else{
			echo 'Could Not Create Directory';
		}
	}

	function upload_original($foo,$folder)
	{
	    $folder = $folder;
	    if ($foo->uploaded) {
	        // save uploaded image with no changes
	        $foo->Process($folder);
	        //echo($images['image_path']);exit;
	        if ($foo->processed) {
	            return true;
	        } else {
	            return false;
	            //echo 'error : ' . $foo->error;
	        }
	    }
	}

	public function uploadFile(){
		$info = json_decode($_POST['info']);
		$file = new Upload($_FILES['file']);
		$data['parent_id'] = $info->parent_id;
	    $data['parent_directory'] = $info->directory;
	    $data['name'] = $file->file_src_name;
		$data['type_name'] = $file->file_src_name_ext;
		$data['item_link'] = "/".$info->directory.$file->file_src_name;
        $upload = $this->upload_original($file,$info->directory);
        if($upload){
        	$this->DocsModel->add_file($data);
        	$files = $this->get_by_parent_id($info->parent_id);
        	echo json_encode($files);
        }else{
        	echo 'Error';
        }
}
}	 