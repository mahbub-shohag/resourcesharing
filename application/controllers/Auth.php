<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth extends CI_Controller
{
	
	function __construct() {
        parent::__construct();
        $this->load->library('email');
    }

	

	public function confirmation()
	{
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->view('auth/confirmation');
	}

	public function reset_password_form()
	{
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->view('auth/reset_password');
	}

	public function signout()
	{
		//unset($_SESSION);
		$this->session->sess_destroy();
		redirect("auth/signin", "refresh");
	}

	public function confirm_reset_pass($reset_token){

		$_SESSION['reset_tkn']=$reset_token;
		 
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(array('reset_token' => $reset_token));

		$query = $this->db->get();
		$user= $query->row();
		
		if ($user)  {
			redirect("auth/reset_password_form", "refresh");
			//$this->load->view('auth/reset_password');
		}
		
		
	}

	public function confirm_reset_password(){
		 
		$reset_tkn=$_SESSION['reset_tkn'];
		$password1=$_POST['password1'];
		$password2=$_POST['password2'];
		if ($password1!=$password2) {
			$this->session->set_flashdata("error", "Password mismatched. Try again please.");
		    redirect("auth/reset_password_form", "refresh");
		}else{

		$password1=md5($_POST['password1']);	

		$this->db->set('password', $password1); //value that used to update column  
		$this->db->where('reset_token', $reset_tkn); //which row want to upgrade  
		$this->db->update('users');  //table name
		$this->session->set_flashdata("success", "Your password has been reset. Please signin <a href=\"signin\">here</a>.");

		redirect("auth/confirmation", "refresh");

		}
	
		
	 }	


	public function reset_password()
	{
		

		$reset_pass_email=$_POST['reset_pass_email'];
		$reset_token=md5($_POST['reset_pass_email']).rand();

		//check user name 
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where(array('email' => $reset_pass_email));
			$query = $this->db->get();
			$user= $query->row();
			$name= $user->name;

		

		$this->db->set('reset_token', $reset_token); //value that used to update column  
		$this->db->where('email', $reset_pass_email); //which row want to upgrade  
		$this->db->update('users');  //table name

		//email send
		$subject="Umipak – Reset your password";
		$message="Hello  ".$name." 

To reset your password, please click below link: 
http://khelafatandolon.org/chanul/auth/confirm_reset_pass/".$reset_token."

If you did not request a password reset, you can ignore it!. Your password remains the same and you can delete this email. 

Made by Umipak – Universal Marine Information and Processing Tank

";					
		$this->email->from('softrithmit@gmail.com', 'Chanul App');
	    $this->email->to($reset_pass_email);

	    $this->email->subject($subject);
	    $this->email->message($message);
		$this->email->send();


	}

	public function contact_form()
	{
		$this->load->helper('url'); 
		$this->load->helper('html');

		$this->form_validation->set_rules('text', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('text', 'Message', 'required');

		if(($_REQUEST['captcha'] == $_SESSION['vercode']))
	
		{

		$name=$_POST['name'];
		$email=$_POST['email'];
		$subject=$_POST['subject'];
		$message_content=$_POST['message'];



				
		$this->email->from($email, 'Chanul App Contact Form Submission');
	    $this->email->to('hafiz.softrithmit@gmail.com');

	    $this->email->subject($subject);
	   
	    $this->email->message($message_content);
		if ($this->email->send()) {
		 	echo "Your message has been sent. We will come back to you soon.";
		 } 
		 else{
		 	echo "Something went wrong!";
		 }

	}
	else{
		echo "Please try with correct capcha";
	}
}


public function demo_captcha()
	{
		
		function getRandomWord($len = 5) {
		    $word = array_merge(range('0', '9'), range('A', 'Z'));
		    shuffle($word);
		    return substr(implode($word), 0, $len);
		}

		$ranStr = getRandomWord();
		$_SESSION["vercode"] = $ranStr;


		$height = 35; //CAPTCHA image height
		$width = 150; //CAPTCHA image width
		$font_size = 24; 

		$image_p = imagecreate($width, $height);
		$graybg = imagecolorallocate($image_p, 245, 245, 245);
		$textcolor = imagecolorallocate($image_p, 34, 34, 34);

		imagefttext($image_p, $font_size, -2, 15, 26, $textcolor, 'fonts/mono.ttf', $ranStr);
		//imagestring($image_p, $font_size, 5, 3, $ranStr, $white);
		echo imagepng($image_p);


	}

	public function signin()
	{
		
		$this->load->helper('url'); 
		$this->load->helper('html');

		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
			
		if ($this->form_validation->run()== TRUE) {

			$email=$_POST['email'];
			$password=md5($_POST['password']);
			$email_confirmation='1';

			//check user email only in db
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where(array('email' => $email));
			$query1 = $this->db->get();
			$user_with_email= $query1->row();
			if ($user_with_email!=null)  {
				$wrong_pass=1;
			}

			//check user in db
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where(array('email' => $email, 'password'=>$password));

			$query = $this->db->get();
			$user= $query->row();

					// if user exist
			if ($user!=null)  {
				$status= $user->status;
				if ( $status==1)  {

				// set session variable
				$_SESSION['user_logged']= TRUE;
				$_SESSION['user']= $user->name;
				$_SESSION['user_id']= $user->id;
				$user_id=$_SESSION['user_id'];

				// redirect to profile page

				//redirect("dashboard/index", "refresh");
				
				//get ip
				$get_ip = $this->input->ip_address();
                //print_r($get_ip);
                $data = array(
					'ip'=>$get_ip,
					'user_id'=>$user_id,
				);		
				
				$this->db->insert('access_ip', $data);


				//check user in db
				$this->db->select('*');
				$this->db->from('subscribers');
				$this->db->where(array('user_id' => $user_id));

				$query = $this->db->get();
				$payment= $query->row();				


				if ($payment!=null)  {
					$_SESSION['subscribed']= 'yes';
					redirect("dashboard/index", "refresh");
				}
				else{
					$_SESSION['subscribed']= 'no';
					redirect("dashboard/index", "refresh");
					//redirect("dashboard", "refresh");
				}

				


			}
			else{
				$this->session->set_flashdata("error", "Your account is not active yet. Please check you email to verify email address.");
					redirect("auth/signin", "refresh");
			}
		}
			else{


				if (isset($wrong_pass)) {
					$this->session->set_flashdata("error", "Unfortunately that password is incorrect. Please try again or click <b>Forgot password</b> below to reset password.");
					redirect("auth/signin", "refresh");
				}

				//captcha 
				$attempt = $this->session->userdata('attempt');
                $attempt++;
                $this->session->set_userdata('attempt', $attempt);
                
                $get_ip=$this->input->ip_address();	


				if ($attempt>=5) {
					$this->session->set_flashdata("error", "No such account exists in database.");
					redirect("auth/signin?captcha", "refresh");
				}else{
					$this->session->set_flashdata("error", "No such account exists in database!");
					redirect("auth/signin", "refresh");
				}		

				
			}


			}
		
		
			$this->load->view('auth/signin');
	}

	

	public function signup()
	{
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->library('session');

		
		if (isset($_POST['password'])) {

			
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|min_length[6] |matches[password]');




			
			if ($this->form_validation->run()== TRUE) {
				
				$key=md5($_POST['email']).rand();

				$data = array(
					'package'=>$_POST['package'],
					'name'=>$_POST['name'],
					'company_name'=>$_POST['company_name'],
					'email'=>$_POST['email'],
					'password'=>md5($_POST['password']),
					'confirmation_link'=>$key,
					'created_date'=>date('Y-m-d'),
					// 'status'=>$status,
					// 'confirmation_link'=>$confirmation_link,
				);



				$this->db->select('*');
				$this->db->from('users');
				$query = $this->db->where('email',$data['email']);
				$query = $this->db->get();
				$countUser = $query->num_rows();
				if($countUser!=0){
					$this->session->set_flashdata("error", "We are sorry but it seems you have already registered in our system. Please try to sign in here with your username and password");
						redirect("auth/signin", "refresh");	
				}else{
					$this->db->insert('users', $data);
					$insert_id = $this->db->insert_id();
					// $this->session->set_flashdata("success", "A mail is sent to your email.Please verify the link to complete your signup process");
					// redirect("auth/confirmation", "refresh");	
				
				
				    
					
			       if(isset($_POST['email'])){
 
					$email=$_POST['email']; 		
					
					$message="Hello  ".$_POST['name']."

Thank you for starting your signing up process with Umipak!. Please click on the link below to continue your sign up process.
http://khelafatandolon.org/chanul/auth/confirmSignUp/".$key."

Thank you, 
Umipak team
";				


					$subject = "From Umipak – Please confirm your email";
					
					$this->email->from('hafiz.softrithmit@gmail.com', 'Chanul App');
				    $this->email->to($email);

				    $this->email->subject($subject);
				    $this->email->message($message);
				    $this->email->send();	

                    //echo $this->email->send(); exit();	
					// if($this->email->send())
					//     {
							$this->session->set_flashdata("success", "A mail is sent to your email.Please verify the link to complete your signup process.");
			
				          // redirect("auth/signup", "refresh");
				           redirect("auth/confirmation", "refresh");

					   // }

		  			 // else
		  				// {
		    		// 		$this->session->set_flashdata("error", "Something went wrong.");
		   			// 	}


				}
			 }		

			}
			// not validate_form

			
		}

		$this->load->model('auth_model');

		$data['packages'] = $this->auth_model->packages();

		$this->load->view('auth/signup', $data);
	}


	public function confirmSignUp($key){
				 
		$this->db->set('status', 1); //value that used to update column  
		$this->db->where('confirmation_link', $key); //which row want to upgrade  
		$this->db->update('users');  //table name





		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(array('confirmation_link' => $key));
		$query = $this->db->get();
		$user= $query->row();

		$email= $user->email;
		
		// set session variable
		$_SESSION['user_logged']= TRUE;
		$_SESSION['user']= $user->name;
		$_SESSION['user_id']= $user->id;
		$user_id=$_SESSION['user_id'];

        //sending email again				
					
		$message="Thank you for sign up with Umipak! 

We are very happy to join your team.  With Umipak you can calculate laytime, store your Bills of Lading, Statements, etc and share it with your peers and clients and keep online folders of your ships. 

But we are not stopping here ;) we are working to always paint our tank with new coatings and deliver more smart solutions for the maritime industry, so we will keep you posted. 

Enjoy Umipak and as the old saying goes, Godspeed!

Made by Umipak – Universal Marine Information and Processing Tank
";					
		$subject = "Subject line: Welcome to Umipak";
		
		$this->email->from('hafiz.softrithmit@gmail.com', 'Chanul App');
	    $this->email->to($email);

	    $this->email->subject($subject);
	    $this->email->message($message);
	    $this->email->send();


		$this->session->set_flashdata("success", "Your account has been registered. Please pay for being a valid user.");

		  // redirect to payment page

		// $_SESSION['subscribed']= 'no';	
		redirect("dashboard/index", "refresh");
				

		//redirect("auth/signin", "refresh");
	}



	public function pdftest()
    {
        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('dashboard/calculator-html-report',[],true);
        $mpdf->WriteHTML($html);

        $mpdf->Output(); // opens in browser
       
    }

	public function testSignUp(){
		$this->load->library('session');
		$data["myVar"] = $this->session->flashdata('item');
		$this->load->view('auth/testSignUp', $data);

	}


	/*Utility Methods Starts*/
		public function encryptIt( $q ) {
	    $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
	    $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
	    return( $qEncoded );
	}

		public function decryptIt( $q ) {
	    $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
	    $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
	    return( $qDecoded );
	}
	/*Utility Methods Ends*/


	// endorsing

	public function save_endorse()
	{
		$data = array(
			'endorse_email'=>$_POST['endorse_email'],
			'user_id'=>$_POST['user_id'],
		);		
		
		$this->db->insert('endorsement', $data);
		//redirect("auth/signin", "refresh");

		$this->session->set_flashdata("success", "Endorse request has been sent.");			
	}



}

