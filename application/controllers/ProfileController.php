<?php
class ProfileController extends CI_Controller
{
	public function __construct()
	 {
	     parent::__construct();
	     $this->load->model('ProfileModel');
	     $this->load->library('email');
	 }

	/*Commun API Starts */

	 public function getlists(){
	    //echo "<pre>";
	    //print_r($_POST);
	    //$table = $_REQUEST['table']; 
	    $data = $this->Calculator_model->getlist($_POST['table']);
	    echo json_encode($data);
	}
        public function get_last_id(){
		$last_id = $this->Calculator_model->get_last_id($_POST['table']);
		echo json_encode($last_id[0]);
	}
        
        public function get_by_id($table=null,$id=null){
		$data = $this->Calculator_model->get_by_id($_POST['table'],$_POST['id']);
	    echo json_encode($data);
	}

	public function index()
	{
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->model('mship');

		$data['ports'] = $this->mship->port();
		$data['activity'] = $this->mship->load_activity();
		$data['cargo'] = $this->mship->load_cargo();
        //$info = $this->ProfileModel->getAll('users');
        //exit;
        //$data['user_info'] = $info[0];
        $this->load->view('profile/profile_view');

		//$this->load->view('profile/ProfileView', $data);

	}

	public function getProfileInfo(){
		$info_array = $this->ProfileModel->getAll('users');
        $info = $info_array[0];
        echo json_encode($info);
	} 

    public function delete(){
    	$table = $_POST['table'];
    	$id    = $_POST['id'];
    	$this->Calculator_model->delete($table,$id);
    	$data = $this->Calculator_model->getlist($table);
	    echo json_encode($data);
    }

    public function updateProfile(){
    	$restult = $this->ProfileModel->update('users', $_POST['data']);
    		$this->getProfileInfo();
    } 

    public function passwordUpdate(){
    	//echo "<pre>";
    	//print_r($_POST);

    	$profile = $_POST['profile'];
    	$password= $_POST['password'];
    	$result = $this->checkEmailPassMatch($profile['email'],$password['current']);
    	if(sizeof($result)==1){
    		$this->ProfileModel->updatePassword($profile['email'],md5($password['current']),md5($password['new']));
    		echo json_encode(1);
    	}else{
    		echo json_encode(0);
    	}
    	
    }
    public function checkEmailPassMatch($email,$password){
    	$password = md5($password);
    	//echo "<pre>";
    	//print_r($this->ProfileModel->getByEmailPassword($email,$password));
    	return $this->ProfileModel->getByEmailPassword($email,$password);
    }


    public function getEndorsersById(){
        $endorsers = $this->ProfileModel->getEndorsersById($_SESSION['user_id']);
        echo json_encode($endorsers);
        //echo "<pre>";
        //($endorsers);
    }

    public function sendMail($sendTo){
        $subject="Umipak – Reset your password";
        $message="Hello  ".$sendTo." You are not endorsed more.
        ";                  
        $this->email->from('softrithmit@gmail.com', 'Chanul App');
        $this->email->to($sendTo);

        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
    }

    public function terminateEndorser(){
        $result = $this->ProfileModel->terminateEndorser($_POST['endorserId']);
        echo $result;
        if($result == 1){
            $this->sendMail('shohag.cse3@gmail.com');
        }
        
    }

}