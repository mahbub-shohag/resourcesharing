<?php

class Dashboard extends CI_Controller
{
	public function index()
	{

        	if(!isset($_SESSION['user_id']))
        	{
        		redirect("auth/signin", "refresh");
        	} 
        	$month= date("Y/m");
        	$this->db->select('*');
			$this->db->from('subscribers');
			$this->db->where(array('user_id' => $_SESSION['user_id']));
			$this->db->where(array('month' => $month));
			$query = $this->db->get();
			$subs_num_rows = $query->num_rows();
			$subs_record = $query->row();


			

			$this->db->select('*');
			$this->db->from('endorsement');
			$this->db->where(array('endorsee' => $_SESSION['user_id']));
			$this->db->where(array('status' => 1));
			$this->db->where(array('payment' => 0));
			// $this->db->where(array('endorse_month' => $month));
			$query = $this->db->get();
			$endorse_num_rows = $query->num_rows();
			

			if ($endorse_num_rows!=null) {
				$endorse_record = $query->row();
			    $endorser=$endorse_record->endorser;
			    $endorsee=$endorse_record->endorsee;

			    $this->db->select('*');
				$this->db->from('users');
				$this->db->where(array('id' => $endorser));
				$query = $this->db->get();
				$users = $query->row();
				$endorser_name=$users->name;



				$this->db->select('*');
				$this->db->from('subscribers');
				$this->db->where(array('user_id' => $endorser));
				$this->db->where(array('month' => $month));
				$query = $this->db->get();
				$endorser_subs_num_rows = $query->num_rows();
				if ($endorser_subs_num_rows!=0) {
					$subs_num_rows_ttl=$subs_num_rows+1;
				}				
				//echo $endorser_subs_num_rows;
				//exit();
			}
			
						
			//exit(); 
			if ($subs_num_rows!=0 && $endorse_num_rows!=0) {
				$data['gold2']="Monthly Subscription Fee for Your Endorser:$15"; //exit();
				$data['endorser']=$endorser;
				$data['endorser_name']=$endorser_name;
				$data['endorsee']=$endorsee;
				$this->load->helper('url'); 
                $this->load->helper('html');
		        $this->load->view('dashboard/payment',$data);
		    }
			elseif($subs_num_rows==0 && $endorse_num_rows==0){
				 $data['gold1']="Monthly Subscription Fee:$15";
				$this->load->helper('url'); 
                $this->load->helper('html');
		        $this->load->view('dashboard/payment',$data);
			}
			elseif($subs_num_rows!=0 && $endorse_num_rows==0){
				//echo "go home"; exit();
				$_SESSION['subscribed']='yes';
				$this->load->helper('url'); 
                $this->load->helper('html');
		        $this->load->view('dashboard/home');
			}
			elseif($subs_num_rows==0 && $endorse_num_rows!=0){
				$data['diamond']="Monthly Subscription Fee Both for You and Your Endorser:$30";//exit();
				$data['endorser']=$endorser;
				$data['endorser_name']=$endorser_name;
				$data['endorsee']=$endorsee;
				$this->load->helper('url'); 
                $this->load->helper('html');
				$this->load->view('dashboard/payment',$data);
			}
			else{	
			//echo "string";exit();	
			    $_SESSION['subscribed']='yes';		
				$this->load->helper('url'); 
				$this->load->helper('html');
				$this->load->view('dashboard/index');
				}


	}

	public function confirmEndorse($key)
	{
		//change endorse status
		$this->db->set('status', '1'); 
		$this->db->where('key', $key); 
		$this->db->update('endorsement');

		// get user id
		$this->db->select('*');
		$this->db->from('endorsement');
		$this->db->where(array('key' => $key));
		$this->db->where(array('status' => '1'));
		$query = $this->db->get();
		$user = $query->row();
		$user_id=$user->endorsee;	


		// get current package
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where(array('id' => $user_id));
		$query = $this->db->get();
		$user = $query->row();
		$package=$user->package;

		// update current package
		$new_package=$package+1;		
		$this->db->set('package', $new_package); 
		$this->db->where('id', $user_id); 
		$this->db->update('users');


		
		$this->session->set_flashdata("success", "Thank you for accepting endorse request. You will pay for your endorser too from now.");
		redirect("dashboard", "refresh");

		// $this->load->helper('url'); 
		// $this->load->helper('html');
		// $this->load->view('dashboard/payment');
	}

	
}

?>