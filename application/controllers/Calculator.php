<?php
class Calculator extends CI_Controller
{
	public function __construct()
	 {
	     parent::__construct();
	     $this->load->model('Calculator_model');
	 }

	/*Commun API Starts */
	 public function getlists(){
	    //echo "<pre>";
	    //print_r($_POST);
	    //$table = $_REQUEST['table']; 
	    $data = $this->Calculator_model->getlist($_POST['table']);
	    echo json_encode($data);
	}
        public function get_last_id(){
		$last_id = $this->Calculator_model->get_last_id($_POST['table']);
		echo json_encode($last_id[0]);
	}
        
        public function get_by_id($table=null,$id=null){
		$data = $this->Calculator_model->get_by_id($_POST['table'],$_POST['id']);
	    echo json_encode($data);
	}
        
        /*Dashboard API Starts*/
 	public function getAllItinaryById(){
 		$data = $this->Calculator_model->getAllItinaryById($_POST['created_by']);
 		echo json_encode($data);
 		//echo "<pre>";
 		//print_r($data);
 		//exit;
 	}
 	public function getAllDocsById(){
 		$data = $this->Calculator_model->getAllDocsById($_POST['created_by']);
 		echo json_encode($data);
 		//echo "<pre>";
 		//print_r($data);
 		//exit;
 	}
 	


 	/*Dashboard API Ends*/
        
        /*calculator part starts*/

        public function getCalculatorsByUser(){
		$calculators = $this->Calculator_model->getCalculatorsByUser($_POST['createdBy'],$_POST['vesselId']);
		echo json_encode($calculators);
		//echo "<pre>";
		//print_r($calculators);
		//exit;
	} 
        public function calEdit(){
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->model('mship');
		$data['ports'] = $this->mship->port();
		$data['activity'] = $this->mship->load_activity();
		$data['cargo'] = $this->mship->load_cargo();
		$this->load->view('dashboard/calculatorEdit', $data);
	}
        /*calculator part ends*/
 
        public function test_mail(){

$this->load->library('email');

    
$email='hafiz.csejnu@gmail.com';
$name='Hafizur rahman';
$message= 'sghdhfghgd';

    $this->email->from($email, $name);
    $this->email->to('hafizur.csejnu@gmail.com');

    $this->email->subject('Subject');
    $this->email->message($message);
    if($this->email->send())
    {
echo 'Hello';
        echo $this->email->print_debugger();
        //redirect('contact-us/thanks', 'location');
    }
   else
   {
    echo 'Something went wrong...';
   }
}


 	/*Commun API Ends */

      public function makePdf($id){
    	$ship_info = $this->Calculator_model->get_by_id('calculator',$id);
    	$data['ship_info'] = $ship_info[0];
    	//echo '<pre>';
    	//print_r($data);
    	//exit;
    	$mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('reports/invoice',$data,true);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }  


 	public function getCustomDropDownlists($dropdown_for=null){
	    //echo "<pre>";
	    //print_r($_SESSION);
	    //exit;
	    $user = $_SESSION['user_id']; 
	    $dropdown_for = $_POST['dropdown_for'];
	    $data = $this->Calculator_model->getCustomDropDownlists($user,$dropdown_for);
	    echo json_encode($data);
	}

	public function index()
	{
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->model('mship');

		$data['ports'] = $this->mship->port();
		$data['activity'] = $this->mship->load_activity();
		$data['cargo'] = $this->mship->load_cargo();

		$this->load->view('dashboard/calculator', $data);

	}

	public function itinaryList(){
		$info = $this->Calculator_model->getlist('itineraryn');
		//echo '<pre>';
		//print_r($info);
		echo json_encode($info);
	}

	public function ship_search()
	{

		$this->load->model('mship');

		if (isset($_GET['term'])) {			
			$result = $this->mship->ship_search($_GET['term']);
			if (count($result)>0) {
				foreach ($result as $sp )
					$arr_result[] = $sp->name;
				echo json_encode($arr_result);
			}
		}
		
	}


	public function ship_details()
	{

		$this->load->model('mship');

		if (isset($_POST['ship_name'])) {			
			$result = $this->mship->ship_details($_POST['ship_name']);

			echo json_encode($result);		
		
		}
		
	}	

	public function save_activity()
	{
		$user = $_SESSION['user_id'];
		$data = array(
			'dropdown_for'=>$_POST['dropdown_for'],
			'dropdown_text'=>$_POST['dropdown_text'],
			'created_by_user_id'=>$user,
		);		
		
		$this->db->insert('custom_dropdown_data', $data);
		$activitylists = $this->Calculator_model->getCustomDropDownlists($user,$_POST['dropdown_for']);
		echo json_encode($activitylists);			
	}

	public function save_dropdown()
	{		
		$user = $_SESSION['user_id'];
		$data = array(
			'dropdown_for'=>$_POST['dropdown_for'],
			'dropdown_text'=>$_POST['dropdown_text'],
			'created_by_user_id'=>$user,
		);		
		
		$this->db->insert('custom_dropdown_data', $data);
		$dropdowns = $this->Calculator_model->getCustomDropDownlists($user,$_POST['dropdown_for']);
		echo json_encode($dropdowns);		
	}

	public function save_description()
	{
		
		$data = array(
			'dropdown_for'=>$_POST['dropdown_for'],
			'dropdown_text'=>$_POST['dropdown_text'],
			'created_by_user_id'=>$_POST['created_by_user_id'],
		);		
		
		$this->db->insert('custom_dropdown_data', $data);

	}




       public function savepdf(){
    	//echo "<pre>";
    	//print_r($_POST);
    	//exit;
		$data['shipName'] = $_POST['info']['ship_name']['name'];
		$data['activity'] = $_POST['info']['activity'];
		$data['arrival'] = $_POST['info']['arrival'];
		$data['berthing'] = $_POST['info']['berthing'];
		$data['cargo'] = $_POST['info']['cargo'];
		$data['completion'] = $_POST['info']['completion'];
		$data['demurrageRate'] = $_POST['info']['demurrageRate'];
		$data['demurrageRateUnit'] = $_POST['info']['demurrageRateUnit'];
		$data['directoryName'] = $_POST['info']['directoryName'];
		$data['despatchRate'] = $_POST['info']['despatchRate'];
		$data['despatchRateUnit'] = $_POST['info']['despatchRateUnit'];
		$data['loa'] = $_POST['info']['loa'];
		$data['imo'] = $_POST['info']['imo'];
		$data['dwt'] = $_POST['info']['dwt'];
        $data['norTendered'] = $_POST['info']['norTendered'];
        $data['port'] = $_POST['info']['port']['destination']['name'];
        $data['quantity'] = $_POST['info']['quantity'];
        $data['rateUnit'] = $_POST['info']['rateUnit'];
        $data['rate'] = $_POST['info']['rate'];
        $data['timeStart'] = $_POST['info']['timeStart'];
	$data['fileName']  = $_POST['info']['fileName'];
        $data['createdBy'] = $_POST['info']['createdBy'];
	$data['vesselId'] = $_POST['info']['vesselId'];
	$id = $this->db->insert('calculator', $data);
		
		//echo '<pre>';
		//print_r($id);
		//exit;
		echo json_encode($this->db->insert_id());
	} 
    


	public function savepdfold(){
		$data['ship_name'] = $_POST['info']['ship_name']['name'];
		$data['loa'] = $_POST['info']['loa'];
		$data['imo'] = $_POST['info']['imo'];
		$data['dwt'] = $_POST['info']['dwt'];
        $data['directoryName'] = $_POST['info']['directoryName'];
		$data['fileName']  = $_POST['info']['fileName'];
		$this->db->insert('ship_info', $data);
		echo json_encode($this->db->insert_id());
	}
   public function infopdf($id)
    {   
    	//echo "<pre>";
    	//print_r($id);
    	//exit;
    	ob_start();
    	$ship_info = $this->Calculator_model->get_by_id('ship_info',$id);
    	$data['ship_info'] = $ship_info[0];
    	$fileName = $ship_info[0]['fileName'];
        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('reports/invoice',$data,true);
        $mpdf->WriteHTML($html);
        ob_clean();
        $mpdf->Output('itinery/'.$fileName.'.pdf','F');
        redirect('Calculator/showFiles');
    }    

    public function showFiles(){
    	$this->load->helper('url'); 
		$this->load->helper('html');
		$data['files'] = $this->Calculator_model->getlist('ship_info');
		// echo '<pre>';
		// print_r($data['files']);
		// exit;
		$this->load->view('dashboard/fileList',$data);
    } 

    /*Dashboard Starts*/
    public function saveShip(){
    	$this->db->insert('ship', $_POST);
    	$data = $this->Calculator_model->getlist('ship');
	    echo json_encode($data);
    } 

    public function updateShip(){
    	$this->Calculator_model->update('ship', $_POST);
    	$data = $this->Calculator_model->getlist('ship');
	    echo json_encode($data);
    }

    public function delete(){
    	$table = $_POST['table'];
    	$id    = $_POST['id'];
    	$this->Calculator_model->delete($table,$id);
    	$data = $this->Calculator_model->getlist($table);
	    echo json_encode($data);
    }

    public function save(){
    	$table = $_POST['table'];
    	unset($_POST['table']);
    	$this->Calculator_model->save($table, $_POST['data']);
    	$data = $this->Calculator_model->getlist($table);
	    echo json_encode($data);
    } 

    public function itnsaveold(){
    	//echo '<pre>';
    	//print_r($_POST);exit;
    	$data = $_POST['data'];
    	$itinerary = [];
    	$itinerary['port'] = $data['vessel']['port'];
    	$itinerary['vessel'] = $data['vessel']['vessel'];
    	$itinerary['cargo'] = $data['cargo'];
    	$itinerary['vessel_folder'] = $data['vessel']['vessel_id'];	
        $this->Calculator_model->save('itineraryn', $itinerary);
        $last_id = $this->Calculator_model->get_last_id('itineraryn');
        //echo '<pre>';print_r($last_id);exit;
        foreach ($data['events'] as $key => $value) {
        	$event = [];
        	$event['event_name'] = $value['event_name'];
        	$event['estimate_date'] = $value['estimate_date'];
        	$event['actual_date'] = $value['actual_date'];
        	$event['itineraryn_id'] = $last_id[0]['id'];
        	$this->Calculator_model->save('itinerary_events', $event);
        }

    } 

    public function update(){
    	$table = $_POST['table'];
    	unset($_POST['table']);
    	$data = $_POST['data'];
    	$this->Calculator_model->update($table, $data);
    	$data = $this->Calculator_model->getlist($table);
	    echo json_encode($data);
    }

    /*Dashboard Ends*/

    /*Itinerary Starts*/
    public function itineraries(){
		$info = $this->Calculator_model->getlist('ship_info');
		$data['itineraries'] = $info;
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->view('itinerary/home',$data);
	}
    /*Itinerary Ends*/

    /*Free Calculator Starts*/
    public function basic_calculator()
	{
		$this->load->helper('url'); 
		$this->load->helper('html');
		$this->load->model('mship');
		$data['ports'] = $this->mship->port();
		$data['activity'] = $this->mship->default_activity();
		$data['cargo'] = $this->mship->default_cargo(); 
		$this->load->view('dashboard/free_calculator',$data);
	}
	
    /*Free Calculator Ends*/
    
    public function itnsave(){
    	//echo '<pre>';
    	//print_r($_POST);exit;
    	$data = $_POST['data'];
    	$itinerary = [];
    	$itinerary['port'] = $data['vessel']['port'];
    	$itinerary['vessel'] = $data['vessel']['vessel'];
    	$itinerary['cargo'] = $data['cargo'];
    	$itinerary['vessel_folder'] = $data['vessel']['vessel_id'];
    	$itinerary['vesselId'] = $data['vessel']['id'];
    	$itinerary['created_by'] = $_SESSION['user_id'];	
        $this->Calculator_model->save('itineraryn', $itinerary);
        $last_id = $this->Calculator_model->get_last_id('itineraryn');
        if(isset($data['events'])){
        	foreach ($data['events'] as $key => $value) {
        	$event = [];
        	$event['event_name'] = $value['event_name'];
        	$event['estimate_date'] = $value['estimate_date'];
        	$event['actual_date'] = $value['actual_date'];
        	$event['itineraryn_id'] = $last_id[0]['id'];
        	$this->Calculator_model->save('itinerary_events', $event);
        	}
		}
        $itn = [];
            $itn['actual_arrival_time'] = $data['itn_basic']['actual_arrival_time'];
            $itn['est_arrival_time'] = $data['itn_basic']['est_arrival_time'];
            $itn['actual_inspection_time'] = $data['itn_basic']['actual_inspection_time'];
            $itn['est_inspection_time'] = $data['itn_basic']['est_inspection_time'];
            $itn['actual_sailing_time'] = $data['itn_basic']['actual_sailing_time'];
            $itn['est_sailing_time'] = $data['itn_basic']['est_sailing_time'];
            $itn['actual_starting_time'] = $data['itn_basic']['actual_starting_time'];
            $itn['est_starting_time'] = $data['itn_basic']['est_starting_time'];
            $itn['actual_bearthing_time'] = $data['itn_basic']['actual_bearthing_time'];
            $itn['est_bearthing_time'] = $data['itn_basic']['est_bearthing_time'];
            $itn['actual_completion_time'] = $data['itn_basic']['actual_completion_time'];
            $itn['est_completion_time'] = $data['itn_basic']['est_completion_time'];
            
            $itn['itineraryn_id'] = $last_id[0]['id'];
            echo $this->Calculator_model->save('itinerary', $itn);
            //echo json_encode($this->itinaryList());

        
    }  

    public function getItinerarylistById(){
        echo json_encode($this->Calculator_model->getItinerarylistById($_POST['vesselId']));
    }

    public function getLastItinerary(){
    	//echo '<pre>';
    	//print_r($_POST);exit;
    	
    	$itinerary_n = $this->Calculator_model->getLastItinerary($_POST['vesselId']);
    	//echo '<pre>';
    	//print_r($itinerary_n);exit;
    	if(!empty($itinerary_n)){
    	    $itinerary = $this->Calculator_model->getItinerary($itinerary_n[0]['id']);
    	$events = $this->Calculator_model->getEvents($itinerary_n[0]['id']);
    	$data['itinerary_n'] = $itinerary_n[0];
    	$data['itinerary'] = $itinerary[0];
    	$data['events'] = $events;
    	echo json_encode($data);   
    	}
    	
    }

     public function getItineraryByIdNew(){
        //echo '<pre>';
        //print_r($_POST);exit;
    	$itinerary_n = $this->Calculator_model->getItineraryById($_POST['id'],$_POST['vesselId']);
    	$itinerary = $this->Calculator_model->getItinerary($itinerary_n[0]['id']);
    	$events = $this->Calculator_model->getEvents($itinerary_n[0]['id']);
    	$data['itinerary_n'] = $itinerary_n[0];
    	$data['itn_basic'] = $itinerary[0];
    	$data['events'] = $events;
        //echo "<pre>";
        //print_r($itinerary);exit;
    	echo json_encode($data);
    }



}