<?php
class Docs extends CI_Controller
{
	public function __construct()
	 {
	     parent::__construct();
	     $this->load->model('DocsModel');
	     $this->load->library('Upload');
	     $this->load->helper('url'); 
		 $this->load->helper('html');
	 }

	 public function index(){
	 	$this->load->view('docs/files');
	 }

	 /*Commun API Starts */


	 public function getlistByTable(){
	 	$data = $this->DocsModel->getlistByTable($_POST['table']);
	    echo json_encode($data);
	 }
	 
	 public function getlists(){
	    //echo "<pre>";
	    //print_r($_POST);
	    //$table = $_REQUEST['table']; 
	    $data = $this->DocsModel->getlist($_POST['parent_id']);
	    echo json_encode($data);
	}
        
    public function get_by_id($table=null,$id=null){
	   $data = $this->DocsModel->get_by_id($_POST['table'],$_POST['id']);
	   echo json_encode($data);
	}
	 public function get_by_vessel_id($table=null,$id=null){
	   $data = $this->DocsModel->get_by_vessel_id($_POST['table'],$_POST['id']);
	   echo json_encode($data);
	}


	public function get_by_parent_id($parent_id){
		$data = $this->DocsModel->get_by_parent_id($parent_id);
	   	return $data;	
	}

	/*Commun API Ends */
	public function checkIsShared($user_id,$docs_id){
	 	$data = $this->DocsModel->checkIsShared($user_id,$docs_id);
	 	return $data;

	}

	public function getSharedFileList(){
		echo json_encode($this->DocsModel->getSharedFileList($_SESSION['user_id']));
	}

	public function shareItemUser(){
		//echo "<pre>";
		//print_r($_SESSION);

		$data['shared_by_id'] = $_SESSION['user_id'];
		$data['user_id'] = $_POST['data']['user_id'];
		$data['docs_id'] = $_POST['data']['docs_id'];
		$isExists = $this->checkIsShared($data['user_id'],$data['docs_id']);
		//echo "<pre>";
		//echo sizeof($isExists);
		//exit;
		if(sizeof($isExists)==0){
			$result = $this->DocsModel->save($_POST['table'],$data);	
			if($result){
				echo "Saved";
			}else{
				echo "Couldn't Save";
			}
		}else{
			echo "exists";
		}
		
	}

    public function shareItemUserNew(){
    	$count = 0;
		foreach ($_POST['data']['docs'] as $key => $value) {
			$data['shared_by_id'] = $_SESSION['user_id'];
			$data['user_id'] = $_POST['data']['user_id'];
			$data['docs_id'] = $value['id'];
			$isExists = $this->checkIsShared($data['user_id'],$data['docs_id']);
			if(sizeof($isExists)==0){
				$result = $this->DocsModel->save($_POST['table'],$data);	
				if($result){
					$count++;
				}else{
					//echo "Couldn't Save";
				}
			}else{
				//echo "exists";
			}	
		}
		echo $count;	
	}

	public function makeDirectory(){
		if (!file_exists($_POST['folder_path'])) {
    			mkdir($_POST['folder_path'], 0777, true);
			}
		$directory['parent_id'] = $_POST['parent_id'];;
		$directory['type_id'] = 8;
		$directory['type_name'] = "folder";
		$directory['name'] = $_POST['directory_name'];
		$directory['item_link'] = "/".$_POST['folder_path'];	
		$directory['created_by'] = $_POST['created_by'];
		$directory['created_date'] = "";
		if(isset($_POST['vessel_id'])){
			$directory['vessel_id'] = $_POST['vessel_id'];
		}
		$result = $this->DocsModel->add_directory($directory);	
		if($result){
			$files = $this->DocsModel->getlist($directory['parent_id']);
        	echo json_encode($files);
		}else{
			echo 'Could Not Create Directory';
		}
	}

	function upload_original($foo,$folder)
	{
	    $folder = $folder;
	    if ($foo->uploaded) {
	        // save uploaded image with no changes
	        $foo->Process($folder);
	        //echo($images['image_path']);exit;
	        if ($foo->processed) {
	            return true;
	        } else {
	            return false;
	            //echo 'error : ' . $foo->error;
	        }
	    }
	}

	public function uploadFile(){
		$info = json_decode($_POST['info']);
		$file = new Upload($_FILES['file']);
		$data['parent_id'] = $info->parent_id;
	    $data['parent_directory'] = $info->directory;
	    $data['name'] = $file->file_src_name;
		$data['type_name'] = $file->file_src_name_ext;
		$data['item_link'] = "/".$info->directory.$file->file_src_name;
		$data['created_by'] = $info->created_by;

        $upload = $this->upload_original($file,$info->directory);
        if($upload){
        	$this->DocsModel->add_file($data);
        	$files = $this->get_by_parent_id($info->parent_id);
        	echo json_encode($files);
        }else{
        	echo 'Error';
        }
	}

	public function delete(){
		 $parent_id = $_POST['docs'][0]['parent_id'];
		 foreach ($_POST['docs'] as $key => $value) {
		 	//echo '<pre>';
		    //print_r($_POST);exit;
			$items = $this->DocsModel->soft_delete($value['id']);
			
		 }
		 $items = $this->DocsModel->getlist($parent_id);
         echo json_encode($items);
		// echo '<pre>';
		// print_r($_POST);exit;
		// $this->DocsModel->soft_delete('docs',object);
	}

	public function paste(){
		foreach ($_POST['docs'] as $key => $value) {
			$this->DocsModel->save('docs',$value);
		}
	}
}	 