<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class LandingPage extends CI_Controller {
		public function index()
		{
			$this->load->helper('url'); 
			$this->load->helper('html');
			$this->load->view('landingpages/first');
		}
		public function second()
		{
			$this->load->helper('url'); 
			$this->load->helper('html');
			$this->load->view('landingpages/second');
		}
		public function third()
		{
			$this->load->helper('url'); 
			$this->load->helper('html');
			$this->load->view('landingpages/third');
		}

		public function signup()
		{
			$this->load->helper('url'); 
			$this->load->helper('html');
			$this->load->view('landingpages/signup');
		}	


		


	}

?>