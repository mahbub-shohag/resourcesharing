<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mship extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }
	
	function ship_search($name){
		$this->db->like('name', $name, 'after');		
		return $this->db->get('ship')->result();
	}

	function ship_details($name){
		$this->db->where('name', $name);		
		return $this->db->get('ship')->result();
	}

	function port(){
		$this->db->select('destination');
		$this->db->where('destination !=', ' ');
		$this->db->order_by("destination", "asc");
		return $this->db->get('ship')->result();
	}




	function load_activity(){
		$this->db->select('dropdown_text');
		$this->db->where('dropdown_for =', 'activity');
		$this->db->where('created_by_user_id =', $_SESSION['user_id']);
		return $this->db->get('custom_dropdown_data')->result();
	}

        function default_activity(){
		$this->db->select('dropdown_text');
		$this->db->where('dropdown_for =', 'activity');
		//$this->db->where('created_by_user_id =', $_SESSION['user_id']);
		return $this->db->get('custom_dropdown_data')->result();
	}

	function default_cargo(){
		$this->db->select('dropdown_text');
		$this->db->where('dropdown_for =', 'cargo');
		//$this->db->where('created_by_user_id =', $_SESSION['user_id']);
		return $this->db->get('custom_dropdown_data')->result();
	}

	function load_cargo(){
		$this->db->select('dropdown_text');
		$this->db->where('dropdown_for =', 'cargo');
		$this->db->where('created_by_user_id =', $_SESSION['user_id']);
		return $this->db->get('custom_dropdown_data')->result();
	}
	function load_description(){
		$this->db->select('dropdown_text');
		$this->db->where('dropdown_for =', 'description');
		$this->db->where('created_by_user_id =', $_SESSION['user_id']);
		return $this->db->get('custom_dropdown_data')->result();
	}




	// function with nomal query style
	
	// function port(){
	// 	$query = $this->db->query("SELECT destination FROM `ship` WHERE destination !='' order by destination asc");
	// 	return $query->result();
	// }





}

?>