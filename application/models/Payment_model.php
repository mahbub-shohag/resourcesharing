<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }

        function endorse_request(){
			
		return $this->db->get('endorsement')->result();
	}

	public function endorseInsert($data){
		$this->db->insert('endorsement', $data);
	}

	public function check_exist($table,$field,$value){
		//print_r($value);
	    $this->db->where($field,$value);
	    $query = $this->db->get($table);
	    //echo $this->db->last_query();exit;
	    //echo $query->num_rows();exit;
	    if ($query->num_rows() > 0){
            return 'exists';
	    }
	    else{
	        return 'not exists';
	    }
	}

	public function check_already_sent($table,$field1,$value1,$field2,$value2,$field3,$value3){
		//print_r($value);
	    $this->db->where($field1,$value1);
	    $this->db->where($field2,$value2);
	    $this->db->where($field3,$value3);
	    $query = $this->db->get($table);
	    //echo $this->db->last_query();exit;
	    //echo $query->num_rows();exit;
	    if ($query->num_rows() > 0){
            return 'exists';
	    }
	    else{
	        return 'not exists';
	    }
	}

}
?>