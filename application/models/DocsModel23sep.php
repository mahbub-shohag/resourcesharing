<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class DocsModel extends CI_Model
{
	
	public function __construct()
    {
        parent::__construct();
    }

    /*commun api starts*/
    public function getlist($parent_id){
        $this->db->select('*');
        $this->db->from('docs');
        $this->db->where('parent_id',$parent_id);
        $this->db->order_by('id','DESC');
        return $this->db->get()->result_array();
    }

    public function save($table,$data){
        $this->db->insert($table,$data);
    }
    public function update($table,$obj){
        $this->db->where('id',$obj['id']);
        unset($obj['id']);
        $this->db->update($table,$obj);
    }
    public function delete($table,$id){
        $this->db->where('id',$id);
        $this->db->delete($table);
    }
    public function get_by_id($table,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        //print_r($sql);exit;
        //$sql = $this->db->last_query();
        return $query->result_array();
    }

    public function get_by_parent_id($parent_id){
        $this->db->select('*');
        $this->db->from('docs');
        $this->db->where('parent_id',$parent_id);
        $query = $this->db->get();
        //print_r($sql);exit;
        //$sql = $this->db->last_query();
        return $query->result_array();
    }



    public function add_directory($data){
    	return $this->db->insert('docs',$data);
    }
    public function add_file($data){
    	$this->db->insert('docs',$data);	
    }
}    