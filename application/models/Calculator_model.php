<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Calculator_model extends CI_Model
{
	
	public function __construct()
    {
        parent::__construct();
    }
	
	/*commun api starts*/

    public function get_last_id($table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('id', 'DESC');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getlist($table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('id','DESC');
        return $this->db->get()->result_array();
    }

    public function save($table,$data){
        $this->db->insert($table,$data);
    }
    public function update($table,$obj){
        $this->db->where('id',$obj['id']);
        unset($obj['id']);
        $this->db->update($table,$obj);
    }
    public function delete($table,$id){
        $this->db->where('id',$id);
        $this->db->delete($table);
    }
    public function get_by_id($table,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        //print_r($sql);exit;
        //$sql = $this->db->last_query();
        return $query->result_array();
    }
    public function search_by_query($table,$searchcriteria=null){
        $query = "select * from ".$table." ".$searchcriteria;
        $q = $this->db->query($query);
        return $q->result_array();
    }
     
     public function getCalculatorsByUser($createdBy,$vesselId){
        $this->db->select('*');
        $this->db->from('calculator');
        $this->db->where('createdBy',$createdBy);
        $this->db->where('vesselId',$vesselId);
        $this->db->order_by('id','DESC');
        //$this->db->get();
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        return $this->db->get()->result_array();
    }


    /*Dashboard API Starts*/
    public function getAllItinaryById($created_by){
        $this->db->select("count(id) total_itinary");
        $this->db->from("ship_info");
        $this->db->where("created_by",$created_by);
        $this->db->group_by("created_by");
        return $this->db->get()->row();
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        //return $query->result_row();
    }

    public function getAllDocsById($created_by){
        $this->db->select("count(id) total_documents");
        $this->db->from("ship_info");
        $this->db->where("created_by",$created_by);
        $this->db->where("fileName is NOT NULL");
        $this->db->group_by("created_by");
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        return $this->db->get()->row();
    }




    /*Dashboard API Ends*/

    /*Customer API Starts*/

    public function getCustomDropDownlists($user=null,$dropdown_for=null){
    	$this->db->select('*');
        $this->db->from('custom_dropdown_data');
        if($user!=null && $dropdown_for!=null){
            $this->db->where('created_by_user_id',$user);
            $this->db->where('dropdown_for',$dropdown_for);    
        }else if($user!=null){
            $this->db->where('created_by_user_id',$user,'dropdown_for',$dropdown_for);    
        }else{

        }
        $this->db->order_by('id','DESC');
        //$this->db->get();
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        return $this->db->get()->result_array();
      
    }

	function fetch_ship_data($query)
	{
		$this->db->select("*");
		$this->db->from("ship");

		if ($query !='') {
			$this->db->like('name', $query);			
		}
		$this->db->order_by('name', ASC);
		
		return $this->db->get();
	}
	
	public function getItinerarylistById($vesselId){
        $this->db->select('*');
        $this->db->from('itineraryn');
        $this->db->where("created_by",$_SESSION['user_id']);
        $this->db->where('vesselId',$vesselId);
        $this->db->order_by('id','DESC');
        return $this->db->get()->result_array();
    }
	
	function getLastItinerary($vesselId){
       $this->db->select('*');
        $this->db->from('itineraryn');
        $this->db->where('vesselId',$vesselId);
        $this->db->where('created_by',$_SESSION['user_id']);
        $this->db->order_by('id', 'DESC');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getItineraryById($id,$vesselId){
       $this->db->select('*');
        $this->db->from('itineraryn');
        $this->db->where('id', $id);
        $this->db->where('vesselId',$vesselId);
        $this->db->where('created_by',$_SESSION['user_id']);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function getItinerary($itn_id){
        $this->db->select('*');
        $this->db->from('itinerary');
        $this->db->where('itineraryn_id', $itn_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function getEvents($itn_id){
        $this->db->select('*');
        $this->db->from('itinerary_events');
        $this->db->where('itineraryn_id', $itn_id);
        $query = $this->db->get();
        return $query->result_array();
    }
}

?>