<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class DocsModel extends CI_Model
{
	
	public function __construct()
    {
        parent::__construct();
    }

    /*commun api starts*/
    public function getlistByTable($table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('id','DESC');
        return $this->db->get()->result_array();
    }
    public function getlist($parent_id){
        $this->db->select('*');
        $this->db->from('docs');
        $this->db->where('parent_id',$parent_id);
        $this->db->where('is_active',null);
        $this->db->where('created_by',$_SESSION['user_id']);
        $this->db->order_by('id','DESC');
        return $this->db->get()->result_array();
    }

    public function getSharedFileList($user_id){
        $this->db->select('d.*,u.name user_name,sb.name shared_by');
        $this->db->from('share_docs_user sdu');
        $this->db->join('docs d','d.id=sdu.docs_id','left');
        $this->db->join('users u','u.id=sdu.user_id','left');
        $this->db->join('users sb','sb.id=sdu.shared_by_id','left');
        $this->db->where('sdu.user_id',$user_id);
        $query = $this->db->get();
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        return $query->result_array();
    }

    public function save($table,$data){
        return $this->db->insert($table,$data);
    }
    public function update($table,$obj){
        $this->db->where('id',$obj['id']);
        unset($obj['id']);
        $this->db->update($table,$obj);
    }


    public function delete($table,$id){
        $this->db->where('id',$id);
        $this->db->delete($table);
    }
    

    public function soft_delete($id){
        $data = array('is_active' => 1);
        $this->db->where('id',$id);
        $this->db->update('docs',$data);
        
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
    }
    public function get_by_id($table,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        
        return $query->result_array();
    }

    public function get_by_vessel_id($table,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('vessel_id',$id);
        $query = $this->db->get();
        //print_r($sql);exit;
        //$sql = $this->db->last_query();
        return $query->result_array();
    }

    public function get_by_parent_id($parent_id){
        $this->db->select('*');
        $this->db->from('docs');
        $this->db->where('parent_id',$parent_id);
        $query = $this->db->get();
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        return $query->result_array();
    }



    public function add_directory($data){
    	return $this->db->insert('docs',$data);
    }
    public function add_file($data){
    	$this->db->insert('docs',$data);	
    }

    public function checkIsShared($user_id,$docs_id){
        $this->db->select('*');
        $this->db->from('share_docs_user');
        $this->db->where('user_id',$user_id);
        $this->db->where('docs_id',$docs_id);
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        $query = $this->db->get();
        return $query->result_array();
    }

}    