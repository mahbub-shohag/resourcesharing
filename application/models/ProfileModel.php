<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProfileModel extends CI_Model
{
	
	public function __construct()
    {
        parent::__construct();
    }
	
	/*commun api starts*/

     public function getAll($table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$_SESSION['user_id']);
        return $this->db->get()->result_array();
    }

    public function get_last_id($table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('id', 'DESC');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getlist($table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('id','DESC');
        return $this->db->get()->result_array();
    }

    public function save($table,$data){
        $this->db->insert($table,$data);
        //$sql = $this->db->last_query();
        //print_r($sql);exit;

    }
    public function update($table,$obj){
        $this->db->where('id',$obj['id']);
        unset($obj['id']);
        $this->db->update($table,$obj);
    }
    public function delete($table,$id){
        $this->db->where('id',$id);
        $this->db->delete($table);
    }
    public function get_by_id($table,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        //print_r($sql);exit;
        //$sql = $this->db->last_query();
        return $query->result_array();
    }
    public function search_by_query($table,$searchcriteria=null){
        $query = "select * from ".$table." ".$searchcriteria;
        $q = $this->db->query($query);
        return $q->result_array();
    }

    public function getByEmailPassword($email,$password){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$email);
        $this->db->where('password',$password);
        $query = $this->db->get();
        return $query->result_array();
    }

   public function updatePassword($email,$current,$newPassword){
        $this->db->set('password', $newPassword);
        $this->db->where('email',$email);
        $this->db->where('password',$current);
        $this->db->update('users');
    }
    
    public function getEndorsersById($endorseeId){
        $this->db->select('e.created_date started_from, e.endorsee endorsee_id, u.id endorser_id, u.name name, u.email email, u.company_name company');
        $this->db->from('endorsement e');
        $this->db->join('users u', 'e.endorser = u.id', 'left');
        $this->db->where('e.endorsee',$endorseeId);
        $this->db->where('e.status',1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function terminateEndorser($endorserId){
        $this->db->set('status',0);
        $this->db->where('endorser',$endorserId);
        $this->db->where('endorsee',$_SESSION['user_id']);
        $this->db->update('endorsement');
        return($this->db->affected_rows() !=1)?false:true;
    }
     
     
}

?>