<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Calculator_model extends CI_Model
{
	
	public function __construct()
    {
        parent::__construct();
    }
	
	/*communt api starts*/
    public function getlist($table){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by('id','DESC');
        return $this->db->get()->result_array();
    }
    public function save($table,$data){
        $this->db->insert($table,$data);
    }
    public function update($table,$obj){
        $this->db->where('id',$obj['id']);
        unset($obj['id']);
        $this->db->update($table,$obj);
    }
    public function delete($table,$id){
        $this->db->where('id',$id);
        $this->db->delete($table);
    }
    public function get_by_id($table,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where('id',$id);
        $query = $this->db->get();
        // if($searchfield && $searchcvalue){
        //     $this->db->where($searchfield,$searchcvalue);
        // }
        //print_r($sql);exit;
        //$sql = $this->db->last_query();
        return $query->result_array();
    }
    public function search_by_query($table,$searchcriteria=null){
        $query = "select * from ".$table." ".$searchcriteria;
        $q = $this->db->query($query);
        return $q->result_array();
    }
    /*Customer API Starts*/

    public function getCustomDropDownlists($user=null,$dropdown_for=null){
    	$this->db->select('*');
        $this->db->from('custom_dropdown_data');
        if($user!=null && $dropdown_for!=null){
            $this->db->where('created_by_user_id',$user);
            $this->db->where('dropdown_for',$dropdown_for);    
        }else if($user!=null){
            $this->db->where('created_by_user_id',$user,'dropdown_for',$dropdown_for);    
        }else{

        }
        $this->db->order_by('id','DESC');
        //$this->db->get();
        //$sql = $this->db->last_query();
        //print_r($sql);exit;
        return $this->db->get()->result_array();
    }

	function fetch_ship_data($query)
	{
		$this->db->select("*");
		$this->db->from("ship");

		if ($query !='') {
			$this->db->like('name', $query);			
		}
		$this->db->order_by('name', ASC);
		
		return $this->db->get();
	}
}

?>