<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Metas -->
    <meta charset="utf-8">
    <meta name="author" content="ChitrakootWeb" />    
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="keywords" content="HTML5 Template Onepage Dicor" />
    <meta name="description" content="Creative One Page Multipurpose Template" />

    <!-- Title  -->
    <title>Welcome to ProjectName</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="http://www.chitrakootweb.com/template/dicor/img/favicon.png">
    <link rel="apple-touch-icon" href="http://www.chitrakootweb.com/template/dicor/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="http://www.chitrakootweb.com/template/dicor/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="http://www.chitrakootweb.com/template/dicor/img/apple-touch-icon-114x114.png">

    <!-- Plugins -->
    <link rel="stylesheet" href="http://www.chitrakootweb.com/template/dicor/css/plugins.css" />

    <link rel="alternate" type="application/rss+xml" title="Altisrael &raquo; Icons Comments Feed" href="http://altisrael.com/icons/feed/" />


    <!-- Core Style Css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assett/css/style.css" />

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

</head>

<body>

    <!-- Start Navbar -->
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <!-- start logo -->
            <a class="logo" href="javascript:void(0);" data-scroll-nav="0">
                <!-- <img src="img/sample-logo.png" alt="Dicor - Creative One Page Multipurpose Template" title="Dicor - Creative One Page Multipurpose Template"> -->
                <?php 
                    echo img('img/logoCopy.png');
                ?>
            </a>
            <!-- end Logo -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"><i class="fas fa-bars"></i></span>
            </button>
            <!-- navbar links -->
            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link active" href="javascript:void(0);" data-scroll-nav="0">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="1">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="2">How it works</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="3">FREE CALCULATOR</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="4">Price</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="5">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="6">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- end navbar links -->
        </div>
    </nav>
    <!-- End Navbar  -->

    <!-- Start Header -->
    <header class="header valign bg-img background-position-top" data-scroll-index="0" data-overlay-dark="5" data-background="img/bg2.jpg" data-stellar-background-ratio="0.5">
        <!-- particles -->
        <div id="particles-js"></div>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center caption">
                    <h3 class="alt-font font-size28 sm-font-size18 no-margin">We Are Awesome</h3>
                    <h1 class="banner-headline clip">
                            <span class="blc">Creative for</span>
                            <span class="banner-words-wrapper">
                              <b class="is-visible">Startup</b>
                              <b>Business</b>
                              <b>Agency</b>
                            </span>
                    </h1>
                    <p class="margin-30px-bottom sm-margin-20px-bottom xs-display-none">Lorem ipsum dolor sit amet consectetur elit sed do eiusmod incididunt ametfh.</p>
                    <a href="javascript:void(0);" class="btn">
                        <span>Learn More</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="arrow">
            <a href="javascript:void(0);" data-scroll-nav="1">
                <i class="fas fa-chevron-down"></i>
            </a>
        </div>
    </header>
    <!-- End Header -->

    <!-- Start About Section-->
    <section class="hero no-padding-bottom" data-scroll-index="1">
        <div class="container margin-50px-bottom">
            <div class="row">
                <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center wow fadeInUp">
                    <div class="alt-font text-pink-color margin-5px-bottom text-uppercase letter-spacing-1">About Business Name</div>
                    <h5 class="margin-10px-bottom font-weight-300">We Are <span class="font-weight-500">Awesome</span> Agency.</h5>
                    <div class="separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                </div>
            </div>
            <div class="row">
                <!-- start feature box item -->
                <div class="col-lg-4 col-md-12 feature-box1 sm-margin-20px-bottom wow fadeInRight animated" data-wow-delay="0.2s">
                    <span class="display-table-cell vertical-align-top"><i class="fas fa-desktop font-size26"></i></span>
                    <div class="width-80 margin-20px-left">
                        <span class="display-block alt-font text-uppercase letter-spacing-1 margin-10px-bottom">Feature One</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
                    </div>
                </div>
                <!-- end feature box item -->

                <!-- start feature box item -->
                <div class="col-lg-4 col-md-12 feature-box1 sm-margin-20px-bottom wow fadeInRight animated" data-wow-delay="0.4s">
                    <span class="display-table-cell vertical-align-top"><i class="fas fa-wrench font-size26"></i></span>
                    <div class="width-80 margin-20px-left">
                        <span class="display-block alt-font text-uppercase letter-spacing-1 margin-10px-bottom">Feature Two</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
                    </div>
                </div>
                <!-- end feature box item -->

                <!-- start feature box item-->
                <div class="col-lg-4 col-md-12 feature-box1 wow fadeInRight animated" data-wow-delay="0.6s">
                    <span class="display-table-cell vertical-align-top"><i class="fas fa-chart-pie font-size26"></i></span>
                    <div class="width-80 margin-20px-left">
                        <span class="display-block alt-font text-uppercase letter-spacing-1 margin-10px-bottom">Feature Three</span>
                        <p class="no-margin">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
                    </div>
                </div>
                <!-- end feature box item -->
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <!-- start slogan -->
                <div class="col-lg-4 col-md-6 col-sm-12 padding-two-all xs-padding-50px-tb xs-padding-20px-lr bg-black position-relative wow fadeInLeft" data-wow-delay="0.6s">
                    <div class="absolute-middle-center xs-absolute-middle-inherit width-80">
                        <i class="fas fa-quote-left font-size28 margin-15px-bottom text-pink-color"></i>
                        <span class="font-size28 xs-font-size22 alt-font text-uppercase letter-spacing-2 text-white display-block">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</span>
                    </div>
                </div>
                <!-- end slogan -->

                <!-- start our story video-->
                <div class="col-lg-4 col-md-6 col-sm-12 story-video cover-background wow fadeInLeft" data-wow-delay="0.4s" style="background-image:url('http://www.chitrakootweb.com/template/dicor/img/agency-skill.jpg');">
                    <div class="opacity-extra-medium bg-black"></div>
                    <div class="inner-border"></div>
                    <div class="text-center center-col absolute-middle-center">
                        <a class="video" href="http://www.chitrakootweb.com/template/dicor/video/video.mp4">
                            <i class="far fa-play-circle font-size24 text-white"></i>
                            <span class="alt-font text-white font-size22 margin-10px-left">Watch Our Story</span>
                        </a>
                    </div>
                </div>
                <!-- end our story video -->

                <!-- start skills-->
                <div class="col-lg-4 col-md-12 position-relative bg-light-gray">
                    <div class="skills absolute-middle-center sm-absolute-middle-inherit width-100 padding-five-tb padding-ten-lr sm-padding-50px-tb sm-no-padding-lr">

                        <span class="font-size16 font-weight-600 letter-spacing-2 text-uppercase black-text margin-20px-bottom display-block alt-font">Our Professional Skills</span>

                        <div class="prog-item">
                            <p>skill 1</p>
                            <div class="skills-progress"><span data-value='95%'></span></div>
                        </div>
                        <div class="prog-item">
                            <p>skill 2</p>
                            <div class="skills-progress"><span data-value='85%'></span></div>
                        </div>
                        <div class="prog-item">
                            <p>skill 3</p>
                            <div class="skills-progress"><span data-value='90%'></span></div>
                        </div>
                        <div class="prog-item">
                            <p>skill 4</p>
                            <div class="skills-progress"><span data-value='80%'></span></div>
                        </div>

                    </div>
                </div>
                <!-- end skills-->
            </div>
        </div>
    </section>
    <!-- End About Section-->

    <!--  Start Services Section -->
    <section class="services" data-scroll-index="2">
        <div class="container">
            <div class="row">
                <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center wow fadeInUp">
                    <div class="alt-font text-pink-color margin-5px-bottom text-uppercase text-small letter-spacing-1">Services</div>
                    <h5 class="margin-10px-bottom font-weight-300">We <span class="font-weight-500">Provide Faster</span> Service</h5>
                    <div class="separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                </div>
            </div>
            <div class="row">
                <!-- start services item -->
                <div data-wow-duration="0.4s" class="col-lg-3 col-md-6 margin-30px-bottom xs-margin-20px-bottom wow fadeInUp">
                    <div class="bg-white border border-width-2 border-color-extra-light-gray text-center padding-30px-tb xs-padding-15px-lr feature-box2">
                        <div class="display-inline-block margin-20px-bottom xs-margin-10px-bottom"><i class="fas fa-wrench font-size36"></i></div>
                        <div class="alt-font text-extra-dark-gray margin-10px-bottom">Service One</div>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-bottom margin-15px-top"></div>
                        <p class="width-75 md-width-90 xs-width-100 center-col no-margin-bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                    </div>
                </div>
                <!-- end services item -->

                <!-- start services item -->
                <div data-wow-duration="0.6s" class="col-lg-3 col-md-6 margin-30px-bottom xs-margin-20px-bottom wow fadeInUp">
                    <div class="bg-white border border-width-2 border-color-extra-light-gray text-center padding-30px-tb xs-padding-15px-lr feature-box2">
                        <div class="display-inline-block margin-20px-bottom xs-margin-10px-bottom"><i class="fas fa-cogs font-size36"></i></div>
                        <div class="alt-font text-extra-dark-gray margin-10px-bottom">Service Two</div>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-bottom margin-15px-top"></div>
                        <p class="width-75 md-width-90 xs-width-100 center-col no-margin-bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                    </div>
                </div>
                <!-- end services item -->

                <!-- start services item -->
                <div data-wow-duration="1.1s" class="col-lg-3 col-md-6 margin-30px-bottom xs-margin-20px-bottom wow fadeInUp">
                    <div class="bg-white border border-width-2 border-color-extra-light-gray text-center padding-30px-tb xs-padding-15px-lr feature-box2">
                        <div class="display-inline-block margin-20px-bottom xs-margin-10px-bottom"><i class="fas fa-mobile font-size36"></i></div>
                        <div class="alt-font text-extra-dark-gray margin-10px-bottom">Service Three</div>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-bottom margin-15px-top"></div>
                        <p class="width-75 md-width-90 xs-width-100 center-col no-margin-bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                    </div>
                </div>
                <!-- end services item -->

                <!-- start services item -->
                <div data-wow-duration="1.6s" class="col-lg-3 col-md-6 margin-30px-bottom xs-margin-20px-bottom wow fadeInUp">
                    <div class="bg-white border border-width-2 border-color-extra-light-gray text-center padding-30px-tb xs-padding-15px-lr feature-box2">
                        <div class="display-inline-block margin-20px-bottom xs-margin-10px-bottom"><i class="fas fa-ribbon font-size36"></i></div>
                        <div class="alt-font text-extra-dark-gray margin-10px-bottom">Service Four</div>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-bottom margin-15px-top"></div>
                        <p class="width-75 md-width-90 xs-width-100 center-col no-margin-bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                    </div>
                </div>
                <!-- end services item -->

                <!-- start services item -->
                <div data-wow-duration="0.4s" class="col-lg-3 col-md-6 sm-margin-30px-bottom xs-margin-20px-bottom wow fadeInUp">
                    <div class="bg-white border border-width-2 border-color-extra-light-gray text-center padding-30px-tb xs-padding-15px-lr feature-box2">
                        <div class="display-inline-block margin-20px-bottom xs-margin-10px-bottom"><i class="fab fa-pied-piper-hat font-size36"></i></div>
                        <div class="alt-font text-extra-dark-gray margin-10px-bottom">Service Five</div>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-bottom margin-15px-top"></div>
                        <p class="width-75 md-width-90 xs-width-100 center-col no-margin-bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                    </div>
                </div>
                <!-- end services item -->

                <!-- start services item -->
                <div data-wow-duration="0.6s" class="col-lg-3 col-md-6 sm-margin-30px-bottom xs-margin-20px-bottom wow fadeInUp">
                    <div class="bg-white border border-width-2 border-color-extra-light-gray text-center padding-30px-tb xs-padding-15px-lr feature-box2">
                        <div class="display-inline-block margin-20px-bottom xs-margin-10px-bottom"><i class="fas fa-code font-size36"></i></div>
                        <div class="alt-font text-extra-dark-gray margin-10px-bottom">Service Six</div>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-bottom margin-15px-top"></div>
                        <p class="width-75 md-width-90 xs-width-100 center-col no-margin-bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                    </div>
                </div>
                <!-- end services item -->

                <!-- start services item -->
                <div data-wow-duration="1.1s" class="col-lg-3 col-md-6 xs-margin-20px-bottom wow fadeInUp">
                    <div class="bg-white border border-width-2 border-color-extra-light-gray text-center padding-30px-tb xs-padding-15px-lr feature-box2">
                        <div class="display-inline-block margin-20px-bottom xs-margin-10px-bottom"><i class="fab fa-free-code-camp font-size36"></i></div>
                        <div class="alt-font text-extra-dark-gray margin-10px-bottom">Service Seven</div>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-bottom margin-15px-top"></div>
                        <p class="width-75 md-width-90 xs-width-100 center-col no-margin-bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                    </div>
                </div>
                <!-- end services item -->

                <!-- start services item -->
                <div data-wow-duration="1.6s" class="col-lg-3 col-md-6 wow fadeInUp">
                    <div class="bg-white border border-width-2 border-color-extra-light-gray text-center padding-30px-tb xs-padding-15px-lr feature-box2">

                        <div class="display-inline-block margin-20px-bottom xs-margin-10px-bottom"><i class="fab fa-pied-piper-hat font-size36"></i></div>

                        <div class="alt-font text-extra-dark-gray margin-10px-bottom">Service Eight</div>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-bottom margin-15px-top"></div>
                        <p class="width-75 md-width-90 xs-width-100 center-col no-margin-bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                    </div>
                </div>
                <!-- end services item -->
            </div>
        </div>
    </section>
    <!-- End Services Section -->

    <!--  Start Counter Section -->
    <div class="numbers bg-light-gray padding-60px-tb">
        <div class="container">
            <div class="row">
                <!-- start counter item -->
                <div class="col-lg-3 col-md-6 sm-margin-30px-bottom">
                    <div class="item text-center bounceIn animated">
                        <span class="icon"><i class="fab fa-pied-piper-hat"></i></span>
                        <h6 class="count">2200</h6>
                        <p>Happy Clients</p>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-top"></div>
                    </div>
                </div>
                <!-- end counter item -->

                <!-- start counter item -->
                <div class="col-lg-3 col-md-6 sm-margin-30px-bottom">
                    <div class="item text-center bounceIn animated">
                        <span class="icon"><i class="fab fa-pied-piper-hat"></i></span>
                        <h6 class="count">3816</h6>
                        <p>Projects Completed</p>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-top"></div>
                    </div>
                </div>
                <!-- end counter item -->

                <!-- start counter item -->
                <div class="col-lg-3 col-md-6">
                    <div class="item text-center xs-margin-30px-bottom bounceIn animated">
                        <span class="icon"><i class="fab fa-pied-piper-hat"></i></span>
                        <h6 class="count">14235</h6>
                        <p>Lines of Code</p>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-top"></div>
                    </div>
                </div>
                <!-- end counter item -->

                <!-- start counter item -->
                <div class="col-lg-3 col-md-6">
                    <div class="item text-center bounceIn animated">
                        <span class="icon"><i class="fab fa-pied-piper-hat"></i></span>
                        <h6 class="count">211</h6>
                        <p>Awarded Won</p>
                        <div class="separator-line-horrizontal-medium-light3 bg-pink center-col margin-15px-top"></div>
                    </div>
                </div>
                <!-- end counter item -->
            </div>
        </div>
    </div>
    <!-- End Counter Section -->

    <!-- Start Portfolio Section -->
    <section class="portfolio" data-scroll-index="3">
        <div class="container">
            <div class="row">
                <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center wow fadeInUp">
                    <div class="alt-font text-pink-color margin-5px-bottom text-uppercase letter-spacing-1">Portfolio</div>
                    <h5 class="margin-10px-bottom font-weight-300">Our <span class="font-weight-500">Free Calculator</span></h5>
                    <div class="separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                </div>
            </div>
            <div class="row">
                <!-- Start links -->
               
                <!-- End links -->
                <div class="clearfix"></div>
                <!-- gallery -->
                <div class="gallery text-center width-100">
                    <!-- start gallery item -->
                    <div class="col-md-4 items design">
                        <div class="item-img">
                            <img src="http://www.chitrakootweb.com/template/dicor/img/portfolio/1.jpg" alt="image">
                            <div class="item-img-overlay valign">
                                <div class="overlay-info width-100 vertical-center">
                                    <h6>Calculator mockup</h6>
                                    <p>Some description here</p>
                                    <a href="http://www.chitrakootweb.com/template/dicor/img/portfolio/1.jpg" class="popimg">
                                        <i class="fa fa-search-plus"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end gallery item -->
                    <!-- start gallery item -->
                    <div class="col-md-8 items design">
                        <div class="item-img">
                            <img src="http://www.chitrakootweb.com/template/dicor/img/portfolio/2.jpg" alt="image">
                            <div class="item-img-overlay valign">
                                <div class="overlay-info width-100 vertical-center">
                                     <h6>Calculator mockup</h6>
                                    <p>Some description here</p>
                                    <a href="http://www.chitrakootweb.com/template/dicor/img/portfolio/2.jpg" class="popimg">
                                        <i class="fa fa-search-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end gallery item -->
                    <!-- start gallery item -->
                   
                  
                    <div class="clear-fix"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Portfolio Section -->

    <!-- Start Testimonials & Faq's Section -->
    <div class="bg-light-gray">
        <div class="container-fluid">
            <div class="row">
                <!-- start testimonials -->
                <div class="col-lg-6 col-md-12 background-position-top center-col testimonials text-center cover-background" style="background-image: url(http://www.chitrakootweb.com/template/dicor/img/3.jpg);">
                    <div class="opacity-full-dark bg-extra-dark-gray"></div>
                    <div class="padding-ten-all sm-padding-50px-tb sm-no-padding-lr  position-relative">
                        <div class="wow fadeInUp">
                            <div class="alt-font text-pink-color margin-5px-bottom text-uppercase letter-spacing-1">Testimonials</div>
                            <h5 class="font-weight-300 text-white margin-10px-bottom">What <span class="font-weight-500">Our Client</span> Say</h5>
                            <div class="margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                        </div>
                        <div class="owl-carousel owl-theme">
                            <!-- start testimonials item-->
                            <div class="citem">
                                <i class="fas fa-quote-left font-size22 margin-40px-bottom text-pink-color"></i>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
                                <div class="separator-line-verticle-extra-large bg-pink center-col margin-30px-tb"></div>
                                <h4>Jama Karle</h4>
                                <h6>CEO</h6>
                            </div>
                            <!-- end testimonials item-->

                            <!-- start testimonials item-->
                            <div class="citem">
                                <i class="fas fa-quote-left font-size22 margin-40px-bottom text-pink-color"></i>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
                                <div class="separator-line-verticle-extra-large bg-pink center-col margin-30px-tb"></div>
                                <h4>Ivonne Drennan</h4>
                                <h6>Manager</h6>
                            </div>
                            <!-- end testimonials item-->

                            <!-- start testimonials item-->
                            <div class="citem">
                                <i class="fas fa-quote-left font-size22 margin-40px-bottom text-pink-color"></i>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
                                <div class="separator-line-verticle-extra-large bg-pink center-col margin-30px-tb"></div>
                                <h4>Bettye Mauck</h4>
                                <h6>Designer</h6>
                            </div>
                            <!-- end testimonials item-->
                        </div>
                    </div>
                </div>
                <!-- end testimonials -->

                <!-- start faq's -->
                <div class="col-lg-6 col-md-12">
                    <div class="padding-ten-all sm-padding-50px-tb sm-no-padding-lr text-center">
                        <div class="wow fadeInUp">
                            <div class="alt-font text-pink-color margin-5px-bottom text-uppercase letter-spacing-1">Faq's</div>
                            <h5 class="font-weight-300 margin-10px-bottom">Can <span class="font-weight-500">We Help</span> You ?</h5>
                            <div class="margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                        </div>
                        <div id="Dicorion" class="Dicorion-style">
                            <div class="card wow fadeInUp">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                 FAQ Sample Question Details ?
                                </button>
                              </h5>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#Dicorion">
                                    <div class="card-body">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                    </div>
                                </div>
                            </div>
                            <div class="card wow fadeInUp">
                                <div class="card-header" id="headingTwo">
                                    <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                     FAQ Sample Question Details ?
                                </button>
                              </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#Dicorion">
                                    <div class="card-body">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                    </div>
                                </div>
                            </div>
                            <div class="card wow fadeInUp">
                                <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    FAQ Sample Question Details ?
                                </button>
                              </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#Dicorion">
                                    <div class="card-body">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummyLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end faq's -->
            </div>

        </div>
    </div>
    <!-- End Testimonials & Faq's Section -->

    <!-- Start Team Section -->
    <section class="team">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 col-sm-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center last-paragraph-no-margin wow fadeInUp">
                    <div class="alt-font text-pink-color margin-5px-bottom text-uppercase letter-spacing-1">Our Team</div>
                    <h5 class="margin-10px-bottom font-weight-300">Meet <span class="font-weight-500">Our Expert</span> Minds</h5>
                    <div class="separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                </div>
            </div>
            <div class="row">
                <div class="owl-carousel owl-theme col-sm-12 team-style">
                    <!-- start team member -->
                    <div class="xs-margin-20px-bottom text-center">
                        <div class="team-member-img">
                            <img class="img-responsive" src="http://www.chitrakootweb.com/template/dicor/img/team/team-02.jpg" alt="">
                        </div>
                        <div class="text-center margin-25px-top margin-15px-bottom  padding-15px-bottom border-bottom">
                            <div class="text-extra-dark-gray font-weight-600 text-uppercase font-size14 alt-font">Jamara Karle</div>
                            <div class="text-uppercase font-size11">Founder</div>
                        </div>
                        <div class="social-links">
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-facebook-f text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-twitter text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-google-plus-g text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block border-radius-100" href="javascript:void(0);"><i class="fab fa-instagram text-medium-gray text-medium"></i></a>
                        </div>
                    </div>
                    <!-- end team member -->

                    <!-- start team member -->
                    <div class="xs-margin-20px-bottom text-center">
                        <div class="team-member-img">
                            <img class="img-responsive" src="http://www.chitrakootweb.com/template/dicor/img/team/team-01.jpg" alt="">
                        </div>
                        <div class="text-center margin-25px-top margin-15px-bottom  padding-15px-bottom border-bottom">
                            <div class="text-extra-dark-gray font-weight-600 text-uppercase font-size14 alt-font">Keir Prestonly</div>
                            <div class="text-uppercase font-size11">Co-Founder</div>
                        </div>
                        <div class="social-links">
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-facebook-f text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-twitter text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-google-plus-g text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block border-radius-100" href="javascript:void(0);"><i class="fab fa-instagram text-medium-gray text-medium"></i></a>
                        </div>
                    </div>
                    <!-- end team member -->

                    <!-- start team member -->
                    <div class="xs-margin-20px-bottom text-center">
                        <div class="team-member-img">
                            <img class="img-responsive" src="http://www.chitrakootweb.com/template/dicor/img/team/team-03.jpg" alt="">
                        </div>
                        <div class="text-center margin-25px-top margin-15px-bottom  padding-15px-bottom border-bottom">
                            <div class="text-extra-dark-gray font-weight-600 text-uppercase font-size14 alt-font">Finley Walkeror</div>
                            <div class="text-uppercase font-size11">Developer</div>
                        </div>
                        <div class="social-links">
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-facebook-f text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-twitter text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-google-plus-g text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block border-radius-100" href="javascript:void(0);"><i class="fab fa-instagram text-medium-gray text-medium"></i></a>
                        </div>
                    </div>
                    <!-- end team member -->

                    <!-- start team member -->
                    <div class="xs-margin-20px-bottom text-center">
                        <div class="team-member-img">
                            <img class="img-responsive" src="http://www.chitrakootweb.com/template/dicor/img/team/team-04.jpg" alt="">
                        </div>
                        <div class="text-center margin-25px-top margin-15px-bottom  padding-15px-bottom border-bottom">
                            <div class="text-extra-dark-gray font-weight-600 text-uppercase font-size14 alt-font">Niamah Hower</div>
                            <div class="text-uppercase font-size11">Designer</div>
                        </div>
                        <div class="social-links">
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-facebook-f text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-twitter text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block margin-10px-right" href="javascript:void(0);"><i class="fab fa-google-plus-g text-medium-gray text-medium"></i></a>
                            <a class="d-inline-block border-radius-100" href="javascript:void(0);"><i class="fab fa-instagram text-medium-gray text-medium"></i></a>
                        </div>
                    </div>
                    <!-- end team member -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Team Section -->

    <!-- Start Price Section -->
    <section class="price bg-light-gray" data-scroll-index="4">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 col-sm-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center last-paragraph-no-margin wow fadeInUp">
                    <div class="alt-font text-pink-color margin-5px-bottom text-uppercase letter-spacing-1">Price</div>
                    <h5 class="margin-10px-bottom font-weight-300">Our <span class="font-weight-500">Standard</span> Price</h5>
                    <div class="separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                </div>
            </div>
            <div class="row">
                <!-- start table -->
                <div class="col-lg-4 sm-margin-30px-bottom wow fadeInRight" data-wow-delay="0.2s">
                    <div class="item text-center">
                        <div class="type">
                            <span class="icon"><i class="icofont icofont-briefcase"></i></span>
                            <h4>Basic</h4>
                        </div>
                        <div class="value">
                            <h3>10<span>$</span></h3>
                            <span class="per">Per Month</span>
                        </div>
                        <div class="features">
                            <ul>
                                <li>24/7 Tech Support</li>
                                <li>Advanced Options</li>
                                <li>1GB Storage</li>
                                <li>6GB Bandwidth</li>
                                <li>Unlimited Support</li>
                            </ul>
                        </div>
                        <div class="order">
                            <a href="javascript:void(0);" class="btn">Purchase Now</a>
                        </div>
                    </div>
                </div>
                <!-- end table -->

                <!-- start table -->
                <div class="col-lg-4 sm-margin-30px-bottom wow fadeInRight" data-wow-delay="0.4s">
                    <div class="item text-center active">
                        <div class="type">
                            <span class="icon"><i class="icofont icofont-rocket-alt-2"></i></span>
                            <h4>Standard</h4>
                        </div>
                        <div class="value">
                            <h3>21<span>$</span></h3>
                            <span class="per">Per Month</span>
                        </div>
                        <div class="features">
                            <ul>
                                <li>24/7 Tech Support</li>
                                <li>Advanced Options</li>
                                <li>1.5GB Storage</li>
                                <li>3GB Bandwidth</li>
                                <li>Unlimited Support</li>
                            </ul>
                        </div>
                        <div class="order">
                            <a href="javascript:void(0);" class="btn">Purchase Now</a>
                        </div>
                    </div>
                </div>
                <!-- end table -->

                <!-- start table -->
                <div class="col-lg-4 wow fadeInRight" data-wow-delay="0.6s">
                    <div class="item text-center">
                        <div class="type">
                            <span class="icon"><i class="icofont icofont-diamond"></i></span>
                            <h4>Premium</h4>
                        </div>
                        <div class="value">
                            <h3>99<span>$</span></h3>
                            <span class="per">Per Month</span>
                        </div>
                        <div class="features">
                            <ul>
                                <li>24/7 Tech Support</li>
                                <li>Advanced Options</li>
                                <li>6GB Storage</li>
                                <li>12GB Bandwidth</li>
                                <li>Unlimited Support</li>
                            </ul>
                        </div>
                        <div class="order">
                            <a href="javascript:void(0);" class="btn">Purchase Now</a>
                        </div>
                    </div>
                </div>
                <!-- end table -->
            </div>
        </div>
    </section>
    <!-- End Price Section -->

    <!-- Start Blog Section -->
    <section class="blog" data-scroll-index="5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 col-sm-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center last-paragraph-no-margin wow fadeInUp">
                    <div class="alt-font text-pink-color margin-5px-bottom text-uppercase letter-spacing-1">Blog</div>
                    <h5 class="margin-10px-bottom font-weight-300">Our <span class="font-weight-500">Recent</span> News</h5>
                    <div class="separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                </div>
            </div>
            <div class="row">
                <!-- start blog -->
                <div class="col-lg-4 sm-margin-30px-bottom wow fadeInUp" data-wow-delay="0.2 s">
                    <div class="item text-center">
                        <div class="post-img">
                            <img src="http://www.chitrakootweb.com/template/dicor/img/blog/1.jpg" alt="">
                        </div>
                        <div class="content">
                            <span class="tag alt-font">
                                    <a href="http://www.chitrakootweb.com/template/dicor/blog.html">Designing</a>
                                </span>
                            <h5><a href="http://www.chitrakootweb.com/template/dicor/single-post.html">Blog post title sample</a></h5>
                            <div class="author">
                                <span class="font-size11 display-inline-block">by <a href="javascript:void(0);">Jay Benjamin</a>&nbsp;&nbsp;|&nbsp;&nbsp;02 May 2018</span>
                            </div>
                            <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy ... </p>
                            <a href="http://www.chitrakootweb.com/template/dicor/single-post.html" class="read-more">Read more</a>
                        </div>
                    </div>
                </div>
                <!-- end blog -->

                <!-- start blog -->
                <div class="col-lg-4 sm-margin-30px-bottom wow fadeInUp" data-wow-delay="0.4s">
                    <div class="item text-center">
                        <div class="post-img">
                            <img src="http://www.chitrakootweb.com/template/dicor/img/blog/2.jpg" alt="">
                        </div>
                        <div class="content">
                            <span class="tag alt-font">
                                    <a href="http://www.chitrakootweb.com/template/dicor/blog.html">Business</a>
                                </span>
                            <h5><a href="http://www.chitrakootweb.com/template/dicor/single-post.html">Blog post title sample</a></h5>
                            <div class="author">
                                <span class="font-size11 display-inline-block">by <a href="javascript:void(0);">Jay Benjamin</a>&nbsp;&nbsp;|&nbsp;&nbsp;11 April 2018</span>
                            </div>
                            <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy ...</p>
                            <a href="http://www.chitrakootweb.com/template/dicor/single-post.html" class="read-more">Read more</a>
                        </div>
                    </div>
                </div>
                <!-- end blog -->

                <!-- start blog -->
                <div class="col-lg-4 wow fadeInUp" data-wow-delay="0.6s">
                    <div class="item text-center">
                        <div class="post-img">
                            <img src="http://www.chitrakootweb.com/template/dicor/img/blog/3.jpg" alt="">
                        </div>
                        <div class="content">
                            <span class="tag alt-font">
                                    <a href="http://www.chitrakootweb.com/template/dicor/blog.html">Camping</a>
                                </span>
                            <h5><a href="http://www.chitrakootweb.com/template/dicor/single-post.html">Blog post title sample</a></h5>
                            <div class="author">
                                <span class="font-size11 display-inline-block">by <a href="javascript:void(0);">Jay Benjamin</a>&nbsp;&nbsp;|&nbsp;&nbsp;04 March 2018</span>
                            </div>
                            <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy... </p>
                            <a href="http://www.chitrakootweb.com/template/dicor/single-post.html" class="read-more">Read more</a>
                        </div>
                    </div>
                </div>
                <!-- end blog -->
            </div>
        </div>
    </section>
    <!-- End Blog Section -->

    <!-- Start Contact Section -->
    <section class="contact bg-light-gray no-padding" data-scroll-index="6">
        <div class="container-fluid">
            <div class="row">
                <!-- start quickly contact -->
                <div class="col-lg-6 map">
                    <div id="ieatmaps"></div>
                    <div class="padding-ten-all sm-padding-50px-tb sm-no-padding-lr info">
                        <div class="wow fadeInUp">
                            <div class="alt-font text-pink-color margin-5px-bottom text-uppercase  text-center letter-spacing-1">Location</div>
                            <h5 class="font-weight-300 margin-10px-bottom text-center text-white">Quickly <span class="font-weight-500">Contact</span> Us</h5>
                            <div class="margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                        </div>
                        <span class="icon-toggle"><i class="fas fa-toggle-on"></i></span>
                        <div class="item wow fadeInUp" data-wow-delay="0.2s">
                            <span class="icon"><i class="icon-phone"></i></span>
                            <div class="cont">
                                <h6 class="text-white">Phone: </h6>
                                <p>(+44) 123 456 789</p>
                            </div>
                        </div>
                        <div class="item wow fadeInUp" data-wow-delay="0.4s">
                            <span class="icon"><i class="icon-map"></i></span>
                            <div class="cont">
                                <h6 class="text-white">Address: </h6>
                                <p>3389 Eglinton Avenue, Windermere, </p>
                                <p>Regina ST, London, SK 8GH.</p>
                            </div>
                        </div>
                        <div class="item wow fadeInUp" data-wow-delay="0.6s">
                            <span class="icon"><i class="icon-envelope"></i></span>
                            <div class="cont">
                                <h6 class="text-white">Email: </h6>
                                <p>email@youradress.com</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end quickly contact -->
                <!-- start get in touch -->
                <div class="col-lg-6">
                    <div class="padding-ten-all sm-padding-50px-tb sm-no-padding-lr">
                        <div class="wow fadeInUp">
                            <div class="alt-font text-pink-color margin-5px-bottom text-uppercase  text-center letter-spacing-1">Contact</div>
                            <h5 class="font-weight-300 margin-10px-bottom text-center">Get <span class="font-weight-500">In Touch</span> Possible</h5>
                            <div class="margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom separator-line-horrizontal-medium-light2 bg-pink center-col"></div>
                        </div>
                        <div class="no-margin-lr" id="success-contact-form" style="display: none;"></div>
                        <form id="contactForm" method="post" class="contact-form wow fadeInUp" data-wow-duration="0.6s" action="http://www.chitrakootweb.com/template/dicor/sendemail.php">
                            <div class="row">
                                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                                    <input type="text" class="medium-input" maxlength="50" placeholder="Name *" required="required" id="name" name="name">
                                </div>
                                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                                    <input type="email" class="medium-input" maxlength="70" placeholder="E-mail *" required="required" id="email" name="email">
                                </div>
                                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                                    <input type="tel" class="medium-input" maxlength="10" placeholder="Phone" id="phone" name="phone">
                                </div>
                                <div class="col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                                    <input type="text" class="medium-input" maxlength="78" placeholder="Subject *" required="required" id="subject" name="subject">
                                </div>
                                <div class="col-md-12 wow fadeInUp" data-wow-delay="0.6s">
                                    <textarea class="big-textarea" rows="6" maxlength="1000" placeholder="Message *" required="required" id="message" name="message"></textarea>
                                </div>
                                <div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.8s">
                                    <button class="btn" type="submit">send message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- end get in touch -->
            </div>
        </div>
    </section>
    <!-- End Contact Section -->

    <!-- Start Clients Section-->
    <div class="clients text-center border-bottom border-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-sm-6 fadeInUp animated">
                    <div class="brand">
                        <img src="http://www.chitrakootweb.com/template/dicor/img/clients-logo/1.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 fadeInUp animated">
                    <div class="brand">
                        <img src="http://www.chitrakootweb.com/template/dicor/img/clients-logo/2.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 fadeInUp animated">
                    <div class="brand">
                        <img src="http://www.chitrakootweb.com/template/dicor/img/clients-logo/3.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 fadeInUp animated">
                    <div class="brand">
                        <img src="http://www.chitrakootweb.com/template/dicor/img/clients-logo/4.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 fadeInUp animated">
                    <div class="brand">
                        <img src="http://www.chitrakootweb.com/template/dicor/img/clients-logo/5.png" alt="">
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 fadeInUp animated">
                    <div class="brand">
                        <img src="http://www.chitrakootweb.com/template/dicor/img/clients-logo/6.png" alt="">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- End Clients Section -->

    <!--  Start Footer Section -->
    <footer class="bg-white">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <!-- start footer logo -->
                    <div class="col-md-12 col-sm-12 text-center wow fadeInUp">
                        <div class="text-center">
                            <a href="javascript:void(0);"><img src="img/sample-logo.png" alt="Dicor - Creative One Page Multipurpose Template" title="Dicor - Creative One Page Multipurpose Template" width="180" height="38"></a>
                        </div>
                        <span class="font-size11 alt-font text-uppercase  display-block letter-spacing-1 text-dark-gray margin-15px-top margin-30px-bottom xs-margin-fifteen">Creative One Page Multipurpose Template</span>
                    </div>
                    <!-- end footer logo -->
                    <!-- start social media link -->
                    <div class="col-md-12 col-sm-12 text-center wow fadeInUp">
                        <div class="footer-social">
                            <a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a>
                            <a href="javascript:void(0);"><i class="fab fa-twitter"></i></a>
                            <a href="javascript:void(0);"><i class="fab fa-google-plus-g"></i></a>
                            <a href="javascript:void(0);"><i class="fab fa-dribbble"></i></a>
                            <a href="javascript:void(0);"><i class="fab fa-youtube"></i></a>
                            <a href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a>
                        </div>
                    </div>
                    <!-- end social media link -->
                </div>
            </div>
        </div>
        <div class="footer-bottom bg-black">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center">
                        <span class="font-size12 text-uppercase letter-spacing-1">
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div id="back-to-top" class="back-to-top reveal">
            <i class="fa fa-angle-up"></i>
        </div>
    </footer>
    <!-- End Footer Section -->

    <!-- jQuery -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/jquery-3.0.0.min.js"></script>
    <script src="http://www.chitrakootweb.com/template/dicor/js/jquery-migrate-3.0.0.min.js"></script>

    <!-- popper.min -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/popper.min.js"></script>

    <!-- bootstrap -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/bootstrap.min.js"></script>

    <!-- scrollIt -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/scrollIt.min.js"></script>

    <!-- animated.headline -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/animated.headline.js"></script>

    <!-- wow -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/wow.js"></script>

    <!-- jquery.waypoints.min -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/jquery.waypoints.min.js"></script>

    <!-- jquery.counterup.min -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/jquery.counterup.min.js"></script>

    <!-- owl carousel -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/owl.carousel.min.js"></script>

    <!-- jquery.magnific-popup js -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/jquery.magnific-popup.min.js"></script>

    <!-- stellar js -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/jquery.stellar.min.js"></script>

    <!-- isotope.pkgd.min js -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/isotope.pkgd.min.js"></script>

    <!-- particles.min js -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/particles.min.js"></script>

    <!-- app js -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/app.js"></script>

    <!-- Map -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/map.js"></script>

    <!-- custom scripts -->
    <script src="http://www.chitrakootweb.com/template/dicor/js/scripts.js"></script>

    <!-- google map api -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIy8l8gSJaMoUvCZJv8AyCAw3CU07nCKs&amp;callback=initMap">
    </script>

</body>


</html>