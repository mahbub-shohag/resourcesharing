<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<script language="javascript">


</script>

<head>    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="It's an Saas based ERP" />
    <meta name="keywords" content="ERP, Calculator, Monthly Subscription" />   
    <title>Welcome to ProjectName</title>
   
    <link rel="shortcut icon" type="image/ico" href="img/favicon.png" />

   
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/normalize.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/animate.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/modal-video.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/stellarnav.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/owl.carousel.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/slick.css">

    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap.min.css"> 
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/font-awesome.min.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/material-icons.css">
    
    

    <!--====== MAIN STYLESHEETS ======-->
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/responsive.css">
    

    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/modernizr-2.8.3.min.js" ></script>
   
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

     <style type="text/css">
              .price-details li {
                font-size: 18px;
                padding: 9px;
   
            }
          .price-details {
             min-height: 481px;
          }
         section#contact {
             background: #fff;
          }
         .basic_cal{
             font-family: "Open Sans", sans-serif;
             font-style:normal;
             font-weight: 400;
             line-height: 1.6em;

             font-size: 14px;
             color: #6F6F6F;
         }



     </style>



</head>

<body class="home-one" data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#home" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header class="top-area" id="home">
        <div class="header-top-area">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a href="#home" class="navbar-brand">

                                <?php 
                                    echo img('img/logoCopy.png');
                                ?>
                                
                            </a>
                        </div>
                        <div id="main-nav" class="stellarnav">
                            <div class="search-and-signup-button white pull-right hidden-md hidden-sm hidden-xs">
                                <button data-toggle="collapse" data-target="#search-form-switcher"><i class="fa fa-search"></i></button>
                                <a href="<?php echo base_url(); ?>auth/signin" class="sign-up">Sign in</a>
                            </div>
                            <ul id="nav" class="nav">
                                <li class="active"><a href="#home">home</a></li>
                                <li><a href="#about">About</a></li>
                                <li><a href="#howit">How It Works</a></li>
                                <li><a href="#pricing">Pricing</a></li>
                                <li><a href="#free-calculator">Calculator</a></li>
                                <li><a href="#whatnext">What Next</a></li>
                                <li><a href="#contact">Contact Us</a></li>                                
                            </ul>
                        </div>
                    </div>
                </nav>
                <div id="search-form-switcher" class="search-collapse-area collapse white">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="white">
                                    <form action="#" class="search-form">
                                        <input type="search" name="search" id="search" placeholder="Search Here..">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END MAINMENU AREA END-->
        </div>
        <div class="welcome-text-area white">
            <div class="area-bg"></div>
            <div class="welcome-area">
                <div class="container">
                    <div class="row flex-v-center">
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                            <div class="welcome-mockup center">
                               <!-- <img src="img/home/watch-mockup.png" alt="">-->
                            </div>
                        </div>
                        <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                            <div class="welcome-text">
                                <h1>Welcome To Company Name.</h1>
                                <p>A ship has several types of tanks and for different functions: ballast, fresh water, fuel, oil, slope, drain… We want to become one tank more for your ship: your virtual tank, to manage and securely store all your ship activities. </p>
                                <div class="home-button">
                                    <a href="<?php echo base_url(); ?>auth/signup">Sign Up</a>
                                    <a href="<?php echo base_url(); ?>calculator/basic_calculator">Try Free Calculator</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--END TOP AREA-->

    <!--FEATURES TOP AREA-->
    <section class="features-top-area padding-100-50" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>About</h2>
                        <span class="icon-and-border"><i class="material-icons">description</i></span>
                        <p>We know that the high seas can be a hectic workplace. 
That’s why our software is designed to improve efficiency without complicating the day to day operations of your team.
</p>
<p>We’re focused on file storage, so you don't have to be. Our cutting-edge enterprise resource planning and online calculators are built to save you time and money.
</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="qs-box relative mb50 center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="qs-box-icon">
                            <i class="material-icons">cloud_done</i>
                        </div>
                        <h3 style="text-transform:capitalize">Safe, Secure & Reliable 24/7 Cloud Storage</h3>
                        <!-- <p>SAFE, SECURE & RELIABLE 24/7 CLOUD STORAGE </p>
                        <a href="#" class="read-more">Learn More</a> -->
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="qs-box relative mb50 center  wow fadeInUp" data-wow-delay="0.3s">
                        <div class="qs-box-icon">
                            <i class="material-icons">important_devices</i>
                        </div>
                        <h3 style="text-transform:capitalize;">Working to get you new and exciting maritime software platforms</h3>
                      <!--   <p>It is our goal to help make sure your operations run smoothly, safely and securely.</p>
                        <a href="#" class="read-more">Learn More</a> -->
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <div class="qs-box relative mb50 center  wow fadeInUp" data-wow-delay="0.4s">
                        <div class="qs-box-icon">
                            <i class="material-icons">language</i>
                        </div>
                        <h3 style="text-transform:capitalize">Designed Specifically for the Different Actors in the Maritime Business</h3>
                        <!-- <p>DESIGNED SPECIFICALLY FOR THE MARITIME INDUSTRY</p>
                        <a href="#" class="read-more">Learn More</a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FEATURES TOP AREA END-->



    <!--WORK AREA-->
    <section class="work-area section-padding sky-gray-bg" id="howit">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>How it <span>works</span></h2>
                        <span class="icon-and-border"><i class="material-icons">hourglass_empty</i></span>
                        <p>SIMPLITY AND EFFICIENCY FOR THE MARITIME INDUSTRY. Our software is designed specifically to improve efficiency without complicating the operations of the ship agents, ship owners, traders, shippers, receivers and other logistics professionals in the maritime industry who rely on us.</p>
                    </div>
                </div>
            </div>
            <div class="row flex-v-center">
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                    <div class="qs-box right mb100 padding-30 wow fadeIn">
                        <div class="line-border"></div>
                        <h4>AN EASY TO USE STORAGE</h4>
                        <p>You can save your Statement of Facts, Bills of Lading, Laytime calculations…you name it!. You simply upload files into [Company] where only you have access. You can also authorize access to other users to your files. </p>
                    </div>
                    <div class="qs-box right mb100 padding-30 wow fadeIn">
                        <div class="line-border"></div>
                        <h4>REAL TIME ITINERARY</h4>
                        <p>[Company] makes it easy to update and collaborate the itinerary of your ship with your colleagues or clients . Just update the last ETA, ETB or other dates important dates and your designated users will  be notified of the changes instantly. </p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 ">
                    <div class="service-image text-center wow fadeIn">
                       <!-- <img src="img/mockups/work-area-mockup.png" alt=""> -->
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 pull-left">
                    <div class="qs-box left mt100 padding-30 wow fadeIn">
                        <div class="line-border"></div>
                        <h4>LAYTIME CALCULATOR</h4>
                        <p>IInside [Company] you will have access to our full online laytime calculator; and you can save your calculations within the system, print on paper or create it as a pdf that you can save in [Company]. You can also share your calculation or Pdf with other [Company] users</p>
                    </div>
                    <div class="qs-box left mt100 padding-30 wow fadeIn">
                        <div class="line-border"></div>
                        <h4>FRIENDLY DATABASE</h4>
                        <p>You create a folder for your ship to store documents and laytimes. You will find  all these folders in a convenient and friendly way so you can search and sort through records just like in a phonebook.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--WORK AREA END-->


    <!--PRICING AREA-->
    <section class="price-area padding-100-70" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                        <h2>Pricing <span>Table</span></h2>
                        <span class="icon-and-border"><i class="material-icons">monetization_on</i></span>
                       
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-price center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="price-hidding">
                            <h2>Try Our <br/> Free Calculator</h2>
                        </div>
                        <div class="price-rate">
                            <h2><sup>$</sup>0.0
                                <sub>Unlimited</sub>
                            </h2>
                        </div>
                        <div class="price-details">
                            <ul>
                                <li>*In our basic free calculator, you can calculate laytime for free up to five exceptions.</li>
                                <li>*Print in pdf format.</li>
                                <li><strong>You don't need to sign up, just go <a href="<?php echo base_url(); ?>calculator/basic_calculator">here</a> to use it!</strong></li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-price center wow fadeInUp" data-wow-delay="0.3s">
                        <div class="price-hidding">
                            <h2>The Monthly Subscription</h2>
                        </div>
                        <div class="price-rate">
                            <h2><sup>$</sup>15
                                <sub>Unlimited</sub>
                            </h2>
                        </div>
                        <div class="price-details">
                            <ul>
                                <li>*Store your shipping documents
                                <li>*Keep track of your ships eta, etb, ets</li> 
                                <li>*Share documents or data to colleagues or clients on real time</li> 
                                <li>*Calculate laytime with full features</li> 
                                <li>*Save your laytimes in the cloud, or pdf, excel and share with others</li> 
                                <li>*See the turnaround time of your ships to set you up for success tomorrow</li>     
                            </ul>

                         <div class="buy-now-button">
                           <a href="<?php echo base_url(); ?>auth/signup" class="read-more">Sign Up</a>
                        </div>
                        </div>
                       
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-price center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="price-hidding">
                    <h2>More coming..</h2><br/><br/>
                </div>
                <div class="price-rate">
                    <h2><sup>$</sup>0.0
                        <sub>Unlimited</sub>
                    </h2>
                </div>
                
                <div class="price-details"> 
                    <ul>
                        <li>We are working in more features and positions which will come soon</li>                
                    </ul>
                </div>
                

             

                    </div>
                </div>
             
            </div>
        </div>
    </section>
    <!--PRICING AREA END-->

    <!--DOWNLOAD AREA-->
    <section class="download-area section-padding  relative sky-gray-bg" id="free-calculator">
        <div class="area-bg1" data-stellar-background-ratio="0.6"></div>
        <div class="container">
           
            <div class="row">
                 <div class="area-title text-center wow fadeIn">
                        <h2>Our <span>  Free LAYTIME Calculator</span></h2>
                        <span class="icon-and-border"><i class="material-icons">monetization_on</i></span>
                       
                    </div>


                
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                     <h3 class="text-center"><a href="<?php echo base_url(); ?>calculator/basic_calculator" target="_blank">TRY OUR ONLINE LAYTIME CALCULATOR FOR FREE TODAY</a> </h3>
                      <h3 class="basic_cal">Just enter your basic SOF information and calculate your laytime FREE. To unlock the full version of the calculator, and all our other great tools, <a href="<?php echo base_url(); ?>auth/signup">sign up </a>here
</h3>
                </div>

                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                     <a href="<?php echo base_url(); ?>calculator/basic_calculator"><img class="basic_calculator" src="<?php echo base_url(); ?>img/basic_calculator.JPG"></a>
                </div>



            </div>


<style type="text/css">
    .basic_calculator {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
}

h3.text-center.basic_cal {
    padding-top: 150px;
}

@media only screen and (max-width: 480px) {
  h3.text-center.basic_cal{padding-top: 35px!important}
}
</style>
           
        </div>
    </section>
    <!--DOWNLOAD AREA END-->

    <!--WHAT NEXT AREA-->

       <section class="download-area section-padding  relative sky-gray-bg1" id="whatnext">
        <div class="area-bg1" data-stellar-background-ratio="0.6"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center wow fadeIn">
                       <h2>What <span> Next</span></h2>
                        <span class="icon-and-border"><i class="material-icons">monetization_on</i></span>
                         <p>At [COMPANY], we are constantly working towards new and exciting improvements.

Periodically we will launch new features so be sure to check back regularly and follow us on social media for all the latest company news.
</p>
                    </div>
                </div>
            </div>
           

        </div>
    </section>




    <!--CONTACT US AREA-->
    <section class="contact-area relative padding-100-50" id="contact">
        <div class="area-bg"></div>
        <div class="contact-form-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-7 col-lg-offset-7 col-md-5 col-lg-5 col-sm-12 col-xs-12">
                        <div class="contact-form mb50 wow fadeIn">
                            
                            <div id="send_message" class="alert alert-success" style="display: none">                        
                            </div>



                            <h2>GET IN TOUCH</h2>
                            <form id="contact-form1" method="post">
                                <div class="form-group" id="name-field">
                                    <div class="form-input">
                                        <style type="text/css">
                                            #name_error_msg,#email_error_msg,#msg_error_msg,#captcha_error_msg{
                                                display: none;
                                                color:red;

                                            }
                                        </style>
                                        <p class="error_class" id="name_error_msg">Name can't be empty!</p>
                                        <input type="text" class="form-control" id="name" name="form-name" placeholder="Name.." required>
                                    </div>
                                </div>
                                <div class="form-group" id="email-field">
                                    <div class="form-input">
                                        <p class="error_class" id="email_error_msg">Email can't be empty!</p>
                                        <input type="email" class="form-control" id="email" name="form-email" placeholder="Email.." required>
                                    </div>
                                </div>
                                <div class="form-group" id="phone-field">
                                    <div class="form-input">
                                       
                                        <input type="text" class="form-control" id="subject" name="form-phone" placeholder="Subject..">
                                    </div>
                                </div>
                                <div class="form-group" id="message-field">
                                    <p class="error_class" id="msg_error_msg">Message can't be empty!</p>
                                     <div class="form-input">                                     
                                        <textarea class="form-control" rows="6" id="message" name="form-message" placeholder="Your Message Here..." required></textarea>
                                    </div>
                                </div>
                              



                                <div class="captcha-block">
                                  <div class="captcha-bottom">
                                    <span class="pull-left">
                                    	<p class="error_class" id="captcha_error_msg">Captcha can't be empty!</p>
                                      <input type="text" placeholder="Enter Code" id="captcha" name="captcha" class="inputcaptcha" required="required">
                                    </span>                                   
                                      <img src="<?php echo base_url(); ?>auth/demo_captcha" class="imgcaptcha" alt="captcha" style="width: 49%;" />
                                      <img src="<?php echo base_url(); ?>img/refresh.png" alt="reload" class="refresh" />
                                  </div>
                                </div>


                                <div class="form-group">
                                    <button type="button" id="contact_form_btn" class="btn-block">Send Message</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->

    <!--FOOER AREA-->
    <div class="footer-area white">
    
        <div class="footer-bottom-area blue-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-social-bookmark text-center wow fadeIn">
                            <div class="footer-logo mb50">
                                <!-- <a href="#"><img src="img/logo.png" alt=""></a>-->
                            </div>
                            <ul class="social-bookmark">
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                            <p>
Copyright &copy;<script>2018</script> All rights reserved </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--FOOER AREA END-->


    <!--====== SCRIPTS JS ======-->
    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/jquery-1.12.4.min.js" ></script>
     <script type="text/javascript" src="<?php echo base_url();?>js/vendor/bootstrap.min.js" ></script>
   

    <!--====== PLUGIbootstrapNS JS ======-->
    <script src="<?php echo base_url();?>js/vendor/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url();?>js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url();?>js/vendor/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>js/slick.min.js"></script>
    <script src="<?php echo base_url();?>js/stellar.js"></script>
    <script src="<?php echo base_url();?>js/wow.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-modal-video.min.js"></script>
    <script src="<?php echo base_url();?>js/stellarnav.min.js"></script>
<!--     <script src="<?php echo base_url();?>js/contact-form.js"></script> -->
    <script src="<?php echo base_url();?>js/jquery.ajaxchimp.js"></script>
    <script src="<?php echo base_url();?>js/jquery.sticky.js"></script>
     <script src='https://www.google.com/recaptcha/api.js'></script>

    <!--===== ACTIVE JS=====-->
    <script src="<?php echo base_url();?>js/main.js"></script>
</body>

</html>


<script type="text/javascript">
    $(document).ready(function() {
    event.preventDefault();

    if ($("name").val()=='') {
    	alert('empty name');
    }

    var error_name = false;



$("#name").blur(function(){
    if (this.value=='') {
        $("#name_error_msg").fadeIn("show");
    } if (this.value!='') {
        $("#name_error_msg").fadeOut();
    }
 });
$("#email").blur(function(){
    if (this.value=='') {
        $("#email_error_msg").fadeIn("show");
    } if (this.value!='') {
        $("#email_error_msg").fadeOut();
    }
 });
$("#message").blur(function(){
    if (this.value=='') {
        $("#msg_error_msg").fadeIn("show");
    }
    if (this.value!='') {
        $("#msg_error_msg").fadeOut();
    }
 });
$("#captcha").blur(function(){
    if (this.value=='') {
        $("#captcha_error_msg").fadeIn("show");
    }
    if (this.value!='') {
        $("#captcha_error_msg").fadeOut();
    }
 });

      $(".refresh").click(function () {       

            $(".imgcaptcha").attr("src","<?php echo base_url(); ?>auth/demo_captcha?_="+((new Date()).getTime()));
        });
     
         
         $("#contact_form_btn").click(function(){ 
             var name = $('#name').val();
             var email = $('#email').val();
             var subject = $('#subject').val();
             var message = $('#message').val();
             var captcha = $('#captcha').val();

             var error_name = false;
             var error_email = false;
             var error_msg = false;
             var error_captcha = false;

              if (name=='') {
              	 $("#name_error_msg").fadeIn("show");
    			error_name= true;
    		}
    		if (email=='') {
              	 $("#email_error_msg").fadeIn("show");
    			error_email= true;
    		}
    		if (message=='') {
              	 $("#msg_error_msg").fadeIn("show");
    			error_msg= true;
    		}if (captcha=='') {
              	 $("#captcha_error_msg").fadeIn("show");
    			error_captcha= true;
    		}
    		

    

            if (error_name == false && error_email == false && error_msg == false && error_captcha == false) {

            $.ajax({
                url:"<?php echo base_url();?>auth/contact_form",
                method:"POST",
                data:{name:name,email:email,subject:subject,message:message,captcha:captcha},
                dataType:"text",
                success: function(data) {
                  //console.log(data);
                  $("#send_message").fadeIn().html(data);

                  // show success message after 2 sec                  
                  // setTimeOut(function(){
                  //    $("#send_message").fadeOut('slow');
                  // },2000);

                  // keep msg showing with 10 sec
                  $("#send_message").show().delay(10000).fadeOut();
               },
               error: function(err) {
              }                
          });
        }



         });


 // $("#name_error_msg").hide();
 // $("#msg_error_msg").hide();
 // $("#email_error_msg").hide();




 
    });
</script>
