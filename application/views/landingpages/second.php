<!DOCTYPE html>
<html lang="en">

<head>

    <!-- meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Buzup - One Page Multipurpose Html Template">
    <meta name="keywords" content="business, responsive, multipurpose, onepage, corporate, clean">
    <meta name="author" content="Designcarebd">

    <!-- Site title -->
    <title>Welcome to ProjectName</title>

    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <!-- Bootstrap css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!--Font Awesome css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">

    <!-- Normalizer css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/normalize.css" rel="stylesheet">

    <!-- imagehover css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/imagehover.min.css" rel="stylesheet">

    <!-- Owl Carousel css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/owl.carousel.min.css" rel="stylesheet">
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/owl.transitions.css" rel="stylesheet">

    <!-- Magnific popup css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/magnific-popup.css" rel="stylesheet">

    <!-- animate css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">

    <!-- Site css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

    <!-- Responsive css -->
    <link  type = "text/css" href = "<?php echo base_url(); ?>assets/css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Preloader starts -->
    <div id="preloader"></div>
    <!-- Preloader ends -->

    <!-- Navigation area starts -->
    <div class="menu-area navbar-fixed-top">
        <div class="container">
            <div class="row">

                <!-- Navigation starts -->
                <div class="col-md-12">
                    <div class="mainmenu">
                        <div class="navbar navbar-nobg">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="index.html">
                                    <?php 
                                        echo img('assets/img/logo1.png');
                                    ?>
                                    <!-- <img src="assets/img/logo1.png" alt="logo"> -->
                                </a>
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse">
                                <nav>
                                    <style type="text/css">
                                        .SIGNIN_BTN{
                                            background-color: #31BFC3;
                                            padding: 10px!important;
                                            border-radius: 3px;
                                        }
                                    </style>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a class="smooth_scroll" href="#slider">HOME</a></li>
                                        <li><a class="smooth_scroll" href="#about">ABOUT</a></li>
                                        <li><a class="smooth_scroll" href="#howitworks">HOW IT WORKS</a></li> 
                                        <li><a class="smooth_scroll" href="#pricing">PRICING</a></li>
                                        <li><a class="smooth_scroll" href="#calculator">FREE CALCULATOR</a></li>
                                        <li><a class="smooth_scroll" href="#whatnext">WHAT NEXT</a></li>
                                        <li><a class="smooth_scroll" href="#contact">CONTACT</a></li>
                                        <li><a class="smooth_scroll SIGNIN_BTN" href="#">SIGN UP </a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Navigation ends -->

            </div>
        </div>
    </div>
    <!-- Navigation area ends -->

    <div class="clearfix space"></div>

    <!-- Slider area starts -->
    <section id="slider">
        <div id="carousel-example-generic" class="carousel slide carousel-fade">
            <div class="carousel-inner" role="listbox">

                <!-- Item 1 -->
                <div class="item active slide1">
                    <div class="table">
                        <div class="table-cell">
                            <div class="intro-text container">
                                <div class="title clearfix">
                                    <h2>TODAY’S <span>PROGRESS</span> WAS YESTERDAY’S  <span>PLAN </span></h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit sed do eiusmod incididunt ametfh.</p>
                                </div>
                                <a href="#" class="btn btn-trnsp btn-big fixed-btn ">PURCHASE NOW</a>
                                <a href="#" class="btn btn-trnsp btn-big">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Item 2 -->
                <div class="item slide2">
                    <div class="table">
                        <div class="table-cell">
                            <div class="intro-text container">
                                <div class="title clearfix ">
                                  <h2>TODAY’S <span>PROGRESS</span> WAS YESTERDAY’S  <span >PLAN </span></h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit sed do eiusmod incididunt ametfh.</p>
                                </div>
                                <a href="#" class="btn btn-trnsp btn-big fixed-btn ">PURCHASE NOW</a>
                                <a href="#" class="btn btn-trnsp btn-big">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Item 3 -->
                <div class="item slide3">
                    <div class="table">
                        <div class="table-cell">
                            <div class="intro-text container">
                                <div class="title clearfix">
                                  <h2>TODAY’S <span>PROGRESS</span> WAS YESTERDAY’S  <span >PLAN </span></h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing
                                        elit sed do eiusmod incididunt ametfh.</p>
                                </div>
                                <a href="#" class="btn btn-trnsp btn-big fixed-btn ">PURCHASE NOW</a>
                                <a href="#" class="btn btn-trnsp btn-big">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Wrapper for slides-->

            <!-- Carousel Pagination -->
            <div class="carsol-control">
                <a class="left welcome-control" href="#carousel-example-generic" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                <a class="right welcome-control" href="#carousel-example-generic" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
            </div>

        </div>
    </section>
    <!-- Slider area ends -->

    <!-- About area start -->
    <section id="about" class="about-area section-big">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center mb-30 ">
                        <h2>About <span>US</span> </h2>
                        <p> <span>Lorem</span> ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt ametfh consectetur dolore magna aliqua.</p>
                    </div>
                </div>
            </div>

            <div class="row">

                <!--About Image-->
                <div class="col-md-5 col-sm-12">
                    <div class="about-image mt-30">
                        <?php 
                            echo img('assets/img/about/about-img.jpg');
                        ?>
                        <!-- <img src="assets/img/about/about-img.jpg" alt=""> -->
                    </div>
                </div>

                <div class="col-md-7 col-sm-12">

                    <!--About Content-->
                    <div class="about-content mt-30">
                        <h3 class="sub-title">Why We Are Best Choice</h3>
                        <div class="about-details">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <div class="space-text-top">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>

                        <!--About single-content-->
                        <div class="bdr single-content mt-30">
                            <div class="icon">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <div class="content-box">
                                <h4>FEATUTE ONE</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and types.</p>
                            </div>
                        </div>

                        <!--About single-content-->
                        <div class="bdr single-content mt-30">
                            <div class="icon">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                            <div class="content-box">
                                <h4>FEATUTE TWO</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and types.</p>
                            </div>
                        </div>

                        <!--About single-content-->
                        <div class="bdr single-content mt-30">
                            <div class="icon">
                                <i class="fa fa-television  "></i>
                            </div>
                            <div class="content-box">
                                <h4>FEATUTE THREE</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and types.</p>
                            </div>
                        </div>

                        <!--About single-content-->
                        <div class="bdr single-content mt-30">
                            <div class="icon">
                                <i class="fa fa-lightbulb-o  "></i>
                            </div>
                            <div class="content-box">
                                <h4>FEATUTE FOUR</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and types.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About area ends -->

 

    <!--Service area start -->
    <section id="howitworks" class="service-area section-big">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center mb-30 ">
                        <h2>How it <span>works</span> </h2>
                        <p> <span>Lorem</span> ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt
						ametfh consectetur dolore magna aliqua.</p>
                    </div>
                </div>
            </div>

            <div class="row mt-30">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <ul class="service-list">
                        <li class="active">
                            <a href="#web" data-toggle="tab"> <i class="fa fa-television"></i>
                                <br> step one </a>
                        </li>
                        <li>
                            <a href="#graphics" data-toggle="tab"> <i class="fa fa-codepen"></i>
                                <br> step two</a>
                        </li>
                        <li>
                            <a href="#web-development" data-toggle="tab"><i class="fa fa-code"></i><br> step three</a>
                        </li>
                        <li>
                            <a href="#seo" data-toggle="tab"><i class="fa fa-globe"></i><br>step four</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active " id="web">
                            <div class="tab-content">
                                <div class="tab-img">
                                    <?php 
                                        echo img('assets/img/service/tab1.jpg');
                                    ?>
                                   <!--  <img src="assets/img/service/tab1.jpg" alt=""> -->
                                </div>
                                <div class="single-tab">
                                    <h3 class="sub-title">TITLE FOR <span> STEP ONE</span></h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    <div class="mt-30 mb-30">
                                        <p>In congue justo sit amet odio semper, non suscipit elit fringilla. Aenean vitae metus efficitur, mattis dolor ac, condimentum lacus Cras semper ultrices libero vel interdum.</p>
                                    </div>
                                    <ul>
                                        <li> Anim pariatur cliche reprehenderit </li>
                                        <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                        <li> Morbi accumsan ipsum velit sodales tellus odio tincidunt auctor</li>
                                        <li>Locavore pork belly scenester, pinterest chillwave microdos</li>
                                    </ul>
                                    <a href="#" class="btn tab-btn">read more</a>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="graphics">
                            <div class="tab-content">
                                <div class="tab-img">
                                    <?php 
                                        echo img('assets/img/service/tab2.jpg');
                                    ?>
                                    <!-- <img src="assets/img/service/tab2.jpg" alt=""> -->
                                </div>
                                <div class="single-tab">
                                    <h3 class="sub-title">TITLE FOR <span> STEP TWO</span></h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    <div class="mt-30 mb-30">
                                        <p>In congue justo sit amet odio semper, non suscipit elit fringilla. Aenean vitae metus efficitur, mattis dolor ac, condimentum lacus Cras semper ultrices libero vel interdum.</p>
                                    </div>
                                    <ul>
                                        <li> Anim pariatur cliche reprehenderit </li>
                                        <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                        <li> Morbi accumsan ipsum velit sodales tellus odio tincidunt auctor</li>
                                        <li>Locavore pork belly scenester, pinterest chillwave microdos</li>
                                    </ul>
                                    <a href="#" class="btn tab-btn">read more</a>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="web-development">
                            <div class="tab-content">
                                <div class="tab-img">
                                    <?php 
                                        echo img('assets/img/service/tab3.jpg');
                                    ?>
                                    <!-- <img src="assets/img/service/tab3.jpg" alt=""> -->
                                </div>
                                <div class="single-tab">
                                    <h3 class="sub-title">TITLE FOR <span> STEP THREE</span></h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    <div class="mt-30 mb-30">
                                        <p>In congue justo sit amet odio semper, non suscipit elit fringilla. Aenean vitae metus efficitur, mattis dolor ac, condimentum lacus Cras semper ultrices libero vel interdum.</p>
                                    </div>
                                    <ul>
                                        <li> Anim pariatur cliche reprehenderit </li>
                                        <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                        <li> Morbi accumsan ipsum velit sodales tellus odio tincidunt auctor</li>
                                        <li>Locavore pork belly scenester, pinterest chillwave microdos</li>
                                    </ul>
                                    <a href="#" class="btn tab-btn">read more</a>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="seo">
                            <div class="tab-content">
                                <div class="tab-img">
                                    <?php 
                                        echo img('assets/img/service/tab4.jpg');
                                    ?>

                                   <!--  <img src="assets/img/service/tab4.jpg" alt=""> -->
                                </div>
                                <div class="single-tab">
                                    <h3 class="sub-title">TITLE FOR <span> STEP FOUR</span></h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    <div class="mt-30 mb-30">
                                        <p>In congue justo sit amet odio semper, non suscipit elit fringilla. Aenean vitae metus efficitur, mattis dolor ac, condimentum lacus Cras semper ultrices libero vel interdum.</p>
                                    </div>
                                    <ul>
                                        <li> Anim pariatur cliche reprehenderit </li>
                                        <li> Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                                        <li> Morbi accumsan ipsum velit sodales tellus odio tincidunt auctor</li>
                                        <li>Locavore pork belly scenester, pinterest chillwave microdos</li>
                                    </ul>
                                    <a href="#" class="btn tab-btn">read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- fun-facts area starts -->

  <!-- Price area starts -->
    <section id="pricing" class="pricing-area pt-95">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center mb-30">
                        <h2>pricing <span>table</span> </h2> 
                        <p> <span>Lorem</span> ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt
                        ametfh consectetur dolore magna aliqua.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Pricing Table -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="price-item mt-30" data-connectors="1">
                        <div class="info">
                            <p class="level">Try Our Free Calculator</p>
                            <p class="price">
                                <span class="number">$ 0.0</span>
                                <span class="month">/ unlimited</span>
                            </p>
                        </div>
                        <div class="features">
                            <ul>
                                <li>Feature One</li>
                                <li>Feature Two</li>
                                <li>Feature Three</li>
                                <li>Feature Four</li>
                                <li>Feature Five</li>
                            </ul>
                        </div>
                        <a class="btn" href="#">Try Now</a>
                    </div>
                </div>

                <!-- Pricing Table -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="price-item mt-30" data-connectors="1">
                        <div class="info">
                            <p class="level">The Monthly Subscription</p>
                            <p class="price">
                                <span class="number">$ 199</span>
                                <span class="month">/ mon</span>
                            </p>
                        </div>
                        <div class="features">
                            <ul>
                                <li>Feature One</li>
                                <li>Feature Two</li>
                                <li>Feature Three</li>
                                <li>Feature Four</li>
                                <li>Feature Five</li>
                            </ul>
                        </div>
                        <a class="btn" href="#">Sign Up</a>
                    </div>
                </div>

                <!-- Pricing Table -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="price-item featured mt-30" data-connectors="1">
                        <div class="info">
                            <p class="level hbg">More Coming! Stay Tuned!</p>
                            <p class="price">
                                <span class="number">Long Term</span>
                                <span class="month">Relation</span>
                            </p>
                        </div>
                        <div class="features">
                            <ul>
                                <li>Future One</li>
                                <li>Future Two</li>
                                <li>Future Three</li>
                                <li>Future Four</li>
                                <li>Future Five</li>
                            </ul>
                        </div>
                        <a class="btn" href="#">Stay Tuned</a>
                    </div>
                </div>

            </div>
            
            <div class="row subscribe-area pt-95">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="subscribe-box">
                        <div class="newsletter">
                            <h1>Stay up to date</h1>
                            <p><span>Subscribe</span> to our Newsletter. We'll send email notifications everytime we get hot offers for you.</p>
                            <form id="mc-form" class="mc-form">
                                <div class="newsletter-form">
                                    <input type="email" autocomplete="off" id="mc-email" placeholder="Enter your email address" class="form-control">

                                    <button class="mc-submit" type="submit">Subscribe now</button>

                                    <!-- mailchimp-alerts Start -->
                                    <div class="mailchimp-alerts">
                                        <div class="mailchimp-submitting"></div>
                                        <!-- mailchimp-submitting end -->
                                        <div class="mailchimp-success"></div>
                                        <!-- mailchimp-success end -->
                                        <div class="mailchimp-error"></div>
                                        <!-- mailchimp-error end -->
                                    </div>
                                    <!-- mailchimp-alerts end -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
               <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="video-area">
                        <div class="video-content text-center">
                            <a class="popup-youtube" href="http://www.youtube.com/watch?v=xtZE3sMv6lg">
                                <i class="fa fa-play"></i>
                            </a>
                           
                        </div>
                    </div>

                </div>
            </div>  
        </div>
    </section>
    <!-- Price area ends -->

    <!-- Free calculator area ends -->
    <section id="calculator" class="work-area section-big">
        <div class="container">
                <div class="row choose-area">
                <div class="col-md-12">
                    
                     <div class="section-title text-center mb-30 ">
                        <h2>Our Free  <span>Calculator</span> </h2>
                        <p> <span>Lorem</span> ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt
                    ametfh consectetur dolore magna aliqua.</p>
                    </div>


                </div>


                
                <!-- About Text -->
                <div class="col-md-6 col-sm-12 col-xs-12 ">
                    <div class="choose-us">
                        <div class="choose-feature mt-30">
                            <i class="fa fa-rocket"></i>
                            <h3>Feature Title One</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting. Ipsum has been the industry's standard dummy text ever since.</p>
                        </div>

                        <div class="choose-feature mt-30">
                            <i class="fa fa-trophy"></i>
                            <h3>Feature Title Two</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting. Ipsum has been the industry's standard dummy text ever since.</p>
                        </div>

                        <div class="choose-feature mt-30">
                            <i class="fa  fa-paper-plane"></i>
                            <h3>Feature Title Three</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting. Ipsum has been the industry's standard dummy text ever since.</p>
                        </div>

                    </div>
                </div>

                <!-- Choose Image -->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="choose-img mt-30">
                        <?php 
                            echo img('assets/img/choose/choose-img.png');
                        ?>

                        <!-- <img src="assets/img/choose/choose-img.png" alt=""> -->
                    </div>
                </div>
            </div>
			
        </div>
    </section>
    <!-- Work area ends -->

    <!-- callto action starts -->
    <section class="callto-action section-big ">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="call-text pull-left">
                        <p>Are You Impressed With Our Amazing Calculator?</p>
                        <h3>START YOUR NEXT PROJECT WITH US</h3>
                    </div>
                    <div class="call-button pull-right">
                        <a class="action-button btn" href="#">What is next</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- callto action ends -->

 

    <!-- Contact area starts -->
    <section id="whatnext" class="contact-area section-big">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center mb-30 ">
                        <h2>WHAT IS <span>NEXT</span> / LATEST <span>NEWS</span> </h2>
                        <p> Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt ametfh consectetur dolore magna aliqua.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
					<div class="single-news">
						<div class="news-image">
                            <?php 
                                echo img('assets/img/news/1.jpg');
                            ?>
							<!--  <img src="assets/img/news/1.jpg" alt=""> -->
							 <div class="new-date">
								<h4><span class="month">Jun</span> <span class="date">26</span></h4>
							</div> 
						</div>
						
						<div class="news-content">
                            <p class="news-meta text-muted">
                                <span><i class="fa fa-user"></i>Posted by admin</span>
                                <span><i class="fa fa-comment"></i>Comments</span>
                            </p>
							<a href="#blog-modal-1" data-toggle="modal">
                                <h3 class="subtitle">blog post sample title</h3>
                            </a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
                            <a class="btn" href="#blog-modal-1" data-toggle="modal">Read More</a>
                        </div>
						
					</div>
				</div>
                <div class="col-md-4">
					<div class="single-news">
						<div class="news-image">
                            <?php 
                                echo img('assets/img/news/2.jpg');
                            ?>
							 <!-- <img src="assets/img/news/2.jpg" alt=""> -->
							 <div class="new-date">
								<h4><span class="month">Jun</span> <span class="date">26</span></h4>
							</div> 
						</div>
						
						<div class="news-content">
                            <p class="news-meta text-muted">
                                <span><i class="fa fa-user"></i>Posted by admin</span>
                                <span><i class="fa fa-comment"></i>Comments</span>
                            </p>
							<a href="#blog-modal-2" data-toggle="modal">
                                <h3 class="subtitle">blog post sample title</h3>
                            </a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
                            <a class="btn" href="#blog-modal-2" data-toggle="modal">Read More</a>
                        </div>
						
					</div>
				</div>
                <div class="col-md-4">
					<div class="single-news">
						<div class="news-image">
                            <?php 
                                echo img('assets/img/news/3.jpg');
                            ?>
							 <!-- <img src="assets/img/news/3.jpg" alt=""> -->
							 <div class="new-date">
								<h4><span class="month">Jun</span> <span class="date">26</span></h4>
							</div> 
						</div>
						
						<div class="news-content">
                            <p class="news-meta text-muted">
                                <span><i class="fa fa-user"></i>Posted by admin</span>
                                <span><i class="fa fa-comment"></i>Comments</span>
                            </p>
							<a href="#blog-modal-3" data-toggle="modal">
                                <h3 class="subtitle">blog post sample title</h3>
                            </a>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
                            <a class="btn" href="#blog-modal-3" data-toggle="modal">Read More</a>
                        </div>
						
					</div>
				</div>
				
				<!-- News 1 Modals -->
				<div class="news-modal modal fade" id="blog-modal-1" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
									<div class="modal-body">
									   <?php 
                                            echo img('assets/img/news/1.jpg');
                                        ?>
										<!-- <img src="assets/img/news/1.jpg" alt=""> -->
										
										<div class="news-content">
											<p class="news-meta text-muted">
												<span><i class="fa fa-user"></i>Posted by admin</span>
												<span><i class="fa fa-comment"></i>Comments</span>
											</p>
											
											<h3 class="subtitle">blog post sample title</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
											
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.,consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
										</div>
										
										<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- News 2 Modals -->
				<div class="news-modal modal fade" id="blog-modal-2" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
									<div class="modal-body">
									   <?php 
                                            echo img('assets/img/news/2.jpg');
                                        ?>
										<!-- <img src="assets/img/news/2.jpg" alt=""> -->
										
										<div class="news-content">
											<p class="news-meta text-muted">
												<span><i class="fa fa-user"></i>Posted by admin</span>
												<span><i class="fa fa-comment"></i>Comments</span>
											</p>
											<h3 class="subtitle">blog post sample title</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
											
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.,consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
										</div>
										
										<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- News 3 Modals -->
				<div class="news-modal modal fade" id="blog-modal-3" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
									<div class="modal-body">
									
										<img src="assets/img/news/3.jpg" alt="">
										
										<div class="news-content">
											<p class="news-meta text-muted">
												<span><i class="fa fa-user"></i>Posted by admin</span>
												<span><i class="fa fa-comment"></i>Comments</span>
											</p>
											<h3 class="subtitle">improve your creative thinking</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.,consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
											
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.,consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamconsectetur adipisicing elit eniam.</p>
										</div>
										
										<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
            </div>
        </div>
    </section>
    <!-- Contact area ends -->

 


    <!-- Testimonial area starts -->
    <section id="testimonial" class="testimonial-area section-big">
        <div class="container">

            <div class="row">
                <div class="testimonial-list">

                    <!-- Testimonial item 1-->
                    <div class="single-testimonial">
                        <div class="featured_tes">
                            <img src="assets/img/testimonial/1.jpg" alt="">
                        </div>

                        <div class="feedback-text">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt ametfh consectetur dolore magna aliqua. Ut enim ad minim veniam dolor sit amet magnaelit ate consectetur adipisicing elit sed do eiusmod tempor incididunt consectetur dolore magna aliqua.</p>
                            <h4>Carol Smith / <span>Director</span></h4>
                        </div>

                    </div>

                    <!-- Testimonial item 2-->
                    <div class="single-testimonial">
                        <div class="featured_tes">
                            <img src="assets/img/testimonial/2.jpg" alt="">
                        </div>
                        <div class="feedback-text">

                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt ametfh consectetur dolore magna aliqua. Ut enim ad minim veniam dolor sit amet magnaelit ate consectetur adipisicing elit sed do eiusmod tempor incididunt consectetur dolore magna aliqua.</p>
                            <h4>Carol Smith   / <span>Director</span> </h4>
                        </div>

                    </div>

                    <!-- Testimonial item 3-->
                    <div class="single-testimonial">
                        <img src="assets/img/testimonial/3.jpg" alt="">

                        <div class="feedback-text">

                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt ametfh consectetur dolore magna aliqua. Ut enim ad minim veniam dolor sit amet magnaelit ate consectetur adipisicing elit sed do eiusmod tempor incididunt consectetur dolore magna aliqua.</p>

                            <h4>Carol Smith   / <span>Director</span> </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial area ends -->

    <!-- Contact area starts -->
    <section id="contact" class="contact-area section-big">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center mb-30 ">
                        <h2>contact us <span>now</span> </h2>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod incididunt
						ametfh consectetur dolore magna aliqua.</p>
                    </div>
                </div>
            </div>

            <div class="row contact-infos">
                <div class="col-md-8">
                    <!-- Contact form starts -->
                    <div class="contact-form mt-30">

                        <!-- Submition status -->
                        <div id="form-messages"></div>

                        <form id="ajax-contact" action="#" method="post">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group in_name">
                                        <input type="text" name="name" class="form-control" id="name" placeholder="Full Name" required="required">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group in_email">
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Email Address" required="required">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group in_subject">
                                        <input type="text" name="subject" class="form-control" id="subject" placeholder="Subject" required="required">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group in_message">
                                        <textarea name="message" class="form-control" id="message" placeholder="Your Message" required="required"></textarea>
                                    </div>
                                    <div class="actions">
                                        <input type="submit" value="Sent Message" name="submit" id="submitButton" class="btn" title="Submit Your Message!">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Contact form ends-->

                </div>
                <div class="col-md-4">
                    <div class="address mt-30">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod invidunt ut labore etiops dolore magna aliquyam erat.</p>
                        <div class="address-list">
                            <p><strong>Address</strong><span>: XXX XXXX <br>XXXX, Florida</span></p>
                            <p><strong>Phone</strong><span>:+XX-XXX-XXX-XXX</span></p>
                            <p><strong>Fax</strong><span>:+XX-XXXX-XXXX-X</span></p>
                            <p><strong>email</strong><span>:youremail@gmail.com</span></p>
                            <p><strong>web</strong><span>:www.domain.com</span></p>
                        </div>

                        <div class="social-media ">
                            <a href="#"><i class="fa fa-rss"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-skype"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact area ends -->

    <!-- copyright area starts -->
    <footer class="copyright-area section-small">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="copy-right">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </p>
                    </div>
                </div>
                <a class="smooth_scroll" href="#slider" id="scroll-to-top"><i class="fa fa-long-arrow-up"></i></a>
            </div>
        </div>
    </footer>
    <!-- copyright area ends -->

    <!-- Latest jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

    <!-- Bootstrap js-->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

    <!-- Owl Carousel js -->
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>

    <!-- Mixitup js -->
    <script src="<?php echo base_url();?>assets/js/jquery.mixitup.js"></script>

    <!-- Magnific popup js -->
    <script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>

    <!-- Waypoint js -->
    <script src="<?php echo base_url();?>assets/js/jquery.waypoints.min.js"></script>

    <!-- jquery.inview js -->
    <script src="<?php echo base_url();?>assets/js/jquery.inview.min.js"></script>

    <!-- wow js -->
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>

    <!-- Ajax Mailchimp js -->
    <script src="<?php echo base_url();?>assets/js/jquery.ajaxchimp.min.js"></script>

    <!-- GOOGLE MAP JS -->
    /*<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWGUaG5S3B5VikF8UtVCgjki1Pv_HKGgo"></script>*/

    <!-- Main js-->
    <script src="<?php echo base_url();?>assets/js/main_script.js"></script>

</body>

</html>