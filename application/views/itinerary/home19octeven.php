<?php
    $this -> load -> view('_header');
        if($_SESSION['subscribed'] != 'yes'){ 
        $this->session->set_flashdata("error", "You are not a subscribed user till now. Please be subscribed first to access our system.");
        redirect("dashboard/payment_option", "refresh");
    }
?>
<script type="text/javascript" src="<?php echo base_url();?>js/itinerary.js"></script> 
<div ng-controller="itinerary">
                
    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
        
            <h2 class="text-center">Itinerary</h2> 
                
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <div class="row">
                      <div class="col-md-4">

                        <label class="control-label">Id Folder : </label>
                        <span>{{itinerary.vessel.vessel_id}}</span>
                      </div>
                      <div class="col-md-4">
                        <label class="control-label">Vessel :</label>
                        <span>{{itinerary.vessel.vessel}}</span>
                      </div>
                      <div class="col-md-4">
                        <label class="control-label">Port :</label>
                        <span>{{itinerary.vessel.port}}</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label class="control-label">Cargo </label>
                        <input type="text" class="form-control" name="" ng-model="itinerary.cargo">
                      </div>
                      <div class="col-md-6">
                        <label class="control-label">Old Records :</label>
                        
                   <ui-select style="background: lightgray;margin-left: 8px;height: 30px;"  ng-model="itinerary.oldRecord" ng-change="selectOldRecord(itinerary.oldRecord)" theme="selectize" ng-disabled="disabled" title="Choose a District">
                          <ui-select-match>{{$select.selected.cargo}}</ui-select-match>
                          <ui-select-choices repeat="obj in itns | filter: {cargo:$select.search}">
                            <span ng-bind-html="obj.cargo | highlight: $select.search"></span>
                          </ui-select-choices>
                        </ui-select>

                      </div>
                    </div>
                  </div>
                  <div class="panel-body">
                    <table class="table table-bordered event-table">
                      <thead>
                        <th>Event Name</th>
                        <th>Estimated Time</th>
                        <th>Actual Time</th>
                        <th>Action</th>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Arrival Time</td>
                          <td>
                            <div class='input-group date' id='est_arrival_time'>
                                  <input type='text' ng-model="itn_basic.est_arrival_time" class="form-control" />
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                            </td>
                          <td>
                            <div class='input-group date' id='actual_arrival_time'>
                                <input ng-model="itn_basic.actual_arrival_time" type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                          </td>
                          <td>
                            <button class="btn btn-danger">Trash</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Inspection Time</td>
                          <td>
                            <div class='input-group date' id='est_inspection_time'>
                                <input type='text' ng-model="itn_basic.est_inspection_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            </td>
                          <td>
                            <div class='input-group date' id='actual_inspection_time'>
                                <input type='text' ng-model="itn_basic.actual_inspection_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                          </td>
                          <td>
                            <button class="btn btn-danger">Trash</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Starting Time</td>
                          <td>
                            <div class='input-group date' id='est_starting_time'>
                                <input type='text' ng-model="itn_basic.est_starting_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            </td>
                          <td>
                            <div class='input-group date' id='actual_starting_time'>
                              <input type='text' ng-model="itn_basic.actual_starting_time" class="form-control" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                          </td>
                          <td>
                            <button class="btn btn-danger">Trash</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Sailing Time</td>
                          <td>
                            <div class='input-group date' id='est_sailing_time'>
                                <input type='text' ng-model="itn_basic.est_sailing_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            </td>
                          <td>
                            <div class='input-group date' id='actual_sailing_time'>
                              <input type='text' ng-model="itn_basic.actual_sailing_time" class="form-control" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                          </td>
                          <td>
                            <button class="btn btn-danger">Trash</button>
                          </td>
                        </tr>
                        <tr>
                          <td>Bearthing Time</td>
                          <td>
                            <div class='input-group date' id='est_bearthing_time'>
                                <input type='text' ng-model="itn_basic.est_bearthing_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            </td>
                          <td>
                            <div class='input-group date' id='actual_bearthing_time'>
                              <input type='text' ng-model="itn_basic.actual_bearthing_time" class="form-control" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                          </td>
                          <td>
                            <button class="btn btn-danger">Trash</button>
                          </td>
                        </tr>

                        <tr>
                          <td>Completion Time</td>
                          <td>
                            <div class='input-group date' id='est_completion_time'>
                                <input type='text' ng-model="itn_basic.est_completion_time" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                            </td>
                          <td>
                            <div class='input-group date' id='actual_completion_time'>
                              <input type='text' ng-model="itn_basic.actual_completion_time" class="form-control" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                          </td>
                          <td>
                            <button class="btn btn-danger">Trash</button>
                          </td>
                        </tr>

                        <tr ng-repeat="event in events">
                          <td><input type="text" class="form-control" ng-model="event.event_name"></td>
                              <td>
                                <div class="input-group" style="width: 100%;
                                box-sizing: border-box;">
                                  <input type="text" class="form-control" uib-datepicker-popup="dd.MM.yyyy" ng-model="event.estimate_date" is-open="isDatepickerOpen" datepicker-options="datepickerOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats"/>
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" ng-click="isDatepickerOpen = !isDatepickerOpen">
                                      <i class="glyphicon glyphicon-calendar"></i>
                                    </button>
                                  </span>
                                </div>
                                </td>
                              <td>
                             <div class="input-group" style="width: 100%;
                                box-sizing: border-box;">
                              <input type="text"  class="form-control" uib-datepicker-popup="dd.MM.yyyy" ng-model="event.actual_date" is-open="isDatepickerOpen1" datepicker-options="datepickerOptions" ng-required="true" close-text="Close" alt-input-formats="altInputFormats"/>
                              <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="isDatepickerOpen1 = !isDatepickerOpen1">
                                  <i class="glyphicon glyphicon-calendar"></i>
                                </button>
                              </span>
                            </div>
                          </td>
                          <td>
                            <button class="btn btn-danger" ng-click="deleteObject(event,'events')">Trash</button>
                            
                          </td>
                        </tr>



                      </tbody>
                    </table>
                    <button class="btn btn-success" ng-click="saveItinerary()" style="float: right;">Save</button>
                    <button class="btn btn-info" style="float: right;" ng-click="addObjectn('events')">Add</button>

                  </div>
                </div>

    </div>
</div> <!--controller ends-->
           
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->

</div>

<?php

    $this -> load -> view('_footer');
?>