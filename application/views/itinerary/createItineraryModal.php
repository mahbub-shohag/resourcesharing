<div class="modal fade" id="itineraryCreateModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 550px;">
        <div class="modal-header" style="background:#08d4ef;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Itinerary</h4>
        </div>
        <div class="modal-body">
            <form>
              <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">

                      <label class="control-label">Date</label>
                      <div class='input-group date' id='itinerary_date'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Vessel Id</label>
                      <div>
                          <input type='text' ng-model="itinerary.vessel_id" readonly="" class="form-control" />
                      </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-6">

                      <label class="control-label">Estimated Arrival Time</label>
                      <div class='input-group date' id='est_arrival_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Actual Arrival Time</label>
                      <div class='input-group date' id='actual_arrival_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                 </div>

                 <div class="row">
                    <div class="col-md-6">
                      <label class="control-label">Estimated Inspection Time</label>
                      <div class='input-group date' id='est_inspection_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Actual Inspection Time</label>
                      <div class='input-group date' id='actual_inspection_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                 </div>

                 <div class="row">
                    <div class="col-md-6">
                      <label class="control-label">Estimated Bearthing Time</label>
                      <div class='input-group date' id='est_bearthing_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Actual Bearthing Time</label>
                      <div class='input-group date' id='actual_bearthing_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                 </div>

                 <div class="row">
                    <div class="col-md-6">
                      <label class="control-label">Estimated Starting Time</label>
                      <div class='input-group date' id='est_starting_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Actual Starting Time</label>
                      <div class='input-group date' id='actual_starting_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                 </div>

                 <div class="row">
                    <div class="col-md-6">
                      <label class="control-label">Estimated Completion Time</label>
                      <div class='input-group date' id='est_completion_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Actual Completion Time</label>
                      <div class='input-group date' id='actual_completion_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                 </div>

                 <div class="row">
                    <div class="col-md-6">
                      <label class="control-label">Estimated Sailing Time</label>
                      <div class='input-group date' id='est_sailing_time'>
                          <input type='text' class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Actual Sailing Time</label>
                      <div class='input-group date' id='actual_sailing_time'>
                          <input type='text'  class="form-control" />
                          <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                      </div>
                    </div>
                 </div>

              </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="submit" ng-click="saveItinerary()" data-dismiss="modal" class="btn btn-default">Save</button>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
            /*var i;
            for(i=1;i<11;i++){
              var id = '#datetimepicker'+i;
              console.log(id);
              $(function () {
                $(id).datetimepicker();
              });  
            }*/


            
            
</script>