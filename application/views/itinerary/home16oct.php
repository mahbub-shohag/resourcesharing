<?php
    $this -> load -> view('_header');
        if($_SESSION['subscribed'] != 'yes'){ 
        $this->session->set_flashdata("error", "You are not a subscribed user till now. Please be subscribed first to access our system.");
        redirect("dashboard/payment_option", "refresh");
    }
?>
<script type="text/javascript" src="<?php echo base_url();?>js/itinerary.js"></script> 
<div ng-controller="itinerary">
                
    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
        
            <h2 class="text-center">Itinerary</h2> 
            <button class="btn btn-info" style="margin-left: 681px;
    margin-top: -19px;">
                {{selectedRecord.vessel_id}}</button>
                <button class="btn btn-default" data-toggle="modal" data-target="#itineraryCreateModal" style="float: right;">Create New</button>   
        <table style="width: 100%">
            <thead></thead>
            <tbody>
                <tr style="width: 100%" ng-repeat="itn in itns track by $index">
                    <td style="width: 100%">
                        <table class="table table-bordered">
                           <thead>
                               
                           </thead>
                           <tbody>
                                   <tr>
                                       <td colspan="1"></td> 
                                       <td>Estimated</td>
                                       <td>Actual</td>
                                   </tr>
                                   <tr>
                                       <td>Time Of Arrival</td>
                                       <td>{{itn.est_arrival_time | date : 'mm/dd/yyyy'}}</td>
                                       <td>{{itn.actual_arrival_time}}</td>
                                    </tr>
                                   <tr>
                                       <td>Time for Inspections</td>
                                       <td>{{itn.est_inspection_time}}</td>
                                       <td>{{itn.actual_inspection_time}}</td>
                                   </tr>
                                   <tr>
                                       <td>Time Of Bearthing</td>
                                       <td>{{itn.est_bearthing_time}}</td>
                                       <td>{{itn.actual_bearthing_time}}</td>
                                   </tr>
                                   <tr>
                                       <td>Starting Time</td>
                                       <td>{{itn.est_starting_time}}</td>
                                       <td>{{itn.actual_starting_time}}</td>
                                   </tr>
                                   <tr>
                                       <td>Completion Time</td>
                                       <td>{{itn.est_completion_time}}</td>
                                       <td>{{itn.actual_completion_time}}</td>
                                   </tr>
                            </tbody>
                       </table>      
                    </td>
                </tr>
            </tbody>
        </table>   
<?php
    $this-> load -> view('itinerary/createItineraryModal');
?>
    </div>
</div> <!--controller ends-->
           
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->

</div>

<?php

    $this -> load -> view('_footer');
?>