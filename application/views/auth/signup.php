<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en" ng-app="signupApp" ng-controller="signupController">
<!--<![endif]-->

<head>    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="It's an Saas based ERP" />
    <meta name="keywords" content="ERP, Calculator, Monthly Subscription" />   
    <title>Signup | ProjectName</title>
   
    <link rel="shortcut icon" type="image/ico" href="img/favicon.png" />

   
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/normalize.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/animate.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/modal-video.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/stellarnav.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/owl.carousel.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/slick.css">

    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap.min.css"> 
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/font-awesome.min.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/material-icons.css">
    
    

    <!--====== MAIN STYLESHEETS ======-->
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/responsive.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/signup.css">
    

    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/modernizr-2.8.3.min.js" ></script>

    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/jquery-1.12.4.min.js" ></script>




<script src="<?php echo base_url();?>js/jquery.complexify.js/jquery.complexify.banlist.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.complexify.js/jquery.complexify.js" type="text/javascript"></script>



    <script type="text/javascript" src="<?php echo base_url();?>js/custom.js" ></script>
   <!--  <script type="text/javascript" src="<?php echo base_url();?>js/signup.js" ></script> -->
   
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="home-one" data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#home" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header>
        <div class="header-top-area">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a href="<?php echo base_url(); ?>" class="navbar-brand">

                              <?php 
                                echo img('img/logoCopy.png');
                              ?>
                              
                            </a>
                        </div>
                        <div id="main-nav" class="stellarnav">
                            <div class="search-and-signup-button white pull-right hidden-md hidden-sm hidden-xs">
                                <button data-toggle="collapse" data-target="#search-form-switcher"><i class="fa fa-search"></i></button>
                                <a href="signin" class="sign-up">Sign In</a>
                            </div>
                           
                        </div>
                    </div>
                </nav>
                <div id="search-form-switcher" class="search-collapse-area collapse white">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="white">
                                    <form action="#" class="search-form">
                                        <input type="search" name="search" id="search" placeholder="Search Here..">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END MAINMENU AREA END-->
        </div>
    
    </header>
    <!--END TOP AREA-->

    <!--CONTACT US AREA-->
    <section class="contact-area relative padding-100-50" id="contact">
       <!--  <div class="area-bg">ssss</div> -->
        <div class="contact-form-area">
            <div class="container">

                <?php
                  // echo '<pre>';
                  // print_r($_SESSION);
                  // exit; 
                  if(isset($_SESSION['success'])){?>
                    <div class="alert alert-success text-center"><?php echo $_SESSION['success']; ?></div>
                 <?php }?>

                  <?php
                  if(isset($_SESSION['error'])){?>
                    <div class="alert alert-danger text-center"><?php echo $_SESSION['error']; ?></div>

                 <?php }?>


                 <?php echo validation_errors('<div class="alert alert-danger text-center">','</div>'); ?>



<form id="regForm" method="post" action="<?php echo site_url("auth/signup") ?>">

   

  
  <!-- One "tab" for each step in the form: -->


<div class="tab">
 <div class="row">
    <div class="text-center">
        <div class="contact-form wow fadeIn">
        <br>
        <br>
            <h2>SIGN UP HERE</h2><br> 

            <h3>Already have an account? Please sign in  <a href="signin">here</a></h3><br> 
        </div>
    </div>
</div>

<h1>Select Your Best Choice From <span style="color:#3da2d4">Our Packages</span></h1>
  <br>

      <style type="text/css">
              .price-details li {
                font-size: 18px;
    padding: 9px;
   
}
.price-details {
        min-height: 481px;
}
section#contact {
    background: #fff;
}


                </style>
   <div class="row">

      <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
            <div class="single-price center wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; min-height: 400px!important ">
                <div class="price-hidding" style="padding: 30px 28px 30px;">
                    <h2>Free laytime calculator</h2>
                </div>
                <div class="price-rate">
                    <h2><sup>$</sup>0.0
                        <sub>Unlimited</sub>
                    </h2>
                </div>
                
                <div class="price-details"> 
                    <ul>
                        <li>*In our basic free calculator, you can calculate laytime for free up to five exceptions</li> 
                        <li>*Print in pdf format</li> 
                        <li><b>You don’t need to sign up, just go <a href="<?php echo base_url(); ?>calculator/basic_calculator" >here</a> to use it!</b></li> 
                                   
                    </ul>    
                </div>    
                
            </div>
        </div>

     <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
            <div class="single-price center wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                <div class="price-hidding" style="padding: 30px 28px 30px;" >
                    <h2>Supercargo</h2>
                </div>
                <div class="price-rate">
                    <h2><sup>$</sup>15.0
                        <sub>Unlimited</sub>
                    </h2>
                </div>
                
                <div class="price-details"> 
                    <ul>
                        <li>*Store your shipping documents
                        <li>*Keep track of your ships eta, etb, ets</li> 
                        <li>*Share documents or data to colleagues or clients on real time</li> 
                        <li>*Calculate laytime with full features</li> 
                        <li>*Save your laytimes in the cloud, or pdf, excel and share with others</li> 
                        <li>*See the turnaround time of your ships to set you up for success tomorrow</li>                
                    </ul>
                     <div class="buy-now-button">                  
                         <input type="radio" name="package" value="1" checked >Select it
                      </div>
                </div>
               
            </div>
        </div>



            <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
            <div class="single-price center wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                <div class="price-hidding" style="padding: 30px 28px 30px;">
                    <h2>More coming..</h2>
                </div>
                <div class="price-rate">
                    <h2><sup>$</sup>0.0
                        <sub>Unlimited</sub>
                    </h2>
                </div>
                
                <div class="price-details"> 
                    <ul>
                        <li>We are working in more features and positions which will come soon</li>                
                    </ul>
                </div>
               
            </div>
        </div>





                   
    </div>

  </div>
  <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
          <div class="tab">
            <h1>Create your <span style="color:#3da2d4">Account</span></h1>
         
                <p><input placeholder="Name..." id="name" name="name"></p>
                <p><input placeholder="Company Name..." oninput="this.className = ''" name="company_name"></p>

                <p> <span class="error_class" id="email_error_message"></span><input type="email" name="email" id="email" placeholder="Email" required>
                    </p>

                <p>(Please enter password including Capital letter, number, special caracther and at least 6 digit)<br> <span id="status" style="color:green"></span>
                   <progress value="0" max="100" id="strength" style="width: 100%"></progress>

                  <span class="error_class" id="password_error_msg"></span>
                   <!--    <input type="password" name="password"  id="password1" placeholder="Password" required></p> -->

                        <p><input type="password" id="password1" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters" required> <img src="<?php echo base_url();?>img/check.jpg" id="check_img" class="unchk" alt=""> <p>


<div id="message">
  <h4>Password must contain the following:</h4>
  <p id="letter" class="invalid">A <b>lowercase</b> letter</p><br>
  <p id="capital" class="invalid">A <b>capital (uppercase)</b> letter</p><br>
  <p id="number" class="invalid">A <b>number</b></p><br>
  <p id="spatial_char" class="invalid">A <b>spatial character</b></p><br>
  <p id="length" class="invalid">Minimum <b>6 characters</b></p><br>
</div>
<br>

                     
               

                <p> <span class="error_class" id="retype_password_error_msg"></span>
                 <input type="password" name="confirm_password"  id="password2" placeholder="Confirm Password" required><img src="<?php echo base_url();?>img/check.jpg" id="c_check_img" class="unchk" alt=""> </p>
          </div>

<div style="overflow:auto;">
    <div style="text-align: center;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn"  name="signup" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
   
  </div>

<style>
/* Style all input fields */

/* Style the submit button */



/* The message box is shown when the user clicks on the password field */
#message {
    display:none;
    background: #f1f1f1;
    color: #000;
    position: relative;
    padding: 20px;
    margin-top: 10px;
}

#message p {
    padding: 10px 35px;
    font-size: 14px;
}

/* Add a green text color and a checkmark when the requirements are right */
.valid {
    color: green;
}

.valid:before {
    position: relative;
    left: -35px;
    content: "✔";
}

#check_img{
	 position: relative;
    height: 40px;
    float: right;
    margin-top: -53px;
    margin-right: 1px;
}
#c_check_img{
	position: relative;
    height: 40px;
    float: right;
    margin-top: -53px;
    margin-right: 1px;
}
.chk {
   
    display: block;
}
.unchk {   
    display: none;
}

/* Add a red text color and an "x" when the requirements are wrong */
.invalid {
    color: red;
}

.invalid:before {
    position: relative;
    left: -35px;
    content: "✖";
}
</style>



<script>
var myInput = document.getElementById("password1");
var letter = document.getElementById("letter");
var capital = document.getElementById("capital");
var number = document.getElementById("number");
var length = document.getElementById("length");
var spatial_char = document.getElementById("spatial_char");

// When the user clicks on the password field, show the message box
myInput.onfocus = function() {
    document.getElementById("message").style.display = "block";
}

// When the user clicks outside of the password field, hide the message box
myInput.onblur = function() {
    document.getElementById("message").style.display = "none";
}

// When the user starts to type something inside the password field
myInput.onkeyup = function() {
  // Validate lowercase letters
  var lowerCaseLetters = /[a-z]/g;
  if(myInput.value.match(lowerCaseLetters)) {  
    letter.classList.remove("invalid");
    letter.classList.add("valid");    
  } else {
    letter.classList.remove("valid");
    letter.classList.add("invalid");
  }
  


  // Validate capital letters
  var upperCaseLetters = /[A-Z]/g;
  if(myInput.value.match(upperCaseLetters)) {  
    capital.classList.remove("invalid");
    capital.classList.add("valid");
  } else {
    capital.classList.remove("valid");
    capital.classList.add("invalid");
  }

  // Validate numbers
  var numbers = /[0-9]/g;
  if(myInput.value.match(numbers)) {  
    number.classList.remove("invalid");
    number.classList.add("valid");
  } else {
    number.classList.remove("valid");
    number.classList.add("invalid");
  }
  
  // Validate length
  if(myInput.value.length >= 6) {
    length.classList.remove("invalid");
    length.classList.add("valid");
  } else {
    length.classList.remove("valid");
    length.classList.add("invalid");
  }

    // Validate spatial characters
  var spatialChar = /[!@#$%^&*()~<>?]/g;
  if(myInput.value.match(spatialChar)) {  
    spatial_char.classList.remove("invalid");
    spatial_char.classList.add("valid");
  } else {
    spatial_char.classList.remove("valid");
    spatial_char.classList.add("invalid");
  }


}
</script>



      </div>
      <div class="col-md-2"></div>
  </div>
  
  
 
</form>

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {  
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    document.getElementById("nextBtn").setAttribute("name", "signup");

    
    $('#nextBtn').off('click');
    $('#nextBtn').click(function(){
        return check_form_validity();       
    });
  } 
  else {
    document.getElementById("nextBtn").innerHTML = "SIGN UP TODAY";
  }  
  fixStepIndicator(n)
}

function nextPrev(n) { 
  var x = document.getElementsByClassName("tab");
  if (n == 1 && !validateForm()) 
    return false;
  x[currentTab].style.display = "none";
  currentTab = currentTab + n;
  if (currentTab >= x.length) {
    document.getElementById("regForm").submit();
    return false;
  }
  showTab(currentTab);
}

function validateForm() { 
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");  
  for (i = 0; i < y.length; i++) {
    if (y[i].value == "") {
      y[i].className += " invalid";
      valid = false;
    }
  }
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid;
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>



                
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->

    <!--FOOER AREA-->
    <div class="footer-area white">
       
        <div class="footer-bottom-area blue-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-social-bookmark text-center wow fadeIn">
                            <div class="footer-logo mb50">
                                <a href="#"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul class="social-bookmark">
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                            <p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--FOOER AREA END-->


    <!--====== SCRIPTS JS ======-->
  
     <script type="text/javascript" src="<?php echo base_url();?>js/vendor/bootstrap.min.js" ></script>
   

    <!--====== PLUGIbootstrapNS JS ======-->
    <script src="<?php echo base_url();?>js/vendor/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url();?>js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url();?>js/vendor/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>js/slick.min.js"></script>
    <script src="<?php echo base_url();?>js/stellar.js"></script>
    <script src="<?php echo base_url();?>js/wow.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-modal-video.min.js"></script>
    <script src="<?php echo base_url();?>js/stellarnav.min.js"></script>
    <script src="<?php echo base_url();?>js/contact-form.js"></script>
    <script src="<?php echo base_url();?>js/jquery.ajaxchimp.js"></script>
    <script src="<?php echo base_url();?>js/jquery.sticky.js"></script>

    <!--===== ACTIVE JS=====-->
    <script src="<?php echo base_url();?>js/main.js"></script>
   <!-- Complexify -->

</body>

</html>




<script type="text/javascript">
    $(document).ready(function() {

// not working here 
//form submission if password matched
    $('#regForm').on('submit', function(){
       var password2 = $('#password2').val();
       var password1 = $('#password1').val();
       console.log('Function called');
       if(password1 != password2){
        return false;
       }
    })
         

     });
</script>
<script type="text/javascript">
  //password strength bar
  var pass= document.getElementById('password1');
  pass.addEventListener('keyup', function() {
    checkPassword(pass.value)
  })

  function checkPassword(password){
    var strengthBar= document.getElementById('strength');
    var strength = 0;
    if (password.match(/[a-z]+/)) {
        strength += 1;
    }
    if (password.match(/[A-Z]+/)) {
        strength += 1;
    }

    if (password.match(/[0-9]+/)) {
        strength += 1;
    }

    if (password.match(/[!@#$%^&*()~<>?]+/)) {
        strength += 1;
    }

    if (password.length>5) {
       strength += 1;

    }
    switch(strength){
      case 0:
              strengthBar.value=0;
              check_img.classList.remove("chk");
              check_img.classList.add("unchk");
              break
      case 1:
              strengthBar.value=20;
              check_img.classList.remove("chk");
              check_img.classList.add("unchk");
              break         

      case 2:
              strengthBar.value=40;
              check_img.classList.remove("chk");
              check_img.classList.add("unchk");
              break

      case 3:
              strengthBar.value=60;
              check_img.classList.remove("chk");
               check_img.classList.add("unchk");
              break
      case 4:
              strengthBar.value=80;
              check_img.classList.remove("chk");
              check_img.classList.add("unchk");
              break
      case 5:
              strengthBar.value=100;
 				
 				check_img.classList.add("chk");
    			check_img.classList.remove("unchk");

              break
            
    }
  }



</script>



