<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="It's an Saas based ERP" />
    <meta name="keywords" content="ERP, Calculator, Monthly Subscription" />   
    <title>Signin | ProjectName</title>
   
    <link rel="shortcut icon" type="image/ico" href="img/favicon.png" />

   
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/normalize.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/animate.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/modal-video.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/stellarnav.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/owl.carousel.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/slick.css">

    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap.min.css"> 
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/font-awesome.min.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/material-icons.css">
    
    

    <!--====== MAIN STYLESHEETS ======-->
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/responsive.css">
    

    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/modernizr-2.8.3.min.js" ></script>
   
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<style type="text/css">
nav.navbar {    
    background: #1193D4;
}

h2 ,p {
    display: block;
}

</style>
<body class="home-one" data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#home" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header>
        <div class="header-top-area">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a href="<?php echo base_url(); ?>" class="navbar-brand">

                                <?php 
                                    echo img('img/logoCopy.png');
                                ?>
                                
                            </a>
                        </div>
                        <div id="main-nav" class="stellarnav">
                            <div class="search-and-signup-button white pull-right hidden-md hidden-sm hidden-xs">
                                <button data-toggle="collapse" data-target="#search-form-switcher"><i class="fa fa-search"></i></button>
                                 <a href="<?php echo base_url(); ?>auth/signup" class="sign-up">Sign up</a>
                               
                            </div>
                           <!--  <ul id="nav" class="nav">
                                <li class="active"><a href="#home">home</a></li>
                                <li><a href="#about">About</a></li>
                                <li><a href="#howit">How It Works</a></li>
                                <li><a href="#pricing">Pricing</a></li>
                                <li><a href="#free-calculator">Calculator</a></li>
                                <li><a href="#whatnext">What Next</a></li>
                                <li><a href="#contact">Contact Us</a></li>                                
                            </ul> -->
                        </div>
                    </div>
                </nav>
                <div id="search-form-switcher" class="search-collapse-area collapse white">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="white">
                                    <form action="#" class="search-form">
                                        <input type="search" name="search" id="search" placeholder="Search Here..">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END MAINMENU AREA END-->
        </div>
    
    </header>
    <!--END TOP AREA-->

   

    <!--CONTACT US AREA-->
    <section class="contact-area relative padding-100-50" id="contact">
       <!--  <div class="area-bg">ssss</div> -->
        <div class="contact-form-area">
            <div class="container">
                <div class="row">

                    <div id="signup_message" class="alert alert-success" style="display: none">
                      <strong>An email has been sent to your address.</strong> Please follow the link
                    </div>


                    <div class="col-md-offset-3 col-lg-offset-3 col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <div class="contact-form mb50 wow fadeIn">
                        <br>
                        <br>
                        <h2 style="display: inline;">SIGN IN HERE</h2> <h4 style="display: inline;float:right;font-style: italic;">Are you new? <a href="signup"><b>Sign up</b></a></h4>

                            <?php
                                 if(isset($_SESSION['success'])){?>
                                <div class="alert alert-success"><?php echo $_SESSION['success']; ?></div>
                             <?php }?>

                              <?php
                                if(isset($_SESSION['error'])){?>
                                <div class="alert alert-danger"><?php echo $_SESSION['error']; ?></div>
                             <?php }?>


                                <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>


                  <form action="" method="post">            
                           <div class="form-group">                
                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
                          </div>
<!-- <?php echo base_url();?>auth/reset_password" -->

                          <div class="form-group">              
                            <input type="password" name="password" class="form-control" id="password1" placeholder="Password" required>
                            <a href="" data-toggle="modal" data-target="#myModal" style="float: right;margin-top: -15px;margin-bottom: 5px; font-style: italic;">Forgot password?</a>
                          </div> 

                          <?php 
                            if (isset($_GET['captcha'])) {?>
                                <div class="form-group">
                                    <div class="g-recaptcha" data-sitekey="6LdbomwUAAAAAKpMGtsF61-X4BVKJHX19aqCOarK" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
                                   <!--  <input class="form-control d-none" data-recaptcha="true" required data-error="Please complete the Captcha"> -->
                                    <div class="help-block with-errors"></div>

                                   <!--  <div class="g-recaptcha" data-sitekey="6LdbomwUAAAAAKpMGtsF61-X4BVKJHX19aqCOarK"></div> -->

                                </div>


                            <?php  }
                          ?>             
                          <button type="submit" class="btn btn-default btn-block" name="signin">Sign In</button>
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <form action="">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please enter your email address to reset your password.</h4>
      </div>
      <div class="modal-body">
        <input type="email" id="reset_pass_email" name="reset_pass_email" class="form-control" placeholder="Type email here">
      </div>
      <div class="modal-footer">
        <button type="button" id="reset_submit" class="btn btn-default" data-dismiss="modal" >Submit</button>
      </div>
    </div>
   </form>
  </div>
</div>




    <!--FOOER AREA-->
    <div class="footer-area white">
       
        <div class="footer-bottom-area blue-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-social-bookmark text-center wow fadeIn">
                            <div class="footer-logo mb50">
                                <a href="#"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul class="social-bookmark">
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                            <p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--FOOER AREA END-->


    <!--====== SCRIPTS JS ======-->
    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/jquery-1.12.4.min.js" ></script>
     <script type="text/javascript" src="<?php echo base_url();?>js/vendor/bootstrap.min.js" ></script>
   

    <!--====== PLUGIbootstrapNS JS ======-->
    <script src="<?php echo base_url();?>js/vendor/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url();?>js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url();?>js/vendor/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>js/slick.min.js"></script>
    <script src="<?php echo base_url();?>js/stellar.js"></script>
    <script src="<?php echo base_url();?>js/wow.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-modal-video.min.js"></script>
    <script src="<?php echo base_url();?>js/stellarnav.min.js"></script>
    <script src="<?php echo base_url();?>js/contact-form.js"></script>
    <script src="<?php echo base_url();?>js/jquery.ajaxchimp.js"></script>
    <script src="<?php echo base_url();?>js/jquery.sticky.js"></script>

    <!--===== ACTIVE JS=====-->
    <script src="<?php echo base_url();?>js/main.js"></script>

      <script src='https://www.google.com/recaptcha/api.js'></script>
</body>

</html>

<script type="text/javascript">
    $(document).ready(function() {
         
         $("#reset_submit").click(function(){ 
             var reset_pass_email = $('#reset_pass_email').val();

            $.ajax({
                url:"<?php echo base_url();?>auth/reset_password",
                method:"POST",
                data:{reset_pass_email:reset_pass_email},
                dataType:"text",
                success: function(data) {
                  //console.log(data);
                  //$("#signup_message").html("Hello");
                  $("#signup_message").show().delay(30000).fadeOut();
               },
               error: function(err) {
              }                
          });



         });
    });
</script>
