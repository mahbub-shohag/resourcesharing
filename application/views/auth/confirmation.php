<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en" ng-app="signupApp" ng-controller="signupController">
<!--<![endif]-->

<head>    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="It's an Saas based ERP" />
    <meta name="keywords" content="ERP, Calculator, Monthly Subscription" />   
    <title>Signup | ProjectName</title>
   
    <link rel="shortcut icon" type="image/ico" href="img/favicon.png" />

   
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/normalize.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/animate.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/modal-video.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/stellarnav.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/owl.carousel.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/slick.css">

    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap.min.css"> 
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/font-awesome.min.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/material-icons.css">
    
    

    <!--====== MAIN STYLESHEETS ======-->
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/responsive.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/signup.css">
    

    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/modernizr-2.8.3.min.js" ></script>

    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/jquery-1.12.4.min.js" ></script>




<script src="<?php echo base_url();?>js/jquery.complexify.js/jquery.complexify.banlist.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.complexify.js/jquery.complexify.js" type="text/javascript"></script>



    <script type="text/javascript" src="<?php echo base_url();?>js/custom.js" ></script>
   <!--  <script type="text/javascript" src="<?php echo base_url();?>js/signup.js" ></script> -->
   
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="home-one" data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#home" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header>
        <div class="header-top-area">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a href="<?php echo base_url(); ?>LandingPage/first" class="navbar-brand">

                              <?php 
                                echo img('img/logoCopy.png');
                              ?>
                              
                            </a>
                        </div>
                        <div id="main-nav" class="stellarnav">
                            <div class="search-and-signup-button white pull-right hidden-md hidden-sm hidden-xs">
                                <button data-toggle="collapse" data-target="#search-form-switcher"><i class="fa fa-search"></i></button>
                                <a href="signin" class="sign-up">Sign In</a>
                            </div>
                           
                        </div>
                    </div>
                </nav>
                <div id="search-form-switcher" class="search-collapse-area collapse white">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="white">
                                    <form action="#" class="search-form">
                                        <input type="search" name="search" id="search" placeholder="Search Here..">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END MAINMENU AREA END-->
        </div>
    
    </header>
    <!--END TOP AREA-->

    <!--CONTACT US AREA-->
    <section class="contact-area relative padding-100-50" id="contact">
       <!--  <div class="area-bg">ssss</div> -->
        <div class="contact-form-area">
            <div class="container" style="margin-top: 200px;margin-bottom: 150px;">

                <?php
                  // echo '<pre>';
                  // print_r($_SESSION);
                  // exit; 
                  if(isset($_SESSION['success'])){?>
                    <div class="alert alert-success text-center"><?php echo $_SESSION['success']; ?></div>
                 <?php }?>

                  <?php
                  if(isset($_SESSION['error'])){?>
                    <div class="alert alert-danger text-center"><?php echo $_SESSION['error']; ?></div>

                 <?php }?>


                 <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>


        
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->

    <!--FOOER AREA-->
    <div class="footer-area white">
       
        <div class="footer-bottom-area blue-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-social-bookmark text-center wow fadeIn">
                            <div class="footer-logo mb50">
                                <a href="#"><img src="img/logo.png" alt=""></a>
                            </div>
                            <ul class="social-bookmark">
                                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a class="google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                        <div class="footer-copyright text-center wow fadeIn">
                            <p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--FOOER AREA END-->


    <!--====== SCRIPTS JS ======-->
  
     <script type="text/javascript" src="<?php echo base_url();?>js/vendor/bootstrap.min.js" ></script>
   

    <!--====== PLUGIbootstrapNS JS ======-->
    <script src="<?php echo base_url();?>js/vendor/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url();?>js/vendor/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url();?>js/vendor/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>js/slick.min.js"></script>
    <script src="<?php echo base_url();?>js/stellar.js"></script>
    <script src="<?php echo base_url();?>js/wow.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery-modal-video.min.js"></script>
    <script src="<?php echo base_url();?>js/stellarnav.min.js"></script>
    <script src="<?php echo base_url();?>js/contact-form.js"></script>
    <script src="<?php echo base_url();?>js/jquery.ajaxchimp.js"></script>
    <script src="<?php echo base_url();?>js/jquery.sticky.js"></script>

    <!--===== ACTIVE JS=====-->
    <script src="<?php echo base_url();?>js/main.js"></script>
   <!-- Complexify -->

</body>

</html>

<script type="text/javascript">
  //password strength bar
  var pass= document.getElementById('password1');
  pass.addEventListener('keyup', function() {
    checkPassword(pass.value)
  })

  function checkPassword(password){
    var strengthBar= document.getElementById('strength');
    var strength = 0;
    if (password.match(/[a-z]+/)) {
        strength += 1;
    }
    if (password.match(/[A-Z]+/)) {
        strength += 1;
    }

    if (password.match(/[0-9]+/)) {
        strength += 1;
    }

    if (password.match(/[!@#$%^&*()~<>?]+/)) {
        strength += 1;
    }

    if (password.length>5) {
       strength += 1;

    }
    switch(strength){
      case 0:
              strengthBar.value=0;
              check_img.classList.remove("chk");
              check_img.classList.add("unchk");
              break
      case 1:
              strengthBar.value=20;
              check_img.classList.remove("chk");
              check_img.classList.add("unchk");
              break         

      case 2:
              strengthBar.value=40;
              check_img.classList.remove("chk");
              check_img.classList.add("unchk");
              break

      case 3:
              strengthBar.value=60;
              check_img.classList.remove("chk");
               check_img.classList.add("unchk");
              break
      case 4:
              strengthBar.value=80;
              check_img.classList.remove("chk");
              check_img.classList.add("unchk");
              break
      case 5:
              strengthBar.value=100;
 				
 				check_img.classList.add("chk");
    			check_img.classList.remove("unchk");

              break
            
    }
  }



</script>



