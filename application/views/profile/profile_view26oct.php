<?php
    $this -> load -> view('_header');
        if($_SESSION['subscribed'] != 'yes'){ 
        $this->session->set_flashdata("error", "You are not a subscribed user till now. Please be subscribed first to access our system.");
        redirect("dashboard/payment_option", "refresh");
    }
?>
 <script type="text/javascript" src="<?php echo base_url();?>js/profile.js"></script>  <!-- -->

<div ng-controller="profileController">
                
    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
      <div class="alert alert-{{alertClass}}" style="display: none" id="alertMessage">
        {{alertMessage}}
      </div>

        <div class="container bootstrap snippet">
    <div class="row">
      <div class="col-sm-10"><h1>User Info</h1></div>
      <div class="col-sm-2"><a href="/users" class="pull-right"><img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"></a></div>
    </div>
    <div class="row">

          
        </div><!--/col-3-->
      <div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#basic">Basic Info</a></li>
                <li><a data-toggle="tab" href="#change">Change Password</a></li>
                <li><a data-toggle="tab" href="#payment">Payment Info</a></li>
              </ul>

              
        <div class="tab-content">
          <div class="tab-pane active" id="basic">
            <div class="panel panel-default">
              <div class="panel-heading">
                User Basic Info
                <button class="btn btn-default" data-toggle="modal" data-target="#profileUpdateModal" style="float: right;margin-top: -7px;">Update Info</button>
              </div>
              <div class="panel-body">
                <table class="table table-bordered">
                  <tr>
                    <th>Name</th>
                    <td>{{profile.name}}</td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td>{{profile.email}}</td>
                  </tr>
                  <tr>
                    <th>Company</th>
                    <td>{{profile.company_name}}</td>
                  </tr>
                  <tr>
                    <th>Status</th>
                    <td>{{profile.status}}</td>
                  </tr>
                  
                </table>
              </div>
            </div>
          </div>  
              <?php
              //echo '<pre>';
              //print_r($user_info);
              ?>
                
              
             <!--/tab-pane-->
                   <div class="tab-pane" id="change">
                    <div class="panel panel-default" style="padding: 16px">
                      <div class="panel-heading">
                        Change Password
                        
                       </div>
                       <div class="form-group">
                          <label>Current Password:</label>
                          <input type="password" ng-model="password.current" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>New Password:</label>
                          <input type="password" ng-model="password.new" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Confirm Password:</label>
                          <input type="password" ng-change="checkPassMatch()" ng-model="password.confirm" class="form-control">
                      </div>
                      <button ng-show="password_matched" ng-click="passwordUpdate()" class="btn btn-default">Update Password</button>
                      </div>    
                </div>
               <h2></h2>
               
             </div><!--/tab-pane-->
             <div class="tab-pane" id="payment">
                
                
                  <hr>
<?php
    $this -> load -> view('profile/passwordChangeModal');
    $this -> load -> view('profile/updateProfileBasic');
?>
                  
              </div>
               
              </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->


    </div>
</div> <!--controller ends-->
           


</div>
</div>
</section>
<!--CONTACT US AREA END-->

</div>

<?php
    $this -> load -> view('_footer');
?>