<div class="modal fade" id="profileUpdateModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Profile</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" ng-model="profile.name" class="form-control" id="name">
          </div>
          <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" ng-model="profile.email" class="form-control" id="email">
          </div>
          <div class="form-group">
            <label for="company">Company:</label>
            <input type="text" ng-model="profile.company_name" class="form-control" id="company">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" ng-click="updateProfileBasic()" class="btn btn-default" data-dismiss="modal">Update</button>
        </div>
      </div>
      
    </div>
  </div>