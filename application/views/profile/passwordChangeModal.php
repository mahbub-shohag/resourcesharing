<div class="modal fade" id="passwordChangeModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Password</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="cpass">Current Password:</label>
            <input type="text" ng-model="password.current" class="form-control" id="cpass">
          </div>
          <div class="form-group">
            <label for="npass">New Password:</label>
            <input type="email" ng-model="password.new" class="form-control" id="npass">
          </div>
          <div class="form-group">
            <label for="cp">Confirm Password:</label>
            <input type="text" ng-model="password.confirm" class="form-control" id="cp">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" ng-click="updatePassword()" class="btn btn-default" data-dismiss="modal">Update</button>
        </div>
      </div>
      
    </div>
  </div>