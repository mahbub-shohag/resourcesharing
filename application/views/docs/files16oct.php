<?php
    $this -> load -> view('_header');
?>  

<script type="text/javascript" src="<?php echo base_url();?>js/docs.js" ></script>
<div class="col-md-8" ng-controller="docsController">
<div class="row">
  <div class="col-md-12">
      <ul>
          <li class="link_li" ng-repeat="directory in directory_queue">
              <span ng-click="findChilds(directory)">{{directory.name}}</span>
          </li>
      </ul>
      <div class="btn-group" style="margin-left: 210px">
          <button type="button" class="btn btn-primary">Share</button>
          <button type="button" class="btn btn-primary">Download</button>
          <button type="button" class="btn btn-primary">Move</button>
          <button type="button" class="btn btn-primary">Delete</button>
          <button type="button" class="btn btn-primary">Copy</button>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fileName">New Folder</button>
     </div> 
      <input type="file" style="background: #fff;float: right;" id="image" name="">
  
  </div>
    
</div>
  
<div class="row">
  <div  class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <table class="table table-hover">
      <thead>
        <th>Name</th>
        <th>Date</th>
        <th>Shared By</th>
      </thead>
      <tbody>
        <tr ng-repeat="item in items">
          <td ng-show="item.type_name==='folder'"><span ng-click="findChilds(item)"><i class="fa fa-folder" aria-hidden="true"></i>{{item.name}}</span></td>
          <td ng-show="item.type_name!='folder'">{{item.name}}</td>
          <td>10-08-2018</td>
          <td>
            <button class="btn btn-success" data-toggle="modal" data-target="#shareUsers" ng-click="sharedItem(item)">Share</button>
            <input type="checkbox" name="">
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>        
  

  <div class="row">
      <h4>Shared Documents</h4>    
   <div  class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <table class="table table-hover">
      <thead>
        <th>Name</th>
        <th>Date</th>
        <th>Shared By</th>
      </thead>
      <tbody>
        <tr ng-repeat="sharedItem in SharedItems">
          <td ng-show="sharedItem.type_name==='folder'"><span ng-click="findChilds(sharedItem)"><i class="fa fa-folder" aria-hidden="true"></i>{{sharedItem.name}}</span></td>
          <td ng-show="sharedItem.type_name!='folder'">{{sharedItem.name}}</td>
          <td>{{sharedItem.created_date | date:'dd/MM/yyyy'}}</td>
          <td>{{sharedItem.shared_by}}</td>
        </tr>
      </tbody>
    </table>
  </div>
  </div>


    <?php
        $this->load->view('docs/fileNameModal');
        $this->load->view('docs/shareUserModal');
    ?>
</div>
<!--Row 8 ends Controller Ends-->



</div>
<!--Main Row Ends--> 


</div>
<!--container-fluid ends-->
</div>
<!--dashboard area ends-->
</section>

</div>    

<?php
    $this -> load -> view('_footer');
?>