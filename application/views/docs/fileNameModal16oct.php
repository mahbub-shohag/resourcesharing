<div id="fileName" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">File/Directory Name</h4>
      </div>
      <div class="modal-body">
        <input type="text" ng-model="directory" class="form-control" name="">
      </div>
      <div class="modal-footer">
        <button type="button" ng-click="makeDirectory()" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>