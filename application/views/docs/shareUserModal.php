<div id="shareUsers" role="dialog" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<span style="font-weight: bold;">Share Settings</span>
				<button class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<h6>Collaborators</h6>
				<div class="panel panel-default">
					<div class="panel-heading">
						<input type="email" ng-model="sharedEmail" class="form-control">
						<div style="margin-top: 10px">
							<button class="btn btn-default btn-xs" ng-click="findUserAndShare(sharedEmail)">Share</button>
							<button class="btn btn-default btn-xs">Cancel</button>	
						</div>
						<div display="none" id="emailExistMessage" class="alert alert-{{messageclass}}">
						  {{message}}
						</div>
						
					</div>
					<div class="panel-body" style="min-height: 150px">
						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>