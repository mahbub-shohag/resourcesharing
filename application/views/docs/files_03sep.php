<?php
    $this -> load -> view('_header');
?>   
<script type="text/javascript" src="<?php echo base_url();?>js/docs.js" ></script>
<!--Controller Starts-->

<div ng-controller="docsController">       
    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
        <ul>
            <li class="link_li" ng-repeat="directory in directory_queue">
                <span ng-click="findChilds(directory)">{{directory.name}}</span>
            </li>
        </ul>
            <button class="btn btn-success" style="float: right;" ng-click="uploadFile()">Upload
        </button>
        <div class="dropdown" style="float: right;">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">ADD
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li class="dropdown-header" data-toggle="modal" data-target="#fileName" style="color: green;text-decoration: double;">Add Folder</li>
              <li><input type="file" id="image" name=""></li>
            </ul>
        </div>
        
    <div class="panel-group">
        
        <div class="panel panel-success">
          <div class="panel-heading">Folders</div>
          <div class="panel-body">
              <ul>
                <li class="folder" ng-repeat="folder in folders">
                     <span class="glyphicon glyphicon-folder-open"></span><br>
                     <span ng-click="findChilds(folder)">{{folder.name}}</span>
                </li>
            </ul>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">Image Files</div>
          <div class="panel-body">
              <ul>
                <li ng-repeat="image_file in image_files" style="float: left;padding: 5px;margin-left: 48px;margin-top: 22px;">
                        <span><i class="glyphicon glyphicon-picture"></i></span>
                        <a href="/chanul/itinery/{{image_file.name}}" download='{{image_file.name}}'>  
                        </a><br>  
                        {{image_file.name}}
                </li>
            </ul> 

          </div>
        </div>

        <div class="panel panel-primary">
          <div class="panel-heading">PDF</div>
          <div class="panel-body">
              <ul>
                <li ng-repeat="pdf in pdfs" style="float: left;padding: 5px;margin-left: 48px;margin-top: 22px;">
                        <span class="glyphicon glyphicon-file"></span>
                        <a href="/chanul/itinery/{{item.name}}" download='{{pdf.name}}'><br>    
                        {{pdf.name}}
                    </a>
                </li>
            </ul>

          </div>
        </div>       
        <div class="panel panel-info">
          <div class="panel-heading">Others</div>
          <div class="panel-body">Not Available</div>
        </div>
  </div>





    </div>
    <?php
        $this->load->view('docs/fileNameModal');
    ?>


</div>
<!--Controller Ends-->


                </div>
            </div>
        </div>
    </section>

</div>    

<?php
    $this -> load -> view('_footer');
?>