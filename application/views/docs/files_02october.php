<?php
    $this -> load -> view('_header');
?>   
<script type="text/javascript" src="<?php echo base_url();?>js/docs.js" ></script>
<!--Controller Starts-->

      
  <!--  
    <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
        <ul>
            <li class="link_li" ng-repeat="directory in directory_queue">
                <span ng-click="findChilds(directory)">{{directory.name}}</span>
            </li>
        </ul>
        <div class="dropdown" style="float: right;width: 163px;border-radius:44px;border-radius: 27px;height: 56px;">
            <button style="border-radius:44px;border-radius: 27px;height: 56px;width:135px" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"></span>ADD
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li class="dropdown-header" data-toggle="modal" data-target="#fileName" style="color:#fff;text-decoration: double;    background: #8bc34a;">Add Folder<span class="glyphicon glyphicon-plus"></span></li>
              <li><input type="file" style="background: #ffc107" id="image" name=""></li>
            </ul>
        </div>
        
    <div class="panel-group">
        
        <div class="panel panel-success">
          <div class="panel-heading">Folders</div>
          <div class="panel-body">
              <ul>
                <li ng-click="findChilds(folder)" class="folder" ng-repeat="folder in folders">
                     <span  class="glyphicon glyphicon-folder-open"></span><br>
                     <span>{{folder.name}}</span>
                </li>
            </ul>

          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading">Image Files</div>
          <div class="panel-body">
              <ul>
                <li ng-repeat="image_file in image_files" style="float: left;padding: 5px;margin-left: 48px;margin-top: 22px;">
                    <a href="/chanul{{image_file.item_link}}" target="_blank">
                        <img height="60px" width="60px" src="/chanul{{image_file.item_link}}">
                        <br>  
                        <span style="margin-left: -29px;">{{image_file.name}}</span>
                    </a>    
                </li>
            </ul> 

          </div>
        </div>

        <div class="panel panel-primary">
          <div class="panel-heading">PDF</div>
          <div class="panel-body">
              <ul>
                <li ng-repeat="pdf in pdfs" style="float: left;padding: 5px;margin-left: 48px;margin-top: 22px;">
                        <a href="/chanul{{pdf.item_link}}" target="_blank">
                            <span class="glyphicon glyphicon-file"></span></br>
                            {{pdf.name}}
                        </a>
                </li>
            </ul>

          </div>
        </div>       
        <div class="panel panel-info">
          <div class="panel-heading">Others</div>
          <div class="panel-body">
            <ul>
                <li ng-repeat="other in others" style="float: left;padding: 5px;margin-left: 48px;margin-top: 22px;">
                    <a href="/chanul{{other.item_link}}" target="_blank">
                        <span class="glyphicon glyphicon-duplicate"></span></br>
                        {{other.name}}
                    </a>
                </li>
            </ul>
        </div>
        </div>
  </div>



-->
<div ng-controller="docsController"> 
  <ul>
      <li class="link_li" ng-repeat="directory in directory_queue">
          <span ng-click="findChilds(directory)">{{directory.name}}</span>
      </li>
  </ul>
  <div class="btn-group" style="margin-left: 400px">
  <button type="button" class="btn btn-primary">Share</button>
  <button type="button" class="btn btn-primary">Download</button>
  <button type="button" class="btn btn-primary">Move</button>
  <button type="button" class="btn btn-primary">Delete</button>
  <button type="button" class="btn btn-primary">Copy</button>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fileName">New Folder</button>
 </div> 
  <input type="file" style="background: #fff;float: right;" id="image" name=""></li>
  
  <!-- <div class="dropdown" style="float: right;width: 163px;border-radius:44px;border-radius: 27px;height: 56px;">
            <button style="border-radius:44px;border-radius: 27px;height: 56px;width:135px" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">ADD
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li class="dropdown-header" data-toggle="modal" data-target="#fileName" style="color:#fff;text-decoration: double;    background: #8bc34a;">Add Folder<span class="glyphicon glyphicon-plus"></span></li>
              <li><input type="file" style="background: #ffc107" id="image" name=""></li>
            </ul>
        </div> -->
  <div  class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
    <table class="table table-hover">
      <thead>
        <th>Name</th>
        <th>Date</th>
        <th>Share</th>
      </thead>
      <tbody>
        <tr ng-repeat="item in items">
          <td ng-show="item.type_name==='folder'"><span ng-click="findChilds(item)"><i class="fa fa-folder" aria-hidden="true"></i>{{item.name}}</span></td>
          <td ng-show="item.type_name!='folder'">{{item.name}}</td>
          <td>10-08-2018</td>
          <td>
            <button class="btn btn-success">Share</button>
            <input type="checkbox" name="">
          </td>
        </tr>
      </tbody>
    </table>
  </div>
<?php
        $this->load->view('docs/fileNameModal');
    ?>

    </div>
    


</div>
<!--Controller Ends-->


                </div>
            </div>
        </div>
    </section>

</div>    

<?php
    $this -> load -> view('_footer');
?>