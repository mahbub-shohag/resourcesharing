<div id="shareUsers" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Users</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
          <thead>
            <th>Sl#</th>
            <th>Name</th>
            <th>Email</th>
            <th></th>
          </thead>
          <tbody>
            <tr ng-repeat="user in users">
              <td>{{$index+1}}</td>
              <td>{{user.name}}</td>
              <td>{{user.email}}</td>
              <td><button class="btn btn-info" ng-click="share(user)" data-dismiss="modal">Share</button></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
      </div>
    </div>

  </div>
</div>