<?php
    $this -> load -> view('_header');
    if($_SESSION['subscribed'] != 'yes'){ 
        $this->session->set_flashdata("error", "You are not a subscribed user till now. Please be subscribed first to access our system.");
        redirect("dashboard/payment_option", "refresh");
    }
?>  

<script type="text/javascript" src="<?php echo base_url();?>js/docs.js" ></script>
<div class="col-md-10" ng-controller="docsController">
<div class="row">
  <div id="alertMessage" style="display: none;" class="col-md-12">
    <div class="alert alert-danger">
      <strong>{{alertMessage}}!</strong>
    </div>
  </div>
  <div id="successMessage" style="display: none;" class="col-md-12">
    <div class="alert alert-success">
      <strong>{{alertMessage}}!</strong>
    </div>
  </div>
    
</div>
  
<div class="row">
  <div  class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <div class="path">
        <ul>
          <li class="link_li" ng-repeat="directory in directory_queue">
              <span ng-click="findChilds(directory)">{{directory.name}}</span>
          </li>
      </ul>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <button ng-show="copiedItems.length>0" ng-click="paste()" class="btn btn-success">Paste</button>
          <button ng-show="movedItems.length>0" ng-click="cutPaste()" class="btn btn-success">Paste</button>
          <!--<button type="button" data-toggle="modal" data-target="#shareUsers" class="btn btn-primary">Share</button>-->
          <button type="button" class="btn btn-primary" ng-click="move()">Move</button>
          <button type="button" ng-click="delete()" class="btn btn-primary">Delete</button>
          <button type="button" ng-click="copy()" class="btn btn-primary">Copy</button>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fileName">New Folder</button>
          <input type="file" style="background: #fff;float: right;margin-right: 550px" id="image" name="">
      </div>

      <div class="panel-body">
        <table class="table" id="docs">
          <thead>
            <th><input type="checkbox"></th>
            <th>Name</th>
            <th>Modified By</th>
            <th>Time Created</th>
            <th></th>
          </thead>
          <tbody>
            <tr ng-repeat="item in items track by $index">
              <td>
                <input type="checkbox" name="" ng-model="item.checked">
              </td>
              <td ng-show="item.type_name==='folder'" ng-click="findChilds(item)">
                <span>
                  <i class="fa fa-folder" aria-hidden="true"></i>
                </span>{{item.name}}
              </td>
              
              <td ng-show="item.type_name!='folder'">
                  {{item.name}}
              </td>
              
              <td>{{item.created_by_name}}</td>
              <td>{{item.created_date}}</td>
              <td>
                <a ng-show="item.type_name!='folder'" href="{{item.item_link}}" download='{{item.name}}'>
                  <i class="fa fa-download" aria-hidden="true"></i>
                </a>
                <span data-toggle="modal" data-target="#shareUsers" ng-click="share(item)">
                  <i class="fa fa-share-alt" aria-hidden="true"></i>
                </span>
              </td>
              
            </tr>
          </tbody>
        </table>
      </div>
      
    </div>
  </div>
</div>        
    <?php
        $this->load->view('docs/fileNameModal');
        $this->load->view('docs/shareUserModal');
    ?>
</div>
<!--Row 8 ends Controller Ends-->



</div>
<!--Main Row Ends--> 


</div>
<!--container-fluid ends-->
</div>
<!--dashboard area ends-->
</section>

</div>    

<?php
    $this -> load -> view('_footer');
?>