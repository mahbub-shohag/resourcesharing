<?php
    $this -> load -> view('_header');
    if($_SESSION['subscribed'] != 'yes'){ 
        $this->session->set_flashdata("error", "You are not a subscribed user till now. Please be subscribed first to access our system.");
        redirect("dashboard/payment_option", "refresh");
    }
?>  

<script type="text/javascript" src="<?php echo base_url();?>js/docs.js" ></script>
<div class="col-md-8" ng-controller="docsController">
<div class="row">
  <div id="alertMessage" style="display: none;" class="col-md-12">
    <div class="alert alert-danger">
      <strong>{{alertMessage}}!</strong>
    </div>
  </div>
  <div id="successMessage" style="display: none;" class="col-md-12">
    <div class="alert alert-success">
      <strong>{{alertMessage}}!</strong>
    </div>
  </div>
  
  <div class="col-md-12">
      <ul>
          <li class="link_li" ng-repeat="directory in directory_queue">
              <span ng-click="findChilds(directory)">{{directory.name}}</span>
          </li>
      </ul>
      <div class="btn-group" style="margin-left: 210px">
          <button ng-show="copiedItems.length>0" ng-click="paste()" class="btn btn-success">Paste</button>
          <button type="button" data-toggle="modal" data-target="#shareUsers" class="btn btn-primary">Share</button>
          <button type="button" class="btn btn-primary">Download</button>
          <button type="button" class="btn btn-primary">Move</button>
          <button type="button" ng-click="delete()" class="btn btn-primary">Delete</button>
          <button type="button" ng-click="copy()" class="btn btn-primary">Copy</button>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fileName">New Folder</button>
     </div> 
      <input type="file" style="background: #fff;float: right;" id="image" name="">
  
  </div>
    
</div>
  
<div class="row">
  <div  class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <table class="table table-hover">
      <thead>
        <th>Name</th>
        <th>Date</th>
        <th>Check</th>
      </thead>
      <tbody>
        <tr ng-repeat="item in items track by $index">
          <td ng-show="item.type_name==='folder'"><span ng-click="findChilds(item)"><i class="fa fa-folder" aria-hidden="true"></i>{{item.name}}</span></td>
          <td ng-show="item.type_name!='folder'">{{item.name}}</td>
          <td>10-08-2018</td>
          <td>
            <input type="checkbox" name="" ng-model="item.checked">
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>        
  

  <div class="row">
      <h4>Shared Documents</h4>    
   <div  class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
    <table class="table table-hover">
      <thead>
        <th>Name</th>
        <th>Date</th>
        <th>Shared By</th>
      </thead>
      <tbody>
        <tr ng-repeat="sharedItem in SharedItems track by $index">
          <td ng-show="sharedItem.type_name==='folder'"><span ng-click="findChilds(sharedItem)"><i class="fa fa-folder" aria-hidden="true"></i>{{sharedItem.name}}</span></td>
          <td ng-show="sharedItem.type_name!='folder'">{{sharedItem.name}}</td>
          <td>{{sharedItem.created_date | date:'dd/MM/yyyy'}}</td>
          <td>{{sharedItem.shared_by}}</td>
        </tr>
      </tbody>
    </table>
  </div>
  </div>


    <?php
        $this->load->view('docs/fileNameModal');
        $this->load->view('docs/shareUserModal');
    ?>
</div>
<!--Row 8 ends Controller Ends-->



</div>
<!--Main Row Ends--> 


</div>
<!--container-fluid ends-->
</div>
<!--dashboard area ends-->
</section>

</div>    

<?php
    $this -> load -> view('_footer');
?>