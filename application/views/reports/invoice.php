<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="report_area1">
       
        <h3>Calculation  <?php echo $ship_info['fileName']; ?> </h3>
        <hr>        
        <table class="table table-bordered">
         
           <tbody>
             <tr>
               <td>Name:</td>             
               <td><?php echo $ship_info['shipName']; ?> </td>
             </tr>
             <tr>
               <td>Acivity:</td>              
               <td> <?php echo $ship_info['activity']; ?></td>
             </tr>

             <tr>
               <td>Arrival:</td>              
               <td><?php echo $ship_info['arrival']; ?> </td>
             </tr>

             <tr>
               <td>Bearthing:</td>              
               <td> <?php echo $ship_info['berthing']; ?></td>
             </tr>

             <tr>
               <td>Cargo:</td>              
               <td><?php echo $ship_info['cargo']; ?> </td>
             </tr>

             <tr>
               <td>Completion:</td>              
               <td><?php echo $ship_info['completion']; ?> </td>
             </tr>

             <tr>
               <td>DemurrageRate:</td>              
               <td> <?php echo $ship_info['demurrageRate']; ?></td>
             </tr>

             <tr>
               <td>Demurrage Rate Unit:</td>              
               <td><?php echo $ship_info['demurrageRateUnit']; ?> </td>
             </tr>
         
             <tr>
               <td>Despatch Rate</td>              
               <td><?php echo $ship_info['despatchRate']; ?> </td>
             </tr>

             <tr>
               <td>Despatch Rate Unit:</td>              
               <td> <?php echo $ship_info['despatchRateUnit']; ?></td>
             </tr>

             <tr>
               <td>Loa:</td>              
               <td><?php echo $ship_info['loa']; ?> </td>
             </tr>
             <tr>
               <td>IMO:</td>              
               <td><?php echo $ship_info['imo']; ?> </td>
             </tr>
             <tr>
               <td>DWT:</td>              
               <td><?php echo $ship_info['dwt']; ?> </td>
             </tr>
             <tr>
               <td>Nor Tendered:</td>              
               <td><?php echo $ship_info['norTendered']; ?> </td>
             </tr>
             <tr>
               <td>Port:</td>              
               <td><?php echo $ship_info['port']; ?> </td>
             </tr>
             <tr>
               <td>Quantity:</td>              
               <td><?php echo $ship_info['quantity']; ?> </td>
             </tr>
              <tr>
               <td>Rate:</td>              
               <td><?php echo $ship_info['rate']; ?> </td>
             </tr>
              <tr>
               <td>Rate Unit:</td>              
               <td><?php echo $ship_info['rateUnit']; ?> </td>
             </tr>
              <tr>
               <td>Time Start:</td>              
               <td><?php echo $ship_info['timeStart']; ?> </td>
             </tr>

         </tbody>
       </table>
       <table class="table table-bordered">
        <tbody>
             <h3>Port Times:</h3>

              <tr>
               <td>Arrival:</td>              
               <td><?php echo $ship_info['dwt']; ?> </td>
             </tr>
             <tr>
               <td>Berthing:</td>              
               <td> <?php echo $ship_info['dwt']; ?></td>
             </tr>
             <tr>
               <td>Nor Tendered:</td>              
               <td><?php echo $ship_info['dwt']; ?> </td>
             </tr>

             <tr>
               <td>Time start:</td>              
               <td> <?php echo $ship_info['dwt']; ?></td>
             </tr>

              <tr>
               <td>Completion:</td>              
               <td><?php echo $ship_info['dwt']; ?> </td>
             </tr>

             
           </tbody>
         </table>  
      </div>
</body>
</html>