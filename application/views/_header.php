<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="It's an Saas based ERP" />
    <meta name="keywords" content="ERP, Calculator, Monthly Subscription" />   
    <title>Welcome to ProjectName</title>
   
    <link rel="shortcut icon" type="image/ico" href="img/favicon.png" />

   
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/normalize.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/animate.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/modal-video.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/stellarnav.min.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/owl.carousel.css">
    <link rel="stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/slick.css">

    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap.min.css"> 
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/font-awesome.min.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/material-icons.css">
    
    

    <!--====== MAIN STYLESHEETS ======-->
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/mystyle.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>assets/css/dashboard.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/responsive.css">
    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/jquery-ui.min.css">

    <link rel = "stylesheet" type = "text/css" href = "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
   

    <!--====== SCRIPTS JS ======-->
    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/jquery-1.12.4.min.js" ></script>
     <script type="text/javascript" src="<?php echo base_url();?>js/vendor/bootstrap.min.js" ></script>
    

    <script type="text/javascript" src="<?php echo base_url();?>js/vendor/modernizr-2.8.3.min.js" ></script>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css">
   
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <script type="text/javascript" src="<?php echo base_url();?>js/angularjs/angular.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url();?>js/angularjs/angular-route.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">    
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">

    <link href="<?php echo base_url();?>dist/css/acute.select.css" rel="stylesheet">   
<link href="<?php echo base_url();?>js/uiselect/select.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.0.0/css/bootstrap-datetimepicker.css" />
<script type="text/javascript" src="<?php echo base_url();?>js/angularjs/angular-sanitize.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/angularjs/angular-animate.min.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/uiselect/select.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>js/setup.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>js/myangular.js"></script> 
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.5.0.js"></script>
</head>

<style type="text/css">
nav.navbar {    
    background: #1193D4;
}

h2 ,p {
    display: inline;
}

</style>
<body ng-app="myApp" class="home-one" data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header>
        <div class="header-top-area">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a href="<?php echo base_url(); ?>" class="navbar-brand">

                                <?php 
                                    echo img('img/logoCopy.png');
                                ?>
                                
                            </a>
                        </div>
                        <div id="main-nav" class="stellarnav">
                            <div class="search-and-signup-button white pull-right hidden-md hidden-sm hidden-xs">
                              
                                
                                 <?php
                                      if(isset($_SESSION['user'])){?>
                                         <p> Hi, <?php echo $_SESSION['user']; ?> | </p> 
                                         <a href="<?php echo base_url(); ?>auth/signout"> Sign Out</a>
                                     <?php }
                                     else{
                                        ?>
                                        <a href="#" class="sign-up">Sign In</a>
                                        <?php redirect("auth/signin", "refresh");
                                    }?>



                            </div>



                            <!-- statr_notification_title -->
                              <div class="notification_title">
                                 <div class="error_message"><i class="fa fa-times" aria-hidden="true"></i>Operation field!!</div>
                                <!--  <div id="success_message" class="success_message"><i class="fa fa-check-square" aria-hidden="true"></i>save successful!</div> -->
                              </div>
                            <!-- end_notification_title --> 


                            
                        
                        </div>
                    </div>
                </nav>
                <div id="search-form-switcher" class="search-collapse-area collapse white">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="white">
                                    <form action="#" class="search-form">
                                        <input type="search" name="search" id="search" placeholder="Search Here..">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--END MAINMENU AREA END-->
        </div>
    
    </header>
    <!--END TOP AREA-->


    <!--CONTACT US AREA-->
    <section class="dasboard-main relative padding-100-50" id="dashboard">
 
       <!--  <div class="area-bg">ssss</div> -->
        <div class="dashbord-area">
            <div class="container-fluid">
                
                <div class="row">


                    <div class="col-md-2 col-lg-2" >
                       <nav class="navbar">
                            <div class="container-fluid">
                               <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsButtonClick" aria-expanded="false" id="clspButton">
                                   <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                                   <span aria-hidden="true">Menu</span>
                                  </button> 
                                </div>
                                <div class="collapse navbar-collapse " id="collapsButtonClick">
                                    <ul class="nav nav-pills nav-stacked" id="dashboard_nav_ul" style="margin-top: 25px;">
                                       <li id="menuDashboardId"><a href="<?php echo base_url(); ?>dashboard"><span class="glyphicon glyphicon-home"></span>Dashboard</a></li>

                                    <!-- <?php
                                            if($_SESSION['subscribed'] == 'yes'){ ?>
                                                <li id="menuDashboardId"><a href="<?php echo base_url(); ?>dashboard"><span class="glyphicon glyphicon-home"></span>Dashboard</a></li>
                                            <?php } else { ?>
                                                <li id="menuDashboardId" class="disable"><a href="<?php echo base_url(); ?>dashboard"><span class="glyphicon glyphicon-home"></span>Dashboard</a></li>
                                           <?php }
                                        ?> -->
                                        
                                          <!--  <li><a href="<?php echo base_url(); ?>dashboard"><span class="glyphicon glyphicon-home"></span>Dashboard</a></li> -->


                                        <li class="dashboard_nav_ul disable"><a href="<?php echo base_url(); ?>Docs">Docs</a></li>
                                        <li class="dashboard_nav_ul disable"><a href="<?php echo base_url(); ?>calculator/itineraries">Itinerary</a></li> 
                                        <li class="dashboard_nav_ul disable"><a href="<?php echo base_url(); ?>calculator/">Calculator</a></li>
                                        <li class="dashboard_nav_ul"><a href="<?php echo base_url(); ?>ProfileController/">Profile</a></li>

                                    </ul>
                                    <!-- <p id="activation_message">Please Select a Record To Active The Menus</p> -->
                                 </div> 
                            </div>       
                        </nav>       

                </div>

                <script type="text/javascript">
                    var user_id = <?php echo json_encode($_SESSION['user_id']) ?>;
                    console.log(user_id);
                </script> 