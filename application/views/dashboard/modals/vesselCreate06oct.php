<!-- Modal -->
  <div class="modal fade" id="vesselCreateModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background:##cddc39">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Vessel</h4>
        </div>
        <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="name">Vessel:</label>
                <ui-select style="background: lightgray;margin-left: 8px;height: 30px;" ng-change="selectShipName()" ng-model="vessel.vessel" theme="selectize" ng-disabled="disabled" title="Choose a District">
	                                  <ui-select-match allow-clear="true">{{$select.selected.name}}</ui-select-match>
	                                  <ui-select-choices repeat="aship in ships | filter: {name:$select.search}">
	                                    <span ng-bind-html="aship.name | highlight: $select.search"></span>
	                                  </ui-select-choices>
                                </ui-select>
              </div>
              <div class="form-group">
              	<div class="row">
              		<div class="col-md-6">
              			<label for="imo">LOA:</label>
                		<input type="text" ng-model="vessel.loa" class="form-control">		
              		</div>
              		<div class="col-md-6">
              			<label for="imo">Year:</label>
                		<input type="text" ng-model="vessel.year" class="form-control">	
              		</div>
              	</div>
              </div>
              <div class="form-group">
              	<div class="row">
              		<div class="col-md-6">
              			<label for="imo">Vessel Type:</label>
                		<input type="text" ng-model="vessel.vessel_type" class="form-control">		
              		</div>
              		<div class="col-md-6">
              			<label for="imo">IMO:</label>
                		<input type="text" ng-model="vessel.imo" class="form-control">	
              		</div>
              	</div>
              </div>
              <div class="form-group">			                
				               <label for="port" class="control-label"><span ng-click="makeFile()">Activity</span></label>
				               <ui-select style="background: lightgray;margin-left: 8px;height: 30px;" ng-model="vessel.activity" theme="selectize" ng-disabled="disabled" title="Choose a District">
	                                  <ui-select-match>{{$select.selected.name}}</ui-select-match>
	                                  <ui-select-choices repeat="activity in activities | filter: {name:$select.search}">
	                                    <span ng-bind-html="activity.name | highlight: $select.search"></span>
	                                  </ui-select-choices>
                                </ui-select>

			  </div>
			  <div class="form-group">			                
				               <label for="port" class="control-label"><span ng-click="makeFile()">PORT</span></label>
				               <ui-select style="background: lightgray;margin-left: 8px;height: 30px;" ng-model="vessel.port" theme="selectize" ng-disabled="disabled" title="Choose a District">
	                                  <ui-select-match>{{$select.selected.name}}</ui-select-match>
	                                  <ui-select-choices repeat="port in ports | filter: {name:$select.search}">
	                                    <span ng-bind-html="port.name | highlight: $select.search"></span>
	                                  </ui-select-choices>
                                </ui-select>

				              </div>
              <div class="form-group">
                <label for="name">Code:</label>
                <input type="text" ng-model="vessel.code" class="form-control">
              </div>
              <div class="form-group">
                <label for="imo">Vessel Id:</label>
                <input type="text" ng-model="vessel.vessel_id" class="form-control">
              </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="submit" ng-click="saveVessel()" data-dismiss="modal" class="btn btn-default">Save</button>
        </div>
      </div>
      
    </div>
  </div>