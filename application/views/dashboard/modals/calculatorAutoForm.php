        <table class="table table-bordered">
         
           <tbody>
             <tr>
               <td>Name:</td>             
               <td>{{ship.ship_name.name}} </td>
             </tr>
             <tr>
               <td>Dwt:</td>              
               <td>{{ship.dwt}} </td>
             </tr>

             <tr>
               <td>Loa:</td>              
               <td>{{ship.loa}} </td>
             </tr>

             <tr>
               <td>Imo:</td>              
               <td> {{ship.imo}}</td>
             </tr>

             <tr>
               <td>Port:</td>              
               <td>{{ship.port.destination.name}} </td>
             </tr>

             <tr>
               <td>Activity:</td>              
               <td>{{ship.activity}} </td>
             </tr>

             <tr>
               <td>Quantity:</td>              
               <td> {{ship.quantity}}</td>
             </tr>

             <tr>
               <td>Cargo:</td>              
               <td>{{ship.cargo}} </td>
             </tr>
         
             <tr>
               <td>Rate of :</td>              
               <td>{{ship.rate}} {{ship.rateUnit}} </td>
             </tr>

             <tr>
               <td>Demurrage Rate:</td>              
               <td>{{ship.demurrageRate}}{{ship.despatchRateUnit}} </td>
             </tr>

             <tr>
               <td>Despatch Rate:</td>              
               <td> {{ship.despatchRate}}{{ship.despatchRateUnit}}</td>
             </tr>
         </tbody>
       </table>
       <table class="table table-bordered">
        <tbody>
             <h3>Port Times:</h3>

              <tr>
               <td>Arrival:</td>              
               <td>{{ship.arrival | date:'MM/dd/yy h:mm a'}}</td>
             </tr>
             <tr>
               <td>Berthing:</td>              
               <td>{{ship.berthing}} </td>
             </tr>
             <tr>
               <td>Nor Tendered:</td>              
               <td>{{ship.norTendered}} </td>
             </tr>

             <tr>
               <td>Time start:</td>              
               <td>{{ship.timeStart}} </td>
             </tr>

              <tr>
               <td>Completion:</td>              
               <td>{{ship.completion}} </td>
             </tr>

             
           </tbody>
         </table>



         <h3>DEDUCTIONS/EXCEPTIONS</h3>
         <table class="table table-bordered">
         	<thead>
         		<th>SL</th>
         		<th>Date</th>
         		<th>From</th>
         		<th>To</th>
         		<th>DESCRIPTION</th>
         	</thead>
         	<tbody>
            <tr ng-repeat="deduction in deductions">
              <td>{{$index}}</td>
              <td>{{deduction.date | date : 'dd-MM-yyyy'}}</td>
              <td>{{deduction.from | date : 'hh:mm'}}</td>
              <td>{{deduction.to | date : 'hh:mm'}}</td>
              <td>{{deduction.description}}</td>
            </tr>
         	</tbody>
         </table>

         <h3>Calculated Part</h3>
         <table class="table table-bordered">
           <thead></thead>
           <tbody>
             <tr>
               <td>TASA</td>
               <td>{{cal.tasa}}</td>
             </tr>
           </tbody>
         </table>


         <div class="footer-btn" style="float: right;margin-bottom: 20px;">
         <button type="button" ng-click="calculate()" class="btn btn-secondary">Calculate</button>
         <span ng-if="ship.id">
            <!-- <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#myModal">Update As</button> 
            <button type="button" ng-click="saveShipInfo()" class="btn btn-secondary">Update</button>  -->    
         </span>
         <span ng-if="!ship.id">
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#myModal" ng-click="saveShipInfo()">save as</button>
            <!-- <button type="button" ng-click="saveShipInfo()" class="btn btn-secondary">save</button> -->      
         </span>
          <!-- <a target="_blank" href="/chanul/calculator/infopdf/{{shipinfoId}}">
          		<button type="button" class="btn btn-secondary">Save Itinery
          		</button>
          </a> -->
          <a ng-if="shipinfoId" href="/chanul/calculator/makePdf/{{shipinfoId}}">
              <button type="button" class="btn btn-secondary">Print</button>
          </a>
          <a ng-if="!shipinfoId">
              <button type="button" ng-click="saveAlert()" class="btn btn-secondary">Print</button>
          </a>
         </div>