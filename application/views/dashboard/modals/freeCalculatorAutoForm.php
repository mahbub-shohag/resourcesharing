<h3>Name of the vehicle: </h3>
        <hr>        
        <table class="table table-bordered">
         
           <tbody>
             <tr>
               <td>Name:</td>             
               <td>{{ship.ship_name.name}} </td>
             </tr>
             <tr>
               <td>Dwt:</td>              
               <td>{{ship.dwt}} </td>
             </tr>

             <tr>
               <td>Loa:</td>              
               <td>{{ship.loa}} </td>
             </tr>

             <tr>
               <td>Imo:</td>              
               <td> {{ship.imo}}</td>
             </tr>

             <tr>
               <td>Port:</td>              
               <td>{{ship.port.destination}} </td>
             </tr>

             <tr>
               <td>Activity:</td>              
               <td>{{ship.activity}} </td>
             </tr>

             <tr>
               <td>Quantity:</td>              
               <td> {{ship.quantity}}</td>
             </tr>

             <tr>
               <td>Cargo:</td>              
               <td>{{ship.cargo}} </td>
             </tr>
         
             <tr>
               <td>Rate of :</td>              
               <td>{{ship.rate}} </td>
             </tr>

             <tr>
               <td>Demurrage Rate:</td>              
               <td>{{ship.demurrageRate}} </td>
             </tr>

             <tr>
               <td>Despatch Rate:</td>              
               <td> {{ship.despatchRate}}</td>
             </tr>
         </tbody>
       </table>
       <table class="table table-bordered">
        <tbody>
             <h3>Port Times:</h3>

              <tr>
               <td>Arrival:</td>              
               <td>{{ship.arrival}} </td>
             </tr>
             <tr>
               <td>Berthing:</td>              
               <td>{{ship.berthing}} </td>
             </tr>
             <tr>
               <td>Nor Tendered:</td>              
               <td>{{ship.nor_tendered}} </td>
             </tr>

             <tr>
               <td>Time start:</td>              
               <td>{{ship.time_start}} </td>
             </tr>

              <tr>
               <td>Completion:</td>              
               <td>{{ship.completition}} </td>
             </tr>

             
           </tbody>
         </table>
         <h3>DEDUCTIONS/EXCEPTIONS</h3>
         <table class="table table-bordered">
         	<thead>
         		<th>SL</th>
         		<th>Date</th>
         		<th>From</th>
         		<th>To</th>
         		<th>DESCRIPTION</th>
         	</thead>
         	<tbody>
                    <tr ng-repeat="deduction in deductions">
                            <td>{{$index}}</td>
                            <td>{{deduction.date}}</td>
                            <td>{{deduction.from}}</td>
                            <td>{{deduction.to}}</td>
                            <td>{{deduction.description}}</td>
                    </tr>
         	</tbody>
         </table>