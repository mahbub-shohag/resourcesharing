<div id="calculators" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Calculators</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
          <thead>
            <th>Sl#</th>
            <th>Ship Name</th>
            <th>Port</th>
            <th>Activity</th>
            <th>Imo</th>
            <th>Loa</th>
          </thead>
          <tbody>
            <tr ng-repeat="calculator in calculatorsList" ng-click="selectCal(calculator)">
              <td>{{$index+1}}</td>
              <td>{{calculator.shipName}}</td>
              <td>{{calculator.port}}</td>
              <td>{{calculator.activity}}</td>
              <td>{{calculator.imo}}</td>
              <td>{{calculator.loa}}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
      </div>
    </div>

  </div>
</div>