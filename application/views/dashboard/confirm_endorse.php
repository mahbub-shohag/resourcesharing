<?php
$this -> load -> view('_header');
?>
<div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">    
  <span  class="text-center" style="font-size: 32px;float: left;">Confirm endorse</span>
  <span id="msg_success" class="alert alert-success" style="margin-left: 70px;display: none">
   <strong>Success!</strong> Indorsing request has been sent.
  </span>
  <span id="msg_error" class="alert alert-danger" style="margin-left: 70px;display: none">
    <strong>Sorry!</strong> This user is not in the system, so it can not pay for your membership. Please insert other user's email or pay via credit card or paypal;.
  </span>
    <span id="msg_error2" class="alert alert-danger" style="margin-left: 70px;display: none">
    <strong>Sorry!</strong> Endorsement already sent to this user from you.
  </span>
  <!-- <span style="float: left;font-size: 31px;margin-left: 70px;">Endorsement Successful!</span> -->
  <hr/>
  <style type="text/css">
  .funding_source img{
    width:170px;
    height: 60px;
  }
</style>

<div class="form-group funding_source">
  <label class="control-label col-sm-2" for="email">Funding Source:</label>
  <div class="col-sm-10">
   <!--  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
      <input type="hidden" name="cmd" value="_s-xclick">
      <input type="hidden" name="hosted_button_id" value="BUPTKUGL2WHPJ">
      <table>
        <tr><td><input type="hidden" name="on0" value="Options">Options</td></tr><tr><td><select name="os0">
          <option value="Option 1">Option 1 : $0.01 USD - monthly</option>
          <option value="Option 2">Option 2 : $0.02 USD - monthly</option>
          <option value="Option 3">Option 3 : $0.03 USD - monthly</option>
        </select> </td></tr>
      </table>
      <input type="hidden" name="currency_code" value="USD">
      <input type="hidden" name="user_id" value="14">
      <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
      <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </form> -->

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="4L5KNBYCV6JT2">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>




</div>
</div>
</div>
</div>
</section>
<!--CONTACT US AREA END-->

<?php
$this -> load -> view('_footer');
?>

<style type="text/css">
.endorsing_div{
  display: none;
}
</style>
<script type="text/javascript">
  $(document).ready(function () {
            // event.preventDefault();
            $("#endorsing").click(function(){ 
              $("#endorsing_div").css("display", "block");
            });

            $("#paypal,#cc").click(function(){                              
              $("#endorsing_div").css("display", "none");
            });

             //save activity modal data
             $("#add_endorse").click(function(){
              event.preventDefault();
              var endorse_email = $('#endorse_email').val();
              var user_id='<?php echo $_SESSION['user_id'];?>';
                $.ajax({
                url:"<?php echo base_url();?>payment/check_exist",
                method:"POST",
                data:{endorse_email:endorse_email, user_id:user_id},
                dataType:"text",
                success: function(data) {
                  console.log(data);
                  if(data=='exists'){
                    $('#msg_error').hide();
                    $('#msg_success').show().delay(2000).fadeOut();
                  }
                   else if(data=='not exists'){
                    $('#msg_success').hide();
                    $('#msg_error').show().delay(3000).fadeOut();
                  }else if(data=='result2'){
                   $('#msg_success').hide();
                   $('#msg_error').hide();
                    $('#msg_error2').show().delay(3000).fadeOut();
                  }
               },
               error: function(err) {
              }                
          });     

            });




           });
         </script>