<?php
    $this -> load -> view('_header');
	
	
$_SESSION['subscribed'] != 'yes';
    if($_SESSION['subscribed'] != 'yes'){ 
      // echo "Yes";
	   // $this->session->set_flashdata("error", "You are not a subscribed user till now. Please be subscribed first to access our system.");
        redirect("dashboard/payment_option", "refresh");
    }
	
                                        
?>
<div class="col-md-9">


<?php 
  if (isset($_GET['tid'])) {
    echo "  <span id=\"msg_success\" class=\"alert alert-success\" style=\"margin-left: 70px;display: block\">
   <strong>Success!</strong> Your payment has been done. Enjoy your dashboard.
  </span>";
  }
?>  

<div ng-controller="dashboard">
                   <!--Summery Starts-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="dash-box dash-box-color-1">
                                <div class="dash-box-icon">
                                    <i class="glyphicon glyphicon-duplicate"></i>
                                </div>
                                <div class="dash-box-body">
                                    <span class="dash-box-count">{{total_itinary.total_itinary}}</span>
                                    <span class="dash-box-title">Itinarary</span>
                                </div>
                                
                                <div class="dash-box-action">
                                    <button>More Info</button>
                                </div>              
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dash-box dash-box-color-2">
                                <div class="dash-box-icon">
                                    <i class="glyphicon glyphicon-download"></i>
                                </div>
                                <div class="dash-box-body">
                                    <span class="dash-box-count">{{total_docs.total_documents}}</span>
                                    <span class="dash-box-title">Documents</span>
                                </div>
                                
                                <div class="dash-box-action">
                                    <button>More Info</button>
                                </div>              
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="dash-box dash-box-color-3">
                                <div class="dash-box-icon">
                                    <i class="glyphicon glyphicon-book"></i>
                                </div>
                                <div class="dash-box-body">
                                    <span class="dash-box-count">{{total_docs.total_documents}}</span>
                                    <span class="dash-box-title">Stow Plan</span>
                                </div>
                                
                                <div class="dash-box-action">
                                    <button>More Info</button>
                                </div>              
                            </div>
                        </div>
                    </div>    
                    <div class="row" id="selectRecordMessage" style="display: none;">
                        <div class="alert alert-success">
                          <strong>Vessel Record "<span style="color: blue;">{{selectedVessel.vessel_id}}</span>" Selected Successfully!</strong>
                        </div>
                    </div>
                    <div class="row" id="selectRecordMessagePromt">
                        <div class="alert alert-danger">
                          <strong>Please Select a record from the log to have Docs and Itinearary tabs
                          active for your folder,otherwise please create a new record!</strong>
                        </div>
                    </div>
                    <!--Summery Ends-->
                    <div class="row">
                        <div class="col-md-12" style="border:2px solid #a2888814;background: #fff">
                            <h3>Vessel Records</h3>
                                    <button class="btn btn-success" ng-click="addShip()" style="float: right;margin-top: -43px;" data-toggle="modal" data-target="#vesselCreateModal">Add New</button>
                            <table  class="table table-bordered table-hover" style="overflow-y: scroll">
                                <thead>
                                    <th>SL.</th>
                                    <th>Vessel</th>
                                    <th>LOA</th>
                                    <th>Year</th>
                                    <th>Vessel Type </th>
                                    <th>IMO</th>
                                    <th>Activity</th>
                                    <th>Port</th>
                                    <th>Code</th>
                                    <th>Vessel ID</th>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="vessel in vessels" ng-click="selectRecord(vessel)">
                                        <td>{{$index+1}}</td>
                                        <td>{{vessel.vessel}}</td>
                                        <td>{{vessel.loa}}</td>
                                        <td>{{vessel.year}}</td>
                                        <td>{{vessel.vessel_type}}</td>
                                        <td>{{vessel.imo}}</td>
                                        <td>{{vessel.activity}}</td>
                                        <td>{{vessel.port}}</td>
                                        <td>{{vessel.code}}</td>
                                        <td>{{vessel.vessel_id}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 50px;">
                             <!--Starts Ship-->
                            
                            <div class="col-md-5" style="border:2px solid #a2888814;background: #fff">
                                    <h3>Ship List</h3>
                                    <button class="btn btn-success" ng-click="addShip()" style="float: right;margin-top: -43px;" data-toggle="modal" data-target="#shipCreateModal">Add New</button>
                                    <table class="table table-bordered table-hover" 
                                    style="height:340px;overflow-y: scroll;display: block;">
                                        <thead>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Imo</th>
                                            <th>Ship Type</th>
                                            <th></th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <input class="form-control" type="text" ng-model="search.name" placeholder="name.." name="">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" ng-model="search.imo" placeholder="imo..." name="">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" ng-model="search.ship_type" placeholder="type..." name="">
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr ng-repeat="ship in ships | filter:{name:search.name,imo:search.imo,ship_type:search.ship_type}">
                                                <td>{{$index+1}}</td>
                                                <td>{{ship.name}}</td>
                                                <td>{{ship.imo}}</td>
                                                <td>{{ship.ship_type}}</td>
                                                <td><button class="btn btn-info" data-toggle="modal" data-target="#shipUpdateModal"  ng-click="editShip(ship)">Edit</button></td>
                                                <td><button ng-click="delete('ship',ship.id)" class="btn btn-info">Delete</button></td>    
                                            </tr>
                                        </tbody>
                                    </table>   
                                  </div>                 
                                <!--Ends Ship-->

                                <div class="col-md-2">
                                    <table class="table table-bordered table-hover">
                                        
                                    </table>
                                </div>

                                 <!--Starts Port-->
                                 <div class="col-md-5" style="border:2px solid #a2888814;background: #fff">
                                    <h3>Port List</h3>
                                    <button class="btn btn-success" ng-click="addPort()" style="float: right;margin-top: -43px;" data-toggle="modal" data-target="#portCreateModal">Add New</button>
                                    <table class="table table-bordered table-hover" 
                                    style="height:340px;overflow-y: scroll;display: block;">
                                        <thead>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Imo</th>
                                            <th>Ship Type</th>
                                            <th></th>
                                            <th></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <input class="form-control" type="text" ng-model="searchPort.name" name="">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" ng-model="searchPort.call_sign" name="">
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr ng-repeat="port in ports | filter:{name:searchPort.name,call_sign:searchPort.call_sign}">
                                                <td>{{$index+1}}</td>
                                                <td>{{port.name}}</td>
                                                <td>{{port.call_sign}}</td>
                                                <td><button class="btn btn-info" data-toggle="modal" data-target="#portUpdateModal"  ng-click="editPort(port)">Edit</button></td>
                                                <td><button ng-click="deletePort('port',port.id)" class="btn btn-info">Delete</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>                    
                                <!--Ends Port-->
                             </div>
                        </div>    
                            
                        <?php
                      $this-> load -> view('dashboard/modals/shipCreate');
                      $this-> load -> view('dashboard/modals/shipUpdate');
                      $this-> load -> view('dashboard/modals/portCreate');
                      $this-> load -> view('dashboard/modals/portUpdate');
                      $this-> load -> view('dashboard/modals/vesselCreate');
                      $this-> load -> view('dashboard/modals/vesselUpdate');
                    ?>

                            
                    </div><!--controller ends-->
                </div> <!--col-md-9 ends-->
                    
                    

                </div> 
            </div>
        </div>
    </section>
    <!--CONTACT US AREA END-->

</div>



<?php
    //$this-> load -> view('dashboard/modals/shipCreate');
    $this -> load -> view('_footer');
?>