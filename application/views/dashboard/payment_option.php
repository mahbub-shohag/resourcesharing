<?php
//echo $amount; exit;
$this -> load -> view('_header');
?>
<div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">    
  <span  class="text-center" style="font-size: 32px;float: left;">Select payment option</span>
  <hr/>
    <br>
<div class="message" style="display: block;margin-bottom: 50px;"> 

  <span id="msg_success" class="alert alert-success" style="margin-left: 70px;display: none">
   <strong>Success!</strong> Indorsing request has been sent.
  </span>
  <span id="msg_error" class="alert alert-danger" style="margin-left: 70px;display: none">
    <strong>Sorry!</strong> This user is not in the system, so it can not pay for your membership. Please insert other user's email or pay via credit card or paypal.
  </span>
    <span id="msg_error2" class="alert alert-danger" style="margin-left: 70px;display: none">
    <strong>Sorry!</strong> Endorsement already sent to this user from you.
  </span>
</div>


  <!-- <span style="float: left;font-size: 31px;margin-left: 70px;">Endorsement Successful!</span> -->

  <style type="text/css">
  .funding_source img{
    width:170px;
    height: 60px;
  }

  .input-lg {
    margin-top: 12px;
}

button.btn.btn-primary.btn-block.mt-4 {
    margin-bottom: 50px!important;
    margin: 15px 0px;
}

  
</style>
<?php
  if(isset($_SESSION['success'])){?>
    <div class="alert alert-success text-center"><?php echo $_SESSION['success']; ?></div>
 <?php }?>

<?php 
  if (isset($_GET['tid'])) {
    echo "  <span id=\"msg_success\" class=\"alert alert-success\" style=\"margin-left: 70px;display: block\">
   <strong>Success!</strong> Your payment has been done.
  </span>";
  }
?>  


<div class="form-group funding_source">
  <label class="control-label col-sm-2" for="email">Funding Source:</label>
  <div class="col-sm-10">

 <label class="radio-inline"><input type="radio" name="optradio" id="cc" ><img src="<?php echo base_url(); ?>img/cc.png"></label><br><br>

<div class="form-group cc_div" id="cc_div" >


    <h2 class="my-4 text-center"><?php
    if(isset($gold1)){ echo $gold1;}
    elseif(isset($gold2)){ echo $gold2;}
    elseif(isset($diamond)){ echo $diamond;}
    else{ echo "Subscription Fee: $15";}

    ?></h2>
    <form action="../stripe/charge.php" method="post" id="payment-form">
      <div class="form-row">
        <div class="col-md-6">
            <input type="text" name="first_name" class="form-control input-lg mb-3 StripeElement StripeElement--empty" placeholder="First Name">
       </div>
       <div class="col-md-6">
       <input type="text" name="last_name" class="form-control input-lg mb-3 StripeElement StripeElement--empty" placeholder="Last Name">
       </div>
       <div class="col-md-12">
       <input type="email" name="email" class="form-control input-lg mb-3 StripeElement StripeElement--empty" placeholder="Email Address">
     </div>
     <div class="col-md-12">
        <div id="card-element" class="form-control input-lg">
          <!-- a Stripe Element will be inserted here. -->
        </div>

        <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id'] ;?>">
       <?php 
          if(isset($endorser)){
           echo  "<input type=\"hidden\" name=\"endorser\" value=\"".$endorser."\">";
          }?>
        <input type="hidden" name="plan" value="<?php
         //stripe plans
        $gold='plan_DlVdmGUnj59KiC';
        $diamond='plan_Do6A3OiDCL4LKS';
        if(isset($gold1)){ echo $gold;}
        elseif(isset($gold2)){ echo $gold;}
        elseif(isset($diamond)){ echo $diamond;}
        ?>">
        <input type="hidden" name="subscription_fee" value="<?php 
        if(isset($gold1)){ echo '$15';}
        elseif(isset($gold2)){ echo '$15';}
        elseif(isset($diamond)){ echo '$30';}
        ?>">
      </div>

        <!-- Used to display form errors -->
        <div id="card-errors" role="alert"></div>
      </div>
     <div class="col-md-12">
      <button>Submit Payment</button>
     </div>
    </form>


<!-- 
  <form action="test.php" method="post">
   <div class="col-md-12">
    <label class="control-label col-md-12" for="email">Name on the card:</label>
    <input type="email" name="" id="endorse_email" class="form-control input-lg" >
  </div>

  <div class="col-md-6">
    <label class="control-label col-md-6" for="email" style="margin-top: 15px;">Card number:</label>
    <input type="email" name="" id="endorse_email" class="form-control input-lg" >
  </div>
   <div class="col-md-6">
    <label class="control-label col-md-6" for="email" style="margin-top: 15px;">Security code (CVV):</label>
    <input type="email" name="" id="endorse_email" class="form-control input-lg" >
  </div>

  <div class="col-md-12">
    <label class="control-label" for="email" style="margin-top: 15px;">Expiration Date:</label>
    <input type="month" id="start" class="form-control input-lg"  name="start"
           min="2018-03" value="2018-05" style="margin-bottom: 10px" />
  </div>

  <div class="col-md-12">
   <button class="btn btn-primary btn-block" type="submit" name="cc_submit" style="margin-bottom: 50px"> Submit</button>
  </div> 
</form>  -->

</div>

<label class="radio-inline">   
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
  <input type="hidden" name="cmd" value="_s-xclick">
  <input type="hidden" name="hosted_button_id" value="4L5KNBYCV6JT2">
<!--   <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"> -->
  <input type="radio">  <input type="image" style="height: 60px;width: 170px;" src="<?php echo base_url(); ?>img/paypal.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
  <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
  </form>
</label><br><br>


<!-- <label class="radio-inline"><input type="radio" name="optradio" id="paypal" checked><img src="<?php echo base_url(); ?>img/paypal.png"></label><br><br>

<div class="form-group paypal_div" id="paypal_div">
  
  <div class="col-md-12">
    <label class="control-label col-md-12" for="email">Email:</label>
   <input type="email" name="paypal_email" id="paypal_email" placeholder="Paypal Email" class="form-control input-lg" >
 </div> 
</div> -->




<label class="radio-inline"><input type="radio" name="optradio" id="endorsing" ><b>Endorsing</b> </label>
</div>
</div>

<div class="form-group endorsing_div" id="endorsing_div">
  <label class="control-label col-sm-2" for="email">User email:</label>
  <div class="col-sm-10">

    <p>We are happy to see our members expand!. Please enter here the email of the person who will pay for your membership:</p>
   <input type="email" name="endorse_email" id="endorse_email" placeholder="Enter here email here" class="form-control input-lg" >
 </div> 
</div>
<br>

<div class="form-group"> 
  <div class="col-sm-offset-2 col-sm-10">
    <br> <button type="button" name="add_endorse" id="add_endorse" class="btn btn-default">Add</button>
    <button type="submit" class="btn btn-default">Cancel</button>
  </div>
  <?php
  
    ?>
</div>

</div>
</div>
</div>
</div>
</section>
<!--CONTACT US AREA END-->

<?php
$this -> load -> view('_footer');
?>

<script src="https://js.stripe.com/v3/"></script>
<script src="<?php echo base_url();?>stripe/js/charge.js"></script>

<style type="text/css">
.cc_div{
  display: none;
}.paypal_div{
  display: none;
}.endorsing_div{
  display: none;
}
</style>
<script type="text/javascript">
  $(document).ready(function () {
            // event.preventDefault();
           
           
            $("#cc").click(function(){       
              $("#cc_div").css("display", "block");                       
              $("#paypal_div").css("display", "none");
              $("#endorsing_div").css("display", "none");
            });

            $("#paypal").click(function(){    
              $("#paypal_div").css("display", "block");                          
              $("#cc_div").css("display", "none");
              $("#endorsing_div").css("display", "none");
            });

             $("#endorsing").click(function(){ 
              $("#endorsing_div").css("display", "block");
              $("#paypal_div").css("display", "none");
              $("#cc_div").css("display", "none");
            });         

            
             //save activity modal data
             $("#add_endorse").click(function(){
              event.preventDefault();
              var endorse_email = $('#endorse_email').val();
              var user_id='<?php echo $_SESSION['user_id'];?>';
                $.ajax({
                url:"<?php echo base_url();?>payment/check_exist",
                method:"POST",
                data:{endorse_email:endorse_email, user_id:user_id},
                dataType:"text",
                success: function(data) {
                  console.log(data);
                  if(data=='exists'){
                    $('#msg_error').hide();
                    $('#msg_success').show().delay(2000).fadeOut();
                  }
                   else if(data=='not exists'){
                    $('#msg_success').hide();
                    $('#msg_error').show().delay(3000).fadeOut();
                  }else if(data=='result2'){
                   $('#msg_success').hide();
                   $('#msg_error').hide();
                    $('#msg_error2').show().delay(3000).fadeOut();
                  }
               },
               error: function(err) {
              }                
          });     

            });




           });
         </script>

