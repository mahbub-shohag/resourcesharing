
<script type="text/javascript">

		// $("#datetimepicker").datetimepicker();



	$(document).ready(function () {

	
		// auto search (ship)
		$('#ship_name').autocomplete({
			source: "<?php echo site_url('calculator/ship_search/?'); ?>"
		});
		// end auto search (ship)

		
		// auto fill next 3 fields
		$('#ship_name').change(function(){
			var ship_name = $(this).val();

			$.ajax({
				url:"<?php echo base_url();?>calculator/ship_details",
					method:"POST",
					data:{ship_name:ship_name},
					success:function(data)
					{
						var jsonData = JSON.parse(data)[0];						
						$('#dwt').val(jsonData.dwt);
						$('#loa').val(jsonData.loa);
						$('#imo').val(jsonData.imo);

					}
			});
			
		});


		// change rate based on selection and modal
		$('#activity').change(function(){
			var activity = $(this).val();
			
			if (activity=='add_more_activity') {
				$('#addMoreActivity').modal({
				    show: 'false'
				}); 
				$('#activity_response').html('');
			}
			else{
				$('#activity_response').html(activity);
			}

		});


		//save activity modal data
		 $("#saveActivity").click(function(){
			
            var dropdown_text = $('#more_activity').val();
            var dropdown_for= 'activity';
            var user_id='<?php echo $_SESSION['user_id'];?>';

	        $.ajax({
					url:"<?php echo base_url();?>calculator/save_activity",
					method:"POST",
					data:{dropdown_text:dropdown_text, dropdown_for:dropdown_for,created_by_user_id:user_id  },
					dataType:"text",
					success:function(data)
					{
						$('#more_activity_response').html(data);

						$('#addMoreActivity').modal('toggle'); //closing the modal
						
					    // apend newly inserted option
					    $("#activity").append("<option value="+dropdown_text+" selected >"+ dropdown_text +"</option>");
					
					}
				});

	   		 });


		 // cargo popup
		$('#cargo').change(function(){
			var cargo = $(this).val();
			
			if (cargo=='add_more_cargo') {
				$('#addMoreCargo').modal({
				    show: 'false'
				}); 
			}

		});


		//save cargo modal data
		 $("#saveCargo").click(function(){
			
            var dropdown_text = $('#more_cargo').val();
            var dropdown_for= 'cargo';
            var user_id='<?php echo $_SESSION['user_id'];?>';

	        $.ajax({
					url:"<?php echo base_url();?>calculator/save_cargo",
					method:"POST",
					data:{dropdown_text:dropdown_text, dropdown_for:dropdown_for,created_by_user_id:user_id  },
					dataType:"text",
					success:function(data)
					{
						$('#more_cargo_response').html(data);

						$('#addMoreCargo').modal('toggle'); //closing the modal
						
					    // apend newly inserted option
					    $("#cargo").append("<option value="+dropdown_text+" selected >"+ dropdown_text +"</option>");
					
					}
				});

	   		 });


		  // description popup
		$('#description').change(function(){
			var description = $(this).val();
			
			if (description=='add_more_description') {
				$('#addMoreDescription').modal({
				    show: 'false'
				}); 
			}

		});


		//save description modal data
		 $("#saveDescription").click(function(){
			
            var dropdown_text = $('#more_description').val();
            var dropdown_for= 'description';
            var user_id='<?php echo $_SESSION['user_id'];?>';

	        $.ajax({
					url:"<?php echo base_url();?>calculator/save_description",
					method:"POST",
					data:{dropdown_text:dropdown_text, dropdown_for:dropdown_for,created_by_user_id:user_id  },
					dataType:"text",
					success:function(data)
					{
						$('#more_description_response').html(data);

						$('#addMoreDescription').modal('toggle'); //closing the modal
						
					    // apend newly inserted option
					    $("#description").append("<option value="+dropdown_text+" selected >"+ dropdown_text +"</option>");
					
					}
				});

	   		 });


		 //add new row with addNewRowBtn

		  $(document).ready(function() {

			      $("#addNewRowBtn").click(function(){

			        $("#newTr").clone().appendTo("#responseTR");

			       
			      // this.id="newid";

			      });
			   
			});


		  
	
		  $("#deleteNewRowBtn").click(function(){
		  	
		        $('#newTr').last().remove();

		    });







		


		
	});
</script>