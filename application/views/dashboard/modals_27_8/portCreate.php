<!-- Modal -->
  <div class="modal fade" id="portCreateModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background:#094a6a">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Port</h4>
        </div>
        <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" ng-model="port.name" class="form-control">
              </div>
              <div class="form-group">
                <label for="call_sign">Call Sign:</label>
                <input type="number" ng-model="port.call_sign" class="form-control">
              </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="submit" ng-click="savePort()" data-dismiss="modal" class="btn btn-default">Save</button>
        </div>
      </div>
      
    </div>
  </div>