<!-- Modal -->
  <div class="modal fade" id="shipCreateModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="background:#094a6a">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Ship</h4>
        </div>
        <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" ng-model="ship.name" class="form-control">
              </div>
              <div class="form-group">
                <label for="imo">IMO:</label>
                <input type="number" ng-model="ship.imo" class="form-control">
              </div>
              <div class="form-group">
                <label for="name">MMSI:</label>
                <input type="text" ng-model="ship.mmsi" class="form-control">
              </div>
              <div class="form-group">
                <label for="imo">Ship Type:</label>
                <input type="text" ng-model="ship.ship_type" class="form-control">
              </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="submit" ng-click="saveShip()" data-dismiss="modal" class="btn btn-default">Save</button>
        </div>
      </div>
      
    </div>
  </div>