<?php
    $this -> load -> view('_header');
?>
<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
	<h2 class="text-center">Calculator</h2>
         <div class="calculator">
         	    <form action="" method="post" >
		            <div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="Name" class="control-label">NAME</label>
								<!-- <input type="text" ng-model="ship.ship_name" name="ship_name" class="form-control " id="ship_name" placeholder="Name" required> -->
								<ui-select style="background: lightgray;margin-left: 8px;height: 30px;" ng-change="selectShipName()" ng-model="ship.ship_name" theme="selectize" ng-disabled="disabled" title="Choose a District">
	                                  <ui-select-match>{{$select.selected.name}}</ui-select-match>
	                                  <ui-select-choices repeat="aship in ships | filter: {name:$select.search}">
	                                    <span ng-bind-html="aship.name | highlight: $select.search"></span>
	                                  </ui-select-choices>
                                </ui-select>
							 </div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
							<label for="dwt" class="control-label">DWT</label>
							 <input type="text" ng-model="ship.dwt" class="form-control " id="dwt" name="dwt" placeholder="DWT" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">						               
			               <div class="form-group">
			               	 <label for="loa" class="control-label">LOA</label><br>	
			                <input type="text" ng-model="ship.loa" name="loa" class="form-control " id="loa" placeholder="LOA" required>
			              </div>
			            </div>
			            <div class="col-md-6">  
			              <div class="form-group">
			              	 <label for="imo" class="control-label">IMO</label><br>	
			                <input type="text" ng-model="ship.imo" name="imo" class="form-control " id="imo" placeholder="IMO" required>
			              </div>
		            	</div>
		            </div>

		            <hr/>
		             
		              <div class="row">

		              	<div class="col-md-6">
							<div class="form-group">			                
				               <label for="port" class="control-label"><span ng-click="makeFile()">PORT</span></label>
				               <ui-select style="background: lightgray;margin-left: 8px;height: 30px;" ng-model="ship.port" theme="selectize" ng-disabled="disabled" title="Choose a District">
	                                  <ui-select-match>{{$select.selected.destination}}</ui-select-match>
	                                  <ui-select-choices repeat="adestination in destinations | filter: {name:$select.search}">
	                                    <span ng-bind-html="adestination.destination | highlight: $select.search"></span>
	                                  </ui-select-choices>
                                </ui-select>

				              </div>
		              	</div>
			              
		              	<div class="col-md-5">

			               <div class="form-group">	
			               	<label for="activity" class="control-label">ACTIVITY</label>
			                <select class="form-control" ng-change="findactivity()" ng-model="ship.activity" id="activity" name="" >
							  <option value="">Select an Activity Item</option>
							  <option ng-repeat="aactivity in activities track by $index" value="{{aactivity.dropdown_text}}">
							  	{{aactivity.dropdown_text}}
							  </option>
							</select>
			              </div>
						</div>
						<div class="1">
							<label></label>
							<label ng-click="addNewDropdown('activity')" data-toggle="modal" data-target="#activityModal" style="margin-top: 32px;margin-right: 33px;background: greenyellow;font-size: 45px;color: white;">+</label>
						</div>

						 <!-- activity modal -->	
						 <div class="modal fade" id="activityModal" role="dialog">
						    <div class="modal-dialog modal-lg">
						      <div class="modal-content">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">Enter custom activity</h4>
						        </div>
						        <div class="modal-body">
						          <div class="modal-body">
						              <input type="text" ng-model="more_activity" name="more_activity" class="form-control " id="more_activity" placeholder="Enter a value">
						          </div>
						        </div>
						        <div class="modal-footer">
						          <button type="button" class="btn btn-default" ng-click="saveActivity(more_activity)" data-dismiss="modal">Save</button>
						        </div>
						      </div>
						    </div>
						  </div>	


						


					</div>


					<div class="row">
						<div class="col-md-6">
			                <div class="form-group">
			              		<label for="quantity" class="control-label">QUANTITY</label>
				                <input type="text" ng-model="ship.quantity" name="quantity" class="form-control" id="quantity" placeholder="1000.00" required>
				             </div>
			            </div>
			            <div class="col-md-5">
			               <div class="form-group">
			               		<label for="cargo" class="control-label">CARGO</label>	
				                <select class="form-control" ng-change="find('cargo')" ng-model="ship.cargo"  name="" >
							  <option value="">Select a Cargo Item</option>
							  <option ng-repeat="cargo in cargoes track by $index" value="{{cargo.dropdown_text}}">
							  	{{cargo.dropdown_text}}
							  </option>
							</select>
			              </div>

			              <!-- Modal activity -->
						<div id="addCargo" class="modal fade" role="dialog">
						  <div class="modal-dialog">

						    <!-- Modal content-->

						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title">Enter custom cargo</h4>
						      </div>
						      <div class="modal-body">
						        <input type="text" ng-model="newCargo" name="more_cargo" class="form-control " id="more_cargo" placeholder="Enter a value">
						      </div>

						      <div class="modal-footer">

						      	<input type="hidden" name="dropdown_for" id="dropdown_for">
						        <button type="button" ng-click="save_dropdown(newCargo)" class="btn btn-default" id="">Save</button>
						      </div>
						    </div>

						  </div>
						</div>

			            </div>

			            <div class="1">
							<label></label>
							<label ng-click="addNewDropdown('cargo')" data-toggle="modal" data-target="#addCargo" style="margin-top: 32px;margin-right: 33px;background: greenyellow;font-size: 45px;color: white;">+</label>
						</div>

		             </div>
		             <hr/>

		             
		             <div class="row">
					    <div class="col-md-6">
					    	<label for="rate">Rate of <span id="activity_response">{{ship.rate}}</span></label>
					    	<div class="input-group input-grouplg">
					    
					      		<input type="text" ng-model="ship.rate" class="form-control"  id="rate" placeholder="1000.00">
					      		<span class="input-group-addon"></span>
					      	
						        <select  class="form-control" ng-model="ship.rateUnit" name="rateUnit" >
						          <option value="" selected="selected">Choose Unit</option>
						          <option value="MT/DAY">MT/DAY</option>
						          <option value="BARRELS/DAY">BARRELS/DAY</option>
						          <option value="DAYS">DAYS</option>
						          <option value="HOURS">HOURS</option>
						        </select>	
					  		</div>
					    </div>
					
					    <div class="col-md-6">
					    	<label for="demurrageRate">Demurrage Rate</label>
					    	<div class="input-group input-grouplg">
					    
					      		<input type="text" ng-model="ship.demurrageRate" class="form-control" id="demurrageRate" placeholder="1000.00">
					      		<span class="input-group-addon"></span>
					      	
						        <select  class="form-control" name="demurrageRateUnit" ng-model="ship.demurrageRateUnit">
						          <option value="" selected="selected">Currency/Time</option>
						          <option value="USD/DAY">USD/DAY</option>
						          <option value="USD/HOUR">USD/HOUR</option>
						          <option value="EUR/DAY">EUR/DAY</option>
						          <option value="EUR/HOUR">EUR/HOUR</option>
						        </select>	
					  		</div>
					    	
					    </div>
					</div>
					<div class="row">
						<div class="col-md-6">
						<label for="despatchRate">Despatch Rate</label>
					    	<div class="input-group input-grouplg">
					      		<input type="text" ng-model="ship.despatchRate" class="form-control" id="despatchRate" placeholder="1000.00">
					      		<span class="input-group-addon"></span>
						        <select  class="form-control" name="despatchRateUnit" ng-model="ship.despatchRateUnit">
						          <option value="" selected="selected">Currency/Time</option>
						          <option value="USD/DAY">USD/DAY</option>
						          <option value="USD/HOUR">USD/HOUR</option>
						          <option value="EUR/DAY">EUR/DAY</option>
						          <option value="EUR/HOUR">EUR/HOUR</option>
						        </select>	
					  		</div>
					  	</div>
					</div>

					  <hr>

					  <h4 class="sub_ttl">PORT TIMES</h4>

					  <br>

					 <div class="row">
						<div class="col-md-6">
			                <div class="form-group">
			              		<label for="quantity" class="control-label">ARRIVAL</label>
				                <input type="datetime-local" ng-model="ship.arrival" name="arrival" class="form-control" id="arrival" placeholder="Arrival" required>
				             </div>
			            </div>
			            <div class="col-md-6">
			               <div class="form-group">
			              		<label for="quantity" class="control-label">BERTHING</label>
				                <input type="datetime-local" ng-model="ship.berthing" name="berthing" class="form-control" id="berthing" placeholder="Berthing" required>
				             </div>
			            </div>
		             </div>


		             <div class="row">
						<div class="col-md-6">
			                <div class="form-group">
			              		<label for="quantity" class="control-label">NOR TENDERED</label>
				                <input type="datetime-local" ng-model="ship.nor_tendered" name="nor_tendered" class="form-control" id="nor_tendered" placeholder="Nor tendered" required>
				             </div>
			            </div>
			            <div class="col-md-6">
			               <div class="form-group">
			              		<label for="quantity" class="control-label">TIME START</label>
				                <input type="datetime-local" ng-model="ship.time_start" name="time_start" class="form-control" id="time_start" placeholder="Time start" required>
				             </div>
			            </div>
		             </div>

		             <div class="row">
						<div class="col-md-6">
			                <div class="form-group">
			              		<label for="quantity" class="control-label" >COMPLETITION</label>
				                <input type="datetime-local" ng-model="ship.completition" name="completition" class="form-control" id="completition" placeholder="Completition" required>
				             </div>
			            </div>
			           
		             </div>




<h4 class="sub_ttl">DEDUCTIONS/EXCEPTIONS</h4><hr>

   <table class="table"><tbody>
	      <tr id="newTr" ng-repeat="deduction in deductions">
	        <td>
	        	<label for="quantity" class="control-label">Date</label>
                 <input type="date" ng-model="deduction.date"  class="form-control" id="" name="" value="2018-07-22" min="2018-01-01" max="2018-12-31" />
	           
	         </td>
	        <td>
	        	<label for="quantity" class="control-label">FROM</label>
		            <input type="time" ng-model="deduction.from"  class="form-control" id="appt-time" name="appt-time" min="9:00" max="18:00" />	
		    </td>
	        <td>
	         	<label for="quantity" class="control-label">TO</label>
	            <input type="time" ng-model="deduction.to"  class="form-control" id="appt-time" name="appt-time" min="9:00" max="18:00" />				            
		    </td>

		     <td>
	         	<div class="form-group">
               		<label for="description" class="control-label">DESCRIPTION</label>	
	                <select class="form-control" ng-change="addNewDropdown('activity')" ng-model="deduction.description" id="description" name="" >
							  <option value="">Select a description</option>
							  <option ng-repeat="deduction_description in deduction_descriptions track by $index" value="{{deduction_description.value}}">
							  	{{deduction_description.name}}
							  </option>
					</select>
              	</div>
              	 

		    </td>

		     <td>
	         	
              		<label for="quantity" class="control-label">%</label>

	                <input type="text" id="deduction" ng-model="percentage" class="form-control" placeholder=" % ">
	             
		    </td>
			<td>
	         	 
              	   <label for="quantity" class="control-label">TIME LOST</label>
	               <input type="time" ng-model="deduction.appt-time"  class="form-control" id="appt-time" name="appt-time"
				               min="9:00" max="18:00" />
		    </td>
		    <td>
		    	<br>
		    	<div id="addbt" ng-click="addObject(deduction,'deductions')"><span  class="glyphicon glyphicon-plus"></span></div>
		    	<div id="deleteNewRowBtn"><span ng-click="deleteObject(deduction,'deductions')" class="glyphicon glyphicon-trash"></span></div>
		    </td>


		  </tr>

		  <!-- <div id="responseTR"></div> -->
	     
	    </tbody>

	  </table>

				    

     <!-- <button type="button" ng-click="addObject(1,'deductions')" id="" class="btn btn-default "><b>Add New Row</b></button> -->
						 


		           
		              
		             <div class="row">
						<div class="col-md-12">
			                <div class="form-group">
			              		<label for="quantity" class="control-label">COMPLETION</label>

				                <input type="text" id="completion" class="form-control" placeholder="Completion">
				             </div>
			            </div>						            
		             </div>

					<style type="text/css">
						td, th {
								    padding: 0;
								    padding-right: 7px;
								}
								#addNewRowBtn{
									margin-bottom: 25px;
									margin-top: -20px;
								}

								.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
											    padding: 4px!important;
											    line-height: 1.42857143;
											    vertical-align: top;
											    border-top: 1px solid #ddd;
											}
					</style>
 
		             
		             <!-- <br><br> <button type="submit" class="btn btn-default" onclick="return check_form_validity();" name="signup">Sign Up</button> -->
		        </form>
         </div>

                 
                    </div>

                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12"> 
                    		<div class="report_area1">
       
        <h3>Name of the vehicle: </h3>
        <hr>        
        <table class="table table-bordered">
         
           <tbody>
             <tr>
               <td>Name:</td>             
               <td>{{ship.ship_name.name}} </td>
             </tr>
             <tr>
               <td>Dwt:</td>              
               <td>{{ship.dwt}} </td>
             </tr>

             <tr>
               <td>Loa:</td>              
               <td>{{ship.loa}} </td>
             </tr>

             <tr>
               <td>Imo:</td>              
               <td> {{ship.imo}}</td>
             </tr>

             <tr>
               <td>Port:</td>              
               <td>{{ship.port.destination}} </td>
             </tr>

             <tr>
               <td>Activity:</td>              
               <td>{{ship.activity}} </td>
             </tr>

             <tr>
               <td>Quantity:</td>              
               <td> {{ship.quantity}}</td>
             </tr>

             <tr>
               <td>Cargo:</td>              
               <td>{{ship.cargo}} </td>
             </tr>
         
             <tr>
               <td>Rate of :</td>              
               <td>{{ship.rate}} </td>
             </tr>

             <tr>
               <td>Demurrage Rate:</td>              
               <td>{{ship.demurrageRate}} </td>
             </tr>

             <tr>
               <td>Despatch Rate:</td>              
               <td> {{ship.despatchRate}}</td>
             </tr>
         </tbody>
       </table>
       <table class="table table-bordered">
        <tbody>
             <h3>Port Times:</h3>

              <tr>
               <td>Arrival:</td>              
               <td>{{ship.arrival}} </td>
             </tr>
             <tr>
               <td>Berthing:</td>              
               <td>{{ship.berthing}} </td>
             </tr>
             <tr>
               <td>Nor Tendered:</td>              
               <td>{{ship.nor_tendered}} </td>
             </tr>

             <tr>
               <td>Time start:</td>              
               <td>{{ship.time_start}} </td>
             </tr>

              <tr>
               <td>Completion:</td>              
               <td>{{ship.completition}} </td>
             </tr>

             
           </tbody>
         </table>
         <h3>DEDUCTIONS/EXCEPTIONS</h3>
         <table class="table table-bordered">
         	<thead>
         		<th>SL</th>
         		<th>Date</th>
         		<th>From</th>
         		<th>To</th>
         		<th>DESCRIPTION</th>
         	</thead>
         	<tbody>
         		<tr ng-repeat="deduction in deductions">
         			<td>{{$index}}</td>
         			<td>{{deduction.date}}</td>
         			<td>{{deduction.from}}</td>
         			<td>{{deduction.to}}</td>
         			<td>{{deduction.description}}</td>
         		</tr>
         	</tbody>
         </table>

         <div class="footer-btn" style="float: right;margin-bottom: 20px;">
         <button type="button" ng-click="saveShipInfo()" class="btn btn-secondary">save</button>
          <a target="_blank" href="/chanul/calculator/infopdf/{{shipinfoId}}">
          		<button type="button" class="btn btn-secondary">Print</button>
          </a>
          <button type="button" class="btn btn-secondary">Download</button>
         </div>

       
       
      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Modal activity -->
					<div id="addMoreDescription" class="modal fade" role="dialog">
					  <div class="modal-dialog">

					    <!-- Modal content-->

					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Enter custom description</h4>
					      </div>
					      <div class="modal-body">
					        <input type="text" name="more_description" class="form-control " id="more_description" placeholder="Enter a value">
					      </div>

					      <div class="modal-footer">

					      	<input type="hidden" name="dropdown_for" id="dropdown_for">
					        <button type="button" class="btn btn-default" id="saveDescription">Save</button>
					      </div>
					    </div>

					  </div>
					</div>
    <!--CONTACT US AREA END-->

<?php
	// $this->load->view('dashboard/activity_modal');
	$this -> load -> view('_footer');
?>

  
<?php include('js.php');?>