<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Landing Pages List</title> 

	<?php 
		$this -> load -> view('common_css_js');
	?> 

	
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-4 main-div">
			<div class="link-div">

				<a href="<?php echo base_url('LandingPage/first') ?>" target="_blank">
					<button class="btn btn-block btn-primary">First Landing Page</button>
				</a> <br/>
				
				<a href="<?php echo base_url('LandingPage/second') ?>" target="_blank">
					<button class="btn btn-block btn-primary">Second Landing Page</button>
				</a> <br/>

				<a href="<?php echo base_url('LandingPage/third') ?>" target="_blank">
					<button class="btn btn-block btn-primary">Third Landing Page</button>
				</a>
			</div>
		</div>
		<div class="col-md-4">
		</div>
	</div>
</div>

</body>
</html>