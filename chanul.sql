-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2018 at 05:47 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chanul`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_ip`
--

CREATE TABLE `access_ip` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `access_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_ip`
--

INSERT INTO `access_ip` (`id`, `user_id`, `ip`, `access_time`) VALUES
(1, 32, '192.168.0.101', '2018-08-16 12:27:24'),
(2, 32, '192.168.0.101', '2018-08-17 09:36:21'),
(3, 32, '192.168.0.101', '2018-08-17 09:43:08'),
(4, 32, '192.168.0.101', '2018-08-19 09:08:11'),
(5, 32, '192.168.0.101', '2018-08-19 09:08:48'),
(6, 32, '192.168.0.101', '2018-08-19 10:41:38'),
(7, 32, '::1', '2018-08-25 06:47:04'),
(8, 32, '::1', '2018-08-25 06:50:56'),
(9, 32, '::1', '2018-08-25 07:07:59'),
(10, 32, '::1', '2018-08-25 07:10:01'),
(11, 32, '::1', '2018-08-25 09:38:51'),
(12, 32, '::1', '2018-08-26 05:20:51'),
(13, 72, '103.237.36.230', '2018-08-26 21:40:43'),
(14, 72, '103.237.36.230', '2018-08-26 21:42:45'),
(15, 72, '103.237.36.230', '2018-08-26 21:49:43'),
(16, 73, '103.60.160.29', '2018-08-27 03:34:21'),
(17, 32, '45.248.146.194', '2018-08-27 06:48:49'),
(18, 73, '103.60.160.25', '2018-08-27 08:16:48'),
(19, 73, '103.60.160.25', '2018-08-27 08:56:48'),
(20, 74, '45.248.146.194', '2018-08-27 09:02:30'),
(21, 74, '45.248.146.194', '2018-08-27 09:16:29'),
(22, 74, '45.248.146.194', '2018-08-27 09:17:11'),
(23, 74, '45.248.146.194', '2018-08-27 09:17:22'),
(24, 74, '45.248.146.194', '2018-08-27 09:19:54'),
(25, 74, '45.248.146.194', '2018-08-27 09:28:50'),
(26, 74, '45.248.146.194', '2018-08-27 09:33:24'),
(27, 74, '45.248.146.194', '2018-08-27 09:34:34'),
(28, 32, '103.60.160.25', '2018-08-27 11:51:34'),
(29, 32, '157.119.236.6', '2018-08-28 11:53:32'),
(30, 32, '103.237.36.230', '2018-08-28 13:45:55'),
(31, 32, '103.237.36.230', '2018-08-28 13:47:09'),
(32, 32, '103.237.36.230', '2018-08-28 19:00:55'),
(33, 32, '103.237.36.230', '2018-08-28 19:01:50'),
(34, 32, '103.237.36.230', '2018-08-28 19:09:31'),
(35, 32, '103.60.160.24', '2018-09-02 05:41:13'),
(36, 32, '103.60.160.24', '2018-09-02 05:42:59'),
(37, 32, '123.108.246.128', '2018-09-02 06:51:21'),
(38, 74, '45.248.146.194', '2018-09-02 13:00:05'),
(39, 32, '103.60.160.30', '2018-09-03 18:26:49'),
(40, 32, '103.60.160.30', '2018-09-03 18:29:14'),
(41, 32, '103.60.160.30', '2018-09-03 18:33:49'),
(42, 32, '103.60.160.30', '2018-09-03 19:13:16'),
(43, 32, '103.60.160.31', '2018-09-04 18:29:34'),
(44, 74, '45.248.146.194', '2018-09-06 06:55:04'),
(45, 32, '45.248.146.245', '2018-09-07 03:11:07'),
(46, 32, '67.191.115.185', '2018-09-07 06:59:21'),
(47, 74, '45.248.146.194', '2018-09-07 10:31:36'),
(48, 32, '162.252.126.45', '2018-09-07 19:56:45'),
(49, 32, '67.191.115.185', '2018-09-10 04:53:49'),
(50, 32, '45.248.146.194', '2018-09-10 13:46:55'),
(51, 75, '162.252.126.45', '2018-09-11 18:48:22'),
(52, 32, '45.248.146.194', '2018-09-14 05:19:38'),
(53, 32, '45.248.146.194', '2018-09-14 15:06:18'),
(54, 32, '45.248.146.194', '2018-09-14 15:07:10'),
(55, 32, '45.248.146.194', '2018-09-17 14:41:21'),
(56, 32, '103.60.160.24', '2018-09-23 01:30:33'),
(57, 32, '103.60.160.24', '2018-09-23 01:32:10'),
(58, 32, '103.60.160.24', '2018-09-25 02:22:32'),
(59, 32, '45.120.98.76', '2018-09-26 02:41:46'),
(60, 32, '45.248.146.194', '2018-09-26 03:38:26'),
(61, 32, '45.248.146.194', '2018-09-26 03:39:37'),
(62, 32, '67.191.115.185', '2018-09-27 01:34:20'),
(63, 32, '67.191.115.185', '2018-09-27 02:11:03'),
(64, 32, '67.191.115.185', '2018-09-27 04:09:33'),
(65, 32, '202.51.191.218', '2018-09-27 04:21:05'),
(66, 32, '45.248.146.194', '2018-09-27 04:31:20'),
(67, 81, '67.191.115.185', '2018-09-27 05:36:22'),
(68, 32, '45.248.146.194', '2018-09-27 05:39:47'),
(69, 82, '67.191.115.185', '2018-09-27 05:45:00'),
(70, 82, '67.191.115.185', '2018-09-27 05:58:11'),
(71, 32, '45.248.146.194', '2018-09-28 04:34:04'),
(72, 82, '67.191.115.185', '2018-09-28 04:45:18'),
(73, 32, '45.248.146.194', '2018-09-28 05:35:36'),
(74, 32, '45.248.146.194', '2018-09-28 06:05:29'),
(75, 32, '45.248.146.245', '2018-09-28 13:39:29'),
(76, 32, '45.248.146.194', '2018-09-28 13:51:02'),
(77, 32, '45.248.146.194', '2018-09-28 13:59:57'),
(78, 32, '45.248.146.194', '2018-09-28 14:07:22'),
(79, 32, '45.248.146.194', '2018-09-28 14:10:52'),
(80, 32, '45.248.146.194', '2018-09-28 14:15:29'),
(81, 32, '45.248.146.245', '2018-09-28 23:59:15'),
(82, 32, '45.248.146.194', '2018-09-29 04:16:47'),
(83, 32, '45.248.146.194', '2018-09-29 06:55:52'),
(84, 32, '45.248.146.194', '2018-09-29 08:03:24'),
(85, 32, '45.248.146.194', '2018-09-29 09:07:38'),
(86, 32, '45.248.146.194', '2018-09-29 09:20:23'),
(87, 32, '45.248.146.194', '2018-09-30 05:02:59'),
(88, 32, '45.248.146.194', '2018-09-30 06:49:41'),
(89, 32, '45.248.146.194', '2018-09-30 07:02:11'),
(90, 84, '73.179.72.71', '2018-10-01 05:22:01'),
(91, 84, '73.179.72.71', '2018-10-01 05:43:42'),
(92, 32, '103.60.160.27', '2018-10-02 01:43:56'),
(93, 32, '103.60.160.27', '2018-10-02 01:50:44'),
(94, 32, '103.60.160.27', '2018-10-02 02:09:08'),
(95, 32, '45.248.146.194', '2018-10-03 05:00:03'),
(96, 32, '45.248.146.194', '2018-10-03 06:01:40'),
(97, 32, '45.248.146.194', '2018-10-05 09:00:26'),
(98, 32, '45.248.146.194', '2018-10-05 09:12:22'),
(99, 32, '45.248.146.194', '2018-10-05 10:00:49'),
(100, 32, '45.248.146.194', '2018-10-05 10:41:10'),
(101, 32, '45.248.146.194', '2018-10-05 10:46:20'),
(102, 32, '45.248.146.194', '2018-10-05 10:53:44'),
(103, 32, '45.248.146.194', '2018-10-05 12:16:03'),
(104, 32, '103.60.160.152', '2018-10-06 02:14:50'),
(105, 32, '45.248.146.194', '2018-10-07 08:36:28'),
(106, 32, '45.248.146.194', '2018-10-07 12:53:06'),
(107, 32, '45.248.146.194', '2018-10-07 12:59:18'),
(108, 32, '45.248.146.194', '2018-10-08 11:46:50'),
(109, 32, '45.248.144.130', '2018-10-08 17:25:15'),
(110, 32, '103.200.39.11', '2018-10-08 17:28:00'),
(111, 32, '103.60.160.152', '2018-10-09 02:12:42'),
(112, 32, '103.60.160.152', '2018-10-09 17:24:32'),
(113, 32, '103.60.160.155', '2018-10-10 01:59:42'),
(114, 32, '45.248.146.194', '2018-10-10 05:15:26'),
(115, 32, '45.248.146.194', '2018-10-10 06:23:25'),
(116, 32, '45.248.146.194', '2018-10-10 12:40:34'),
(117, 71, '45.248.144.130', '2018-10-10 16:01:38'),
(118, 32, '45.248.144.130', '2018-10-10 17:50:42'),
(119, 32, '45.248.144.130', '2018-10-10 17:58:23'),
(120, 71, '45.248.146.194', '2018-10-11 05:38:02'),
(121, 71, '45.248.146.194', '2018-10-11 09:13:47'),
(122, 32, '45.248.144.130', '2018-10-11 13:31:09'),
(123, 32, '45.248.144.130', '2018-10-11 13:49:01'),
(124, 32, '45.248.144.130', '2018-10-11 13:49:49'),
(125, 32, '45.248.144.130', '2018-10-11 13:56:57'),
(126, 32, '45.248.144.130', '2018-10-11 13:58:07'),
(127, 71, '45.248.144.130', '2018-10-11 14:19:11'),
(128, 71, '45.248.144.130', '2018-10-11 14:20:56'),
(129, 32, '45.248.144.130', '2018-10-11 14:46:55'),
(130, 32, '103.60.160.153', '2018-10-11 18:44:11'),
(131, 71, '45.248.146.194', '2018-10-12 05:28:42'),
(132, 32, '45.248.146.194', '2018-10-12 05:31:59'),
(133, 32, '45.248.146.194', '2018-10-12 05:40:26'),
(134, 32, '45.248.146.194', '2018-10-12 09:12:20'),
(135, 32, '45.248.146.194', '2018-10-12 09:32:08'),
(136, 32, '45.248.146.194', '2018-10-12 11:42:59'),
(137, 86, '45.248.146.194', '2018-10-12 12:29:14'),
(138, 86, '45.248.146.194', '2018-10-13 07:15:12'),
(139, 86, '45.248.146.194', '2018-10-13 10:08:06'),
(140, 86, '45.248.146.194', '2018-10-13 10:08:42'),
(141, 86, '45.248.146.194', '2018-10-14 12:09:32'),
(142, 32, '45.248.146.194', '2018-10-14 12:40:48'),
(143, 32, '45.248.146.194', '2018-10-14 12:41:09'),
(144, 32, '103.60.160.86', '2018-10-14 23:54:27'),
(145, 32, '103.60.160.86', '2018-10-15 00:46:15'),
(146, 32, '103.60.160.86', '2018-10-15 01:29:20'),
(147, 32, '103.60.160.86', '2018-10-15 01:36:37'),
(148, 32, '103.60.160.86', '2018-10-15 01:44:03'),
(149, 32, '45.248.146.194', '2018-10-15 05:11:50'),
(150, 32, '45.248.146.194', '2018-10-15 05:17:59'),
(151, 32, '45.248.146.194', '2018-10-15 05:41:27'),
(152, 86, '45.248.146.194', '2018-10-15 05:42:21'),
(153, 88, '45.248.146.194', '2018-10-15 06:23:40'),
(154, 89, '73.179.72.71', '2018-10-15 06:32:42'),
(155, 88, '45.248.146.194', '2018-10-15 07:09:44'),
(156, 89, '45.248.144.130', '2018-10-15 12:05:23'),
(157, 89, '116.58.203.37', '2018-10-15 12:06:23'),
(158, 32, '103.60.160.86', '2018-10-15 23:00:29'),
(159, 32, '103.60.160.86', '2018-10-16 03:10:45'),
(160, 32, '103.60.160.86', '2018-10-16 03:16:38'),
(161, 89, '73.179.72.71', '2018-10-16 05:08:05'),
(162, 32, '45.248.146.194', '2018-10-16 05:48:16'),
(163, 99, '45.248.146.194', '2018-10-16 07:07:02'),
(164, 99, '45.248.146.194', '2018-10-16 07:08:25'),
(165, 100, '45.248.146.194', '2018-10-16 07:33:53'),
(166, 100, '45.248.146.194', '2018-10-16 13:35:27'),
(167, 100, '45.248.146.194', '2018-10-16 14:16:40'),
(168, 99, '45.248.146.194', '2018-10-16 14:16:55'),
(169, 101, '45.248.146.194', '2018-10-16 14:47:42'),
(170, 99, '45.248.146.194', '2018-10-16 14:51:06'),
(171, 99, '45.248.146.194', '2018-10-16 14:55:36'),
(172, 99, '45.248.146.194', '2018-10-16 14:56:18'),
(173, 99, '45.248.146.194', '2018-10-16 14:56:49'),
(174, 99, '45.248.146.194', '2018-10-16 15:13:54'),
(175, 99, '45.248.146.194', '2018-10-16 15:23:18'),
(176, 99, '45.248.146.194', '2018-10-16 15:23:50'),
(177, 99, '45.248.146.194', '2018-10-16 15:30:43'),
(178, 99, '45.248.146.194', '2018-10-16 15:32:23'),
(179, 89, '::1', '2018-10-16 17:27:39'),
(180, 89, '::1', '2018-10-16 18:02:02'),
(181, 89, '::1', '2018-10-16 18:19:20'),
(182, 89, '::1', '2018-10-17 00:26:37'),
(183, 89, '::1', '2018-10-17 01:02:04'),
(184, 89, '::1', '2018-10-17 01:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `calculator`
--

CREATE TABLE `calculator` (
  `id` int(11) NOT NULL,
  `shipName` varchar(100) DEFAULT NULL,
  `dwt` varchar(100) DEFAULT NULL,
  `loa` varchar(100) DEFAULT NULL,
  `imo` varchar(100) DEFAULT NULL,
  `port` varchar(100) DEFAULT NULL,
  `activity` varchar(100) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `cargo` varchar(255) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `directoryName` varchar(200) DEFAULT NULL,
  `arrival` date DEFAULT NULL,
  `berthing` date DEFAULT NULL,
  `completion` date DEFAULT NULL,
  `demurrageRate` double DEFAULT NULL,
  `demurrageRateUnit` varchar(200) DEFAULT NULL,
  `despatchRate` double DEFAULT NULL,
  `despatchRateUnit` varchar(200) DEFAULT NULL,
  `norTendered` varchar(200) DEFAULT NULL,
  `rateUnit` varchar(200) DEFAULT NULL,
  `timeStart` datetime DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `vesselId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calculator`
--

INSERT INTO `calculator` (`id`, `shipName`, `dwt`, `loa`, `imo`, `port`, `activity`, `quantity`, `cargo`, `rate`, `directoryName`, `arrival`, `berthing`, `completion`, `demurrageRate`, `demurrageRateUnit`, `despatchRate`, `despatchRateUnit`, `norTendered`, `rateUnit`, `timeStart`, `fileName`, `createdBy`, `vesselId`) VALUES
(12, 'abc', '0', '0', '333', 'Chittagong', 'LOAD', 1000, 'GRAINS', 2000, 'MVHanjalaRecord', '2018-10-15', '2018-10-15', '2018-10-15', 1000, 'EUR/HOUR', 500, 'EUR/HOUR', '2018-10-15 05:57:18', 'DAYS', '2018-10-15 05:57:21', NULL, 0, 0),
(13, 'MV Hanjala', '0', '0', '101', 'Jedda', 'LOAD', NULL, 'PETCOKE', 1000, 'MVHanjalaRecord', '2018-10-15', '2018-10-15', NULL, 1000, 'USD/DAY', 500, 'USD/DAY', NULL, 'DAYS', NULL, NULL, 0, 0),
(14, 'MV Hanjala', '0', '0', '101', 'Jedda', 'LOAD', NULL, 'PETCOKE', 1000, 'MVHanjalaRecord', '2018-10-15', '2018-10-15', NULL, 1000, 'USD/DAY', 500, 'USD/DAY', NULL, 'DAYS', NULL, 'sgfs', 0, 0),
(15, 'MV Hanjala', '0', '0', '101', NULL, NULL, NULL, NULL, NULL, 'MVHanjalaRecord', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
(16, 'MV Hanjala', '0', '0', '101', NULL, NULL, NULL, NULL, NULL, 'MVHanjalaRecord', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32, 12),
(17, 'MV Hanjala', '0', '0', '101', 'Jedda', NULL, NULL, 'GRAINS', 1000, 'MVHanjalaRecord', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MT/DAY', NULL, NULL, 32, 12),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'MVHanjalaRecord', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32, 12),
(19, 'MV Hanjala', '0', '0', '101', 'Chittagong', NULL, NULL, NULL, NULL, 'MVHanjalaRecord', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 32, 12);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parentId`, `name`) VALUES
(1, 0, 'supercategory'),
(2, 1, 'file'),
(3, 2, 'pdf'),
(4, 2, 'docs'),
(5, 2, 'ppt'),
(6, 2, 'jpg'),
(7, 2, 'png'),
(8, 2, 'folder'),
(9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_dropdown_data`
--

CREATE TABLE `custom_dropdown_data` (
  `id` bigint(20) NOT NULL,
  `dropdown_for` varchar(255) NOT NULL,
  `dropdown_text` varchar(255) NOT NULL,
  `created_by_user_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_dropdown_data`
--

INSERT INTO `custom_dropdown_data` (`id`, `dropdown_for`, `dropdown_text`, `created_by_user_id`, `created_at`) VALUES
(3, 'activity', 'Test ACT', 32, '2018-07-29 19:15:31'),
(17, 'cargo', 'Cargo2', 32, '2018-07-30 13:55:53'),
(19, 'cargo', 'Cargo 1', 33, '2018-07-30 14:06:40'),
(20, 'description', 'TEST DESCRIPTION', 32, '2018-07-30 17:00:41'),
(21, 'description', 'TEST DESCRIPTION 2', 32, '2018-07-30 17:01:18'),
(23, 'cargo', 'Car Import', 32, '2018-08-14 09:01:11'),
(24, 'cargo', 'Car Import', 32, '2018-08-14 09:01:13'),
(25, 'activity', 'Fishing Vessel', 32, '2018-10-09 10:42:26'),
(26, 'activity', 'Other', 32, '2018-10-14 16:55:22');

-- --------------------------------------------------------

--
-- Table structure for table `deduction_description`
--

CREATE TABLE `deduction_description` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `value` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `docs`
--

CREATE TABLE `docs` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_directory` varchar(100) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `type_name` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `item_link` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `vessel_id` int(11) NOT NULL,
  `is_active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `docs`
--

INSERT INTO `docs` (`id`, `parent_id`, `parent_directory`, `type_id`, `type_name`, `name`, `item_link`, `created_by`, `created_date`, `vessel_id`, `is_active`) VALUES
(23, 1, '', 8, 'folder', 'Habib', '/itinery/shohag', 0, '0000-00-00 00:00:00', 0, NULL),
(24, 1, '', 8, 'folder', 'Alom', '/itinery/shohag', 0, '0000-00-00 00:00:00', 0, NULL),
(25, 1, '', 8, 'folder', 'shohag', '/itinery/shohag', 0, '0000-00-00 00:00:00', 0, NULL),
(26, 1, '', 8, 'folder', 'mahbub', '/itinery/mahbub', 0, '0000-00-00 00:00:00', 0, NULL),
(27, NULL, '', 8, 'folder', 'muaz', '/mahbub/muaz', 0, '0000-00-00 00:00:00', 0, NULL),
(28, 26, '', 8, 'folder', 'muaz', '/mahbub/muaz', 0, '0000-00-00 00:00:00', 0, NULL),
(29, 28, '', 8, 'folder', 'haresa', '/muaz/haresa', 0, '0000-00-00 00:00:00', 0, NULL),
(30, NULL, 'muaz', NULL, 'png', 'Screenshot from 2018-01-31 23-17-46.png', '/muaz/Screenshot from 2018-01-31 23-17-46.png', NULL, NULL, 0, NULL),
(31, NULL, 'muaz', NULL, 'png', 'Screenshot from 2018-01-31 23-17-46.png', '/muaz/Screenshot from 2018-01-31 23-17-46.png', NULL, NULL, 0, NULL),
(32, NULL, 'muaz', NULL, 'png', 'Screenshot from 2018-01-31 23-17-46.png', '/muaz/Screenshot from 2018-01-31 23-17-46.png', NULL, NULL, 0, NULL),
(33, 29, 'haresa', NULL, 'png', 'Screenshot from 2018-01-31 23-17-46.png', '/haresa/Screenshot from 2018-01-31 23-17-46.png', NULL, NULL, 0, NULL),
(34, 24, 'shohag', NULL, 'png', 'Screenshot from 2018-01-31 23-18-37.png', '/shohag/Screenshot from 2018-01-31 23-18-37.png', NULL, NULL, 0, NULL),
(35, 1, '', 8, 'folder', 'Jakariya', '/itinery/Jakariya', 0, '0000-00-00 00:00:00', 0, NULL),
(36, 1, '', 8, 'folder', 'Nazmul', '/itinery/Nazmul', 0, '0000-00-00 00:00:00', 0, NULL),
(37, 1, '', 8, 'folder', 'Hasib', '/itinery/Hasib', 0, '0000-00-00 00:00:00', 0, NULL),
(38, 1, '', 8, 'folder', 'Mufachir', '/itinery/Mufachir', 0, '0000-00-00 00:00:00', 0, NULL),
(39, 1, '', 8, 'folder', 'Hamza', '/itinery/Hamza', 0, '0000-00-00 00:00:00', 0, NULL),
(40, 1, '', 8, 'folder', 'Naba', '/itinery/Naba', 0, '0000-00-00 00:00:00', 0, NULL),
(41, 25, '', 8, 'folder', 'sadfdsf', '/shohag/sadfdsf', 0, '0000-00-00 00:00:00', 0, NULL),
(42, 40, '', 8, 'folder', 'Juairiya', '/Naba/Juairiya', 0, '0000-00-00 00:00:00', 0, NULL),
(43, 39, 'Hamza', NULL, 'jpg', 'softrithmLogo.jpg', '/Hamza/softrithmLogo.jpg', NULL, NULL, 0, NULL),
(44, 39, '', 8, 'folder', 'Hanjala', '/Hamza/Hanjala', 0, '0000-00-00 00:00:00', 0, NULL),
(45, 39, 'Hamza', NULL, 'pdf', 'nibondhon.pdf', '/Hamza/nibondhon.pdf', NULL, NULL, 0, NULL),
(46, 39, 'Hamza', NULL, 'odt', 'Biodata_Of_Mahbub.odt', '/Hamza/Biodata_Of_Mahbub.odt', NULL, NULL, 0, NULL),
(47, 1, '', 8, 'folder', 'kingdom', '/docs/kingdom/', 0, '0000-00-00 00:00:00', 0, NULL),
(48, 47, '', 8, 'folder', 'subkingdom', '/docs/kingdom/subkingdom/', 0, '0000-00-00 00:00:00', 0, NULL),
(49, 48, '', 8, 'folder', 'phylum', '/docs/kingdom/subkingdom/phylum/', 0, '0000-00-00 00:00:00', 0, NULL),
(50, 49, '', 8, 'folder', 'class', '/docs/kingdom/subkingdom/phylum/class/', 0, '0000-00-00 00:00:00', 0, NULL),
(51, 50, '', 8, 'folder', 'order', '/docs/kingdom/subkingdom/phylum/class/order/', 0, '0000-00-00 00:00:00', 0, NULL),
(52, 50, '', 8, 'folder', 'genus', '/docs/kingdom/subkingdom/phylum/class/genus/', 0, '0000-00-00 00:00:00', 0, NULL),
(53, 50, '', 8, 'folder', 'species', '/docs/kingdom/subkingdom/phylum/class/species/', 0, '0000-00-00 00:00:00', 0, NULL),
(54, 51, '', 8, 'folder', 'genus', '/docs/kingdom/subkingdom/phylum/class/order/genus/', 0, '0000-00-00 00:00:00', 0, NULL),
(55, 54, '', 8, 'folder', 'species', '/docs/kingdom/subkingdom/phylum/class/order/genus/species/', 0, '0000-00-00 00:00:00', 0, NULL),
(56, 47, 'docs/kingdom/', NULL, 'jpg', 'animals.jpg', '/docs/kingdom/animals.jpg', NULL, NULL, 0, NULL),
(57, 48, 'docs/kingdom/subkingdom/', NULL, 'jpg', 'ape.jpg', '/docs/kingdom/subkingdom/ape.jpg', NULL, NULL, 0, NULL),
(58, 49, 'docs/kingdom/subkingdom/phylum/', NULL, 'jpeg', 'dear.jpeg', '/docs/kingdom/subkingdom/phylum/dear.jpeg', NULL, NULL, 0, NULL),
(59, 51, 'docs/kingdom/subkingdom/phylum/class/order/', NULL, 'jpg', 'albino-animals-3.jpg', '/docs/kingdom/subkingdom/phylum/class/order/albino-animals-3.jpg', NULL, NULL, 0, NULL),
(60, 51, 'docs/kingdom/subkingdom/phylum/class/order/', NULL, 'jpg', 'maxresdefault.jpg', '/docs/kingdom/subkingdom/phylum/class/order/maxresdefault.jpg', NULL, NULL, 0, NULL),
(61, 1, 'docs/', NULL, 'jpg', 'animals.jpg', '/docs/animals.jpg', NULL, NULL, 0, NULL),
(62, 1, 'docs/', NULL, 'jpg', 'albino-animals-3.jpg', '/docs/albino-animals-3.jpg', NULL, NULL, 0, NULL),
(63, 1, 'docs/', NULL, 'pdf', 'hello.pdf', '/docs/hello.pdf', NULL, NULL, 0, NULL),
(64, 1, '', 8, 'folder', 'Hafijur_Rahman', '/docs/Hafijur_Rahman/', 0, '0000-00-00 00:00:00', 0, NULL),
(65, 64, 'docs/Hafijur_Rahman/', NULL, 'png', 'win-the-first-hour-of-the-day_quote_jenkelchner-400x284.png', '/docs/Hafijur_Rahman/win-the-first-hour-of-the-day_quote_jenkelchner-400x284.png', NULL, NULL, 0, NULL),
(66, 64, '', 8, 'folder', 'Sadia', '/docs/Hafijur_Rahman/Sadia/', 0, '0000-00-00 00:00:00', 0, NULL),
(67, 66, 'docs/Hafijur_Rahman/Sadia/', NULL, 'pdf', 'chantalleteo.pdf', '/docs/Hafijur_Rahman/Sadia/chantalleteo.pdf', NULL, NULL, 0, NULL),
(68, 1, '', 8, 'folder', 'Nayeem', '/docs/Nayeem/', 0, '0000-00-00 00:00:00', 0, NULL),
(69, 47, 'docs/kingdom/', NULL, 'jpg', '38918489_2220556211506995_7356122154376626176_n.jpg', '/docs/kingdom/38918489_2220556211506995_7356122154376626176_n.jpg', NULL, NULL, 0, NULL),
(70, 48, '', 8, 'folder', 'Shahjalal', '/docs/kingdom/subkingdom/Shahjalal/', 0, '0000-00-00 00:00:00', 0, NULL),
(71, 70, '', 8, 'folder', 'My Images', '/docs/kingdom/subkingdom/Shahjalal/My Images/', 0, '0000-00-00 00:00:00', 0, NULL),
(72, 1, '', 8, 'folder', 'MV Shindabad', '/undefinedMV Shindabad/', 0, '0000-00-00 00:00:00', 9, NULL),
(73, 72, '', 8, 'folder', 'my directory', '/MV Shindabad/my directory/', 0, '0000-00-00 00:00:00', 0, NULL),
(74, 73, 'MV Shindabad/my directory/', NULL, 'jpg', 'softrithmLogo.jpg', '/MV Shindabad/my directory/softrithmLogo.jpg', NULL, NULL, 0, NULL),
(75, 1, '', 8, 'folder', 'MV Doe', '/undefinedMV Doe/', 0, '0000-00-00 00:00:00', 10, NULL),
(76, 75, 'MV Doe/', NULL, 'png', 'animation - location.png', '/MV Doe/animation - location.png', NULL, NULL, 0, NULL),
(77, 75, '', 8, 'folder', 'Test', '/MV Doe/Test/', 0, '0000-00-00 00:00:00', 0, NULL),
(78, 1, '', 8, 'folder', 'HafijFolder', '/undefinedHafijFolder/', 0, '0000-00-00 00:00:00', 11, NULL),
(79, 78, 'HafijFolder/', NULL, 'jpg', 'Koala.jpg', '/HafijFolder/Koala.jpg', NULL, NULL, 0, NULL),
(80, 1, '', 8, 'folder', 'MVHanjalaRecord', '/undefinedMVHanjalaRecord/', 0, '0000-00-00 00:00:00', 12, NULL),
(81, 80, '', 8, 'folder', 'My Repository', '/MVHanjalaRecord/My Repository/', 0, '0000-00-00 00:00:00', 0, NULL),
(82, 80, '', 8, 'folder', 'Hafijur Rahman', '/MVHanjalaRecord/Hafijur Rahman/', 0, '0000-00-00 00:00:00', 0, NULL),
(83, 78, 'HafijFolder/', NULL, 'jpg', 'Rio Haina - port map.jpg', '/HafijFolder/Rio Haina - port map.jpg', NULL, NULL, 0, NULL),
(84, 78, '', 8, 'folder', 'test 1', '/HafijFolder/test 1/', 0, '0000-00-00 00:00:00', 0, NULL),
(85, 80, 'MVHanjalaRecord/', NULL, 'png', 'download.png', '/MVHanjalaRecord/download.png', NULL, NULL, 0, NULL),
(86, 80, 'MVHanjalaRecord/', NULL, 'pdf', 'chantalleteo.pdf', '/MVHanjalaRecord/chantalleteo.pdf', NULL, NULL, 0, NULL),
(87, 80, '', 8, 'folder', 'abc', '/MVHanjalaRecord/abc/', 32, '0000-00-00 00:00:00', 0, NULL),
(88, 80, '', 8, 'folder', 'daily star', '/MVHanjalaRecord/daily star/', 32, '0000-00-00 00:00:00', 0, NULL),
(89, 80, '', 8, 'folder', 'new age', '/MVHanjalaRecord/new age/', 32, '0000-00-00 00:00:00', 0, 1),
(90, 87, '', 8, 'folder', 'daily star', '/MVHanjalaRecord/daily star/', 32, '0000-00-00 00:00:00', 0, NULL),
(91, 1, '', 8, 'folder', 'Hanjala - Jed bl', '/undefinedHanjala - Jed bl/', NULL, '0000-00-00 00:00:00', 13, NULL),
(92, 91, '', 8, 'folder', 'Personal', '/Hanjala - Jed bl/Personal/', 100, '0000-00-00 00:00:00', 0, NULL),
(93, 91, '', 8, 'folder', 'Images', '/Hanjala - Jed bl/Images/', 100, '0000-00-00 00:00:00', 0, 1),
(94, 91, '', 8, 'folder', 'abc', '/Hanjala - Jed bl/abc/', 100, '0000-00-00 00:00:00', 0, NULL),
(95, 91, '', 8, 'folder', 'washington times', '/Hanjala - Jed bl/washington times/', 100, '0000-00-00 00:00:00', 0, NULL),
(96, 92, '', 8, 'folder', 'washington times', '/Hanjala - Jed bl/washington times/', 100, '0000-00-00 00:00:00', 0, NULL),
(97, 91, '', 8, 'folder', 'softrithm it', '/Hanjala - Jed bl/softrithm it/', 100, '0000-00-00 00:00:00', 0, NULL),
(98, 92, '', 8, 'folder', 'softrithm it', '/Hanjala - Jed bl/softrithm it/', 100, '0000-00-00 00:00:00', 0, 1),
(99, 91, '', 8, 'folder', 'abc', '/Hanjala - Jed bl/abc/', 89, '0000-00-00 00:00:00', 0, 1),
(100, 91, '', 8, 'folder', 'move', '/Hanjala - Jed bl/move/', 89, '0000-00-00 00:00:00', 0, NULL),
(101, 91, '', 8, 'folder', 'move1', '/Hanjala - Jed bl/move1/', 89, '0000-00-00 00:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `endorsement`
--

CREATE TABLE `endorsement` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `endorse_to_email` varchar(255) NOT NULL,
  `endorse_month` varchar(100) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `endorsement`
--

INSERT INTO `endorsement` (`id`, `user_id`, `endorse_to_email`, `endorse_month`, `status`, `created_date`) VALUES
(96, '32', 'hafizur.csejnu@gmail.com', 'October 2018', '0', '2018-10-02 22:00:10'),
(104, '32', 'hafiz.softrithmit@gmail.com', 'October 2018', '0', '2018-10-07 06:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `individual`
--

CREATE TABLE `individual` (
  `individual_id` int(2) NOT NULL,
  `individual_name` varchar(30) NOT NULL,
  `individual_address` varchar(80) NOT NULL,
  `individual_contact` int(15) NOT NULL,
  `individual_email` varchar(30) NOT NULL,
  `individual_password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `individual`
--

INSERT INTO `individual` (`individual_id`, `individual_name`, `individual_address`, `individual_contact`, `individual_email`, `individual_password`) VALUES
(2, 'Dhruba KC', '405, windmill House', 894084194, 'dhruba_kc@hotmail.com', 'c44a471bd78cc6c2fea32b9fe028d30a'),
(4, 'Rabin', 'Dockroad', 899532283, 'sapkota.robin18@gmail.com', '5cfdbbc033e0ab0a21a80bf63783c7fd');

-- --------------------------------------------------------

--
-- Table structure for table `itinerary`
--

CREATE TABLE `itinerary` (
  `id` int(11) NOT NULL,
  `vessel_id` int(11) DEFAULT NULL,
  `itinerary_date` timestamp NULL DEFAULT NULL,
  `est_arrival_time` datetime DEFAULT NULL,
  `actual_arrival_time` datetime DEFAULT NULL,
  `est_inspection_time` datetime DEFAULT NULL,
  `actual_inspection_time` datetime DEFAULT NULL,
  `est_bearthing_time` datetime DEFAULT NULL,
  `actual_bearthing_time` datetime DEFAULT NULL,
  `est_starting_time` datetime DEFAULT NULL,
  `actual_starting_time` datetime DEFAULT NULL,
  `est_completion_time` datetime DEFAULT NULL,
  `actual_completion_time` datetime DEFAULT NULL,
  `est_sailing_time` datetime DEFAULT NULL,
  `actual_sailing_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itinerary`
--

INSERT INTO `itinerary` (`id`, `vessel_id`, `itinerary_date`, `est_arrival_time`, `actual_arrival_time`, `est_inspection_time`, `actual_inspection_time`, `est_bearthing_time`, `actual_bearthing_time`, `est_starting_time`, `actual_starting_time`, `est_completion_time`, `actual_completion_time`, `est_sailing_time`, `actual_sailing_time`) VALUES
(27, 0, '2018-10-09 15:19:37', '2018-10-09 08:19:39', NULL, '2018-10-09 08:19:46', '2018-10-09 08:19:45', '2018-10-09 08:19:48', '2018-10-18 08:20:02', '2018-10-09 08:19:50', '2018-10-04 08:19:59', '2018-10-09 08:19:51', '2018-10-09 08:19:58', '2018-10-09 08:19:53', '2018-11-05 08:19:55'),
(28, 0, NULL, NULL, NULL, '2018-10-15 05:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 0, '2018-10-10 03:06:09', '2018-10-01 20:06:19', '2018-10-17 20:06:22', '2018-10-15 20:06:27', '2018-10-24 20:06:30', '2018-10-22 20:06:33', '2018-10-31 20:06:35', '2018-10-30 20:06:38', '2018-10-24 20:06:40', '2018-10-30 20:06:43', '2018-10-25 20:06:45', '2018-10-30 20:06:48', '2018-11-09 20:06:50'),
(30, 0, '2018-10-18 03:06:09', '2018-10-01 20:06:19', '2018-10-17 20:06:22', '2018-10-15 20:06:27', '2018-10-24 20:06:30', '2018-10-22 20:06:33', '2018-10-31 20:06:35', '2018-10-30 20:06:38', '2018-10-24 20:06:40', '2018-10-30 20:06:43', '2018-10-25 20:06:45', '2018-10-30 20:06:48', '2018-11-09 20:06:50'),
(31, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 0, NULL, '2018-10-17 08:24:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `itineraryn`
--

CREATE TABLE `itineraryn` (
  `id` int(11) NOT NULL,
  `cargo` varchar(200) NOT NULL,
  `vesselId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `vessel` varchar(200) NOT NULL,
  `port` varchar(200) NOT NULL,
  `vessel_folder` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itineraryn`
--

INSERT INTO `itineraryn` (`id`, `cargo`, `vesselId`, `created_by`, `created_date`, `is_active`, `vessel`, `port`, `vessel_folder`) VALUES
(1, 'sadfds', 0, 0, 0, 0, 'MV Hanjala', 'Jedda', 'Hanjala - Jed bl'),
(2, 'sadfds', 0, 0, 0, 0, 'MV Hanjala', 'Jedda', 'Hanjala - Jed bl'),
(3, 'sadfds', 0, 0, 0, 0, 'MV Hanjala', 'Jedda', 'Hanjala - Jed bl'),
(4, 'sadfds', 0, 0, 0, 0, 'MV Hanjala', 'Jedda', 'Hanjala - Jed bl'),
(5, 'sadfds', 0, 0, 0, 0, 'MV Hanjala', 'Jedda', 'Hanjala - Jed bl');

-- --------------------------------------------------------

--
-- Table structure for table `itinerary_events`
--

CREATE TABLE `itinerary_events` (
  `id` int(11) NOT NULL,
  `event_name` varchar(200) NOT NULL,
  `estimate_date` date NOT NULL,
  `actual_date` date NOT NULL,
  `itineraryn_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itinerary_events`
--

INSERT INTO `itinerary_events` (`id`, `event_name`, `estimate_date`, `actual_date`, `itineraryn_id`) VALUES
(1, 'asfd', '0000-00-00', '0000-00-00', 5),
(2, 'asdf', '0000-00-00', '0000-00-00', 5);

-- --------------------------------------------------------

--
-- Table structure for table `itinery_repository`
--

CREATE TABLE `itinery_repository` (
  `id` int(11) NOT NULL,
  `parenId` int(11) DEFAULT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `path` varchar(45) DEFAULT NULL,
  `directoryName` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itinery_repository`
--

INSERT INTO `itinery_repository` (`id`, `parenId`, `displayName`, `path`, `directoryName`) VALUES
(1, 0, 'itnery', '/itinery', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_act` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `title`, `description`, `is_act`) VALUES
(1, 'Package 1', 'Feature 01 <br> \r\nFeature 02 <br> \r\nFeature 03 <br> ', NULL),
(2, 'Package 2', 'Feature 01 <br> \r\nFeature 02 <br> \r\nFeature 03 <br> ', NULL),
(3, 'Package 03', 'Feature 01 <br> \r\nFeature 02 <br> \r\nFeature 03 <br> ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `method` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `month` varchar(255) NOT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `port`
--

CREATE TABLE `port` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `call_sign` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  `is_act` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `port`
--

INSERT INTO `port` (`id`, `name`, `call_sign`, `userId`, `is_act`) VALUES
(1, 'Chittagong', '567', 0, 0),
(2, 'Jedda', '123', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pricing`
--

CREATE TABLE `pricing` (
  `id` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `package_type` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `freatures` text NOT NULL,
  `created_at` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `created_by_user_id` int(11) NOT NULL,
  `last_updated_by_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `share_docs_user`
--

CREATE TABLE `share_docs_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `docs_id` int(11) NOT NULL,
  `shared_by_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `share_docs_user`
--

INSERT INTO `share_docs_user` (`id`, `user_id`, `docs_id`, `shared_by_id`) VALUES
(13, 32, 79, 32),
(14, 32, 81, 32),
(15, 80, 81, 32),
(16, 32, 82, 32),
(17, 32, 88, 32),
(18, 100, 95, 100),
(19, 100, 97, 100),
(20, 89, 99, 89),
(21, 101, 99, 89);

-- --------------------------------------------------------

--
-- Table structure for table `ship`
--

CREATE TABLE `ship` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `imo` double NOT NULL,
  `mmsi` double NOT NULL,
  `ship_type` varchar(255) NOT NULL,
  `flag` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `call_sign` varchar(255) NOT NULL,
  `loa` int(11) NOT NULL,
  `beam` varchar(255) NOT NULL,
  `grt` double NOT NULL,
  `dwt` double NOT NULL,
  `teu` int(11) NOT NULL,
  `draught` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ship`
--

INSERT INTO `ship` (`id`, `name`, `imo`, `mmsi`, `ship_type`, `flag`, `destination`, `call_sign`, `loa`, `beam`, `grt`, `dwt`, `teu`, `draught`) VALUES
(26, 'j&j', 455, 0, 'sdfafds', '', '', '', 0, '', 0, 0, 0, ''),
(27, 'abc', 333, 0, 'dsfasdf', '', '', '', 0, '', 0, 0, 0, ''),
(28, 'sfad', 44, 4, '444', '', '', '', 0, '', 0, 0, 0, ''),
(29, 'MV Hanjala', 101, 0, 'Merchant Vessel', '', '', '', 0, '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ship_info`
--

CREATE TABLE `ship_info` (
  `id` int(11) NOT NULL,
  `ship_name` varchar(200) DEFAULT NULL,
  `loa` varchar(200) DEFAULT NULL,
  `dwt` varchar(200) DEFAULT NULL,
  `imo` varchar(200) DEFAULT NULL,
  `directoryName` varchar(200) NOT NULL,
  `fileName` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ship_info`
--

INSERT INTO `ship_info` (`id`, `ship_name`, `loa`, `dwt`, `imo`, `directoryName`, `fileName`, `created_by`) VALUES
(7, 'ANNA-THERESE', '13', '0', '0', '', '', 0),
(8, 'GINGA-CARACAL', '160', '26015', '9426300', '', '', 0),
(9, 'GINGA-CARACAL', '160', '26015', '9426300', '', '', 0),
(10, 'ANNA-THERESE', '13', '0', '0', '', '', 0),
(11, 'CHRISTI-C.', '30', '0', '0', '', '', 0),
(12, 'CHRISTI-C.', '30', '0', '0', '', '', 0),
(13, 'ASIAN-MOON', '148', '13670', '9359117', '', '', 0),
(14, 'C2', '79', '220', '1009833', '', '', 0),
(15, NULL, NULL, NULL, NULL, '', '', 0),
(16, 'RVE-28', '10', '0', '0', 'itnery', 'MahbubAlam', 0),
(17, 'CHRISTI-C.', '30', '0', '0', 'itnery', 'Muaz', 0),
(18, 'GINGA-CARACAL', '160', '26015', '9426300', 'itnery', 'Halima_Sadia', 0),
(19, 'GINGA-CARACAL', '160', '26015', '9426300', 'itinery', 'asfddsafdsa', 0),
(20, 'RVE-28', '10', '0', '0', 'itinery', 'Hafizur Rahman.', 0),
(21, 'RVE-28', '10', '0', '0', 'itinery', 'hr test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `subscription_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `month` varchar(100) NOT NULL,
  `subscription_fee` varchar(100) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `subscription_id`, `user_id`, `month`, `subscription_fee`, `first_name`, `last_name`, `email`, `created_at`) VALUES
(1, 'sub_DmJkTJ5k7dApLX', 89, '2018/10', '$15', 'Sales', 'Chanul', 'sales@chanul.us', '2018-10-11 07:35:53'),
(2, '', 98, '2018/10', '', 'Haifzur', 'Rahman', 'hr.hafiz@yahoo.com', '2018-10-11 00:48:40'),
(5, '', 89, '2018/10', '', 'prueba', 'prueba', 'andom@gprueba.com', '2018-10-11 02:39:13'),
(6, '', 72, '2018/10', '', 'SoftRithm', 'Localoy', 'hr.hafiz@yahoo.com', '2018-10-11 02:40:54'),
(10, 'sub_DnUiNNmrE5Psdt', 101, '2018/10', '$15', 'Mufachir', 'Hossain', 'mufachirhossain@gmail.com', '2018-10-16 07:40:34');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` varchar(255) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `product` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `customer_id`, `product`, `amount`, `currency`, `status`, `created_at`) VALUES
('ch_1DJyuRE6QY2mkOiXGtruO4KA', 'cus_DlVw8mAw6u85wU', 'Monthly Subscription Fee', '5000', 'usd', 'succeeded', '2018-10-11 00:48:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `confirmation_link` varchar(255) DEFAULT NULL,
  `reset_token` varchar(255) DEFAULT NULL,
  `reset_token_expiry_date` datetime DEFAULT NULL,
  `package` varchar(100) DEFAULT NULL,
  `created_date` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `company_name`, `email`, `password`, `status`, `confirmation_link`, `reset_token`, `reset_token_expiry_date`, `package`, `created_date`) VALUES
(84, 'Gabriel Piedrahita', 'Chanul', 'sacotin05@outlook.com', '806487591dc33b3a72bff882ce3c7ee6', 1, '88fc2f9b059e8779d487a132d22e09ed1742143701', '88fc2f9b059e8779d487a132d22e09ed16383156', NULL, '1', '2018-10-01'),
(89, 'Gabriel Piedrahita', 'Chanul', 'shohag@gmail.com', '96e79218965eb72c92a549dd5a330112', 1, '507d9202c0927d0d0f37f3b22a1d1a6c2048283282', NULL, NULL, '1', '2018-10-15'),
(99, 'Mufachir Hossain', 'SoftRithm IT Limited', 'hafizur.csejnu@gmail.com', '5433170077211737d0f482550b50d7ed', 1, '3221af568e71800d1b89990b9c618a6b1180281576', NULL, NULL, '1', '2018-10-16'),
(100, 'Mizanur Rahman', 'SoftRithm IT Limited', 'md.mizanmr9@gmail.com', '2e9267e78c8814184ce74868f5361cae', 1, '36489a6123ce17d333f4a2b563b13c811951051584', NULL, NULL, '1', '2018-10-16'),
(101, 'Mufachir Hossain', 'SoftRithm IT Limited', 'mufachirhossain@gmail.com', '7686e46a58464ac75f8ecc3ee62fc7bb', 1, '4c1cd7e6dd2913161b763e03fe031258935500454', '4c1cd7e6dd2913161b763e03fe0312581504139196', NULL, '1', '2018-10-16');

-- --------------------------------------------------------

--
-- Table structure for table `vessel`
--

CREATE TABLE `vessel` (
  `id` int(11) NOT NULL,
  `vessel` varchar(300) NOT NULL,
  `loa` varchar(100) NOT NULL,
  `year` varchar(100) NOT NULL,
  `vessel_type` varchar(100) NOT NULL,
  `imo` varchar(100) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `port` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `vessel_id` varchar(100) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_date` date NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vessel`
--

INSERT INTO `vessel` (`id`, `vessel`, `loa`, `year`, `vessel_type`, `imo`, `activity`, `port`, `code`, `vessel_id`, `created_by`, `created_date`, `is_active`) VALUES
(1, 'STELLA-BEL', '24', '2018', 'Fishing vesselrrr', '5090567', 'Bunkering', 'Long Beach', 'sdaf', 'sdfa', '', '2018-09-21', 1),
(2, 'ANNA-THERESE', '13', '2018', 'Fishing vessel', '0', 'Discharge', 'Long Beach', '101254', 'Hello Anna', '', '2018-09-21', 1),
(3, 'STELLA-BEL', '24', '2018', 'Fishing vesselrrr', '5090567', 'Discharge', 'Long Beach', '01210', 'My Vessel', '', '2018-09-22', 1),
(4, 'GINGA-CARACAL', '160', '2019', 'Tanker (HAZ-B)', '9426300', 'Discharge', 'Long Beach', '001', 'Mahbub Vessel', '', '2018-09-23', 1),
(5, 'GINGA-CARACAL', '160', '2019', 'Tanker (HAZ-B)', '9426300', 'Load', 'Long Beach', '002', 'Jakariya Vessel', '', '2018-09-23', 1),
(6, 'ANNA-THERESE', '13', '2025', 'Fishing vessel', '0', 'Discharge', 'Long Beach', '003', 'Muaz', '', '2018-09-23', 1),
(7, 'ANNA-THERESE', '13', '2025', 'Fishing vessel', '0', 'Discharge', 'Long Beach', '003', 'Muaz1', '', '2018-09-23', 1),
(8, 'j&j', '0', '2018', 'sdfafds', '455', 'Discharge', 'Chittagong', '001', 'My Vesse Folder', '', '2018-09-23', 1),
(9, 'j&j', '0', '2019', 'sdfafds', '455', 'Discharge', 'Chittagong', '002', 'MV Shindabad', '', '2018-09-23', 1),
(10, 'j&j', '0', '2018', 'sdfafds', '455', 'Discharge', 'Chittagong', '0023', 'MV Doe', '', '2018-09-25', 1),
(11, 'abc', '0', '2018', 'dsfasdf', '333', 'Discharge', 'Chittagong', '0125', 'HafijFolder', '', '2018-09-28', 1),
(12, 'MV Hanjala', '0', '20178', 'Merchant Vessel', '101', 'Discharge', 'Jedda', '0015', 'MVHanjalaRecord', '', '2018-10-05', 1),
(13, 'MV Hanjala', '0', '', 'Merchant Vessel', '101', 'Load', 'Jedda', '', 'Hanjala - Jed bl', '', '2018-10-16', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_ip`
--
ALTER TABLE `access_ip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calculator`
--
ALTER TABLE `calculator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_dropdown_data`
--
ALTER TABLE `custom_dropdown_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deduction_description`
--
ALTER TABLE `deduction_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `docs`
--
ALTER TABLE `docs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `endorsement`
--
ALTER TABLE `endorsement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `individual`
--
ALTER TABLE `individual`
  ADD PRIMARY KEY (`individual_id`);

--
-- Indexes for table `itinerary`
--
ALTER TABLE `itinerary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itineraryn`
--
ALTER TABLE `itineraryn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itinerary_events`
--
ALTER TABLE `itinerary_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itinery_repository`
--
ALTER TABLE `itinery_repository`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `port`
--
ALTER TABLE `port`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricing`
--
ALTER TABLE `pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `share_docs_user`
--
ALTER TABLE `share_docs_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ship`
--
ALTER TABLE `ship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ship_info`
--
ALTER TABLE `ship_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UC_users` (`email`);

--
-- Indexes for table `vessel`
--
ALTER TABLE `vessel`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_ip`
--
ALTER TABLE `access_ip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;

--
-- AUTO_INCREMENT for table `calculator`
--
ALTER TABLE `calculator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `custom_dropdown_data`
--
ALTER TABLE `custom_dropdown_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `deduction_description`
--
ALTER TABLE `deduction_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `docs`
--
ALTER TABLE `docs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `endorsement`
--
ALTER TABLE `endorsement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `individual`
--
ALTER TABLE `individual`
  MODIFY `individual_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `itinerary`
--
ALTER TABLE `itinerary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `itineraryn`
--
ALTER TABLE `itineraryn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `itinerary_events`
--
ALTER TABLE `itinerary_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `port`
--
ALTER TABLE `port`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pricing`
--
ALTER TABLE `pricing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `share_docs_user`
--
ALTER TABLE `share_docs_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `ship`
--
ALTER TABLE `ship`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `ship_info`
--
ALTER TABLE `ship_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `vessel`
--
ALTER TABLE `vessel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
