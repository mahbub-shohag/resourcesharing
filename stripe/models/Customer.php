<?php
  class Customer {
    private $db;

    public function __construct() {
      $this->db = new Database;
    }

    public function addCustomer($data) {
      // Prepare Query
      $this->db->query('INSERT INTO subscribers (subscription_id, user_id, month, subscription_fee, first_name, last_name, email) VALUES(:subscription_id, :user_id, :month, :subscription_fee, :first_name, :last_name, :email)');

      // Bind Values
      $this->db->bind(':subscription_id', $data['subscription_id']);
      $this->db->bind(':user_id', $data['user_id']);
      $this->db->bind(':month', $data['month']);
      $this->db->bind(':subscription_fee', $data['subscription_fee']);
      $this->db->bind(':first_name', $data['first_name']);
      $this->db->bind(':last_name', $data['last_name']);
      $this->db->bind(':email', $data['email']);

      // Execute
      if($this->db->execute()) {
        return true;
      } else {
        return false;
      }
    }

    public function getCustomers() {
      
      $this->db->query('SELECT * FROM subscribers ORDER BY created_at DESC');

      $results = $this->db->resultset();

      return $results;
    }
    // public function getUserEmail() {
    //   $this->db->query('SELECT * FROM pay_by_email');
      
    //   $results = $this->db->resultset();

    //   return $results;
    // }
  }