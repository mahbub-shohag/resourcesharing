var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);

app.controller('itinerary',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
  $scope.itns = [];
  $scope.itinerary = {};

  $scope.search = {};
  if(JSON.parse(sessionStorage.selectedRecord)){
      $('.dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
      $scope.selectedRecord = JSON.parse(sessionStorage.selectedRecord);
      $scope.itinerary.vessel_id = $scope.selectedRecord.vessel_id;
      console.log($scope.selectedRecord.vessel_id)
  }

  $scope.itineraryList = function(){
      $http({
            url: "/chanul/calculator/itinaryList",
            method: 'POST',
            data: $httpParamSerializerJQLike(),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.itns = response.data;
             console.log($scope.itns)
      });
  }
  $scope.itineraryList();
  $scope.saveItinerary = function() {
      console.log($scope.itinerary);
      $http({
            url: "/chanul/calculator/save",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'itinerary','data':$scope.itinerary}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.itns = response.data;
             console.log($scope.itineraries)
             if($scope.itns){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }



  $scope.deleteObject = function (obj, arrayName) {
    console.log(arrayName);
    var indexOfDeletedItem = $scope[arrayName].indexOf(obj);
    $scope[arrayName].splice(indexOfDeletedItem, 1); 
  }

     $scope.addObject = function(position, arrayName){
      console.log(position);
       //$scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appt-time:''};
       $scope.deduction = {};
       $scope[arrayName].push($scope.deduction); 
       console.log($scope[arrayName]);          
     }

    $scope.getList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship_info'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.itineraries = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    //$scope.getList();

    $scope.getPortList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'port'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ports = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    //$scope.getPortList();



    $scope.savePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/save",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

     $scope.addShip = function(){
      $scope.ship = {};
     }

    
    
     $scope.editPort = function(port){
        delete port.$$hashKey;
        $scope.port = port;
        $scope.port.call_sign = Number(port.call_sign);
     }


     $scope.updatePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/update",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

    $scope.deletePort = function(table,id){
      $http({
            url   :"/chanul/calculator/delete",
            method:"POST",
            data : $httpParamSerializerJQLike({table : table,id : id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.ports = response.data;
            if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
        });
    }





        $(function () {
          $('#est_arrival_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_arrival_time').on('change',function(){
          console.log($('#est_arrival_time').data('date'));
          $scope.itinerary.est_arrival_time = $('#est_arrival_time').data('date');
          console.log($scope.itinerary);
        })
    
      });

      $(function () {
          $('#itinerary_date').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#itinerary_date').on('change',function(){
            $scope.itinerary.itinerary_date = $('#itinerary_date').data('date');
          })
        });
      $(function () {
          $('#actual_arrival_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_arrival_time').on('change',function(){
            $scope.itinerary.actual_arrival_time = $('#actual_arrival_time').data('date');
          })
        });

      $(function () {
          $('#est_inspection_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_inspection_time').on('change',function(){
            $scope.itinerary.est_inspection_time = $('#est_inspection_time').data('date');
          })
        });
      $(function () {
          $('#actual_inspection_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_inspection_time').on('change',function(){
            $scope.itinerary.actual_inspection_time = $('#actual_inspection_time').data('date');
          })
        });
       $(function () {
          $('#est_bearthing_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_bearthing_time').on('change',function(){
            $scope.itinerary.est_bearthing_time = $('#est_bearthing_time').data('date');
          })
        });
        $(function () {
          $('#actual_bearthing_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_bearthing_time').on('change',function(){
            $scope.itinerary.actual_bearthing_time = $('#actual_bearthing_time').data('date');
          })
        });
         $(function () {
          $('#est_starting_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_starting_time').on('change',function(){
            $scope.itinerary.est_starting_time = $('#est_starting_time').data('date');
          })
        });
          $(function () {
          $('#actual_starting_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_starting_time').on('change',function(){
            $scope.itinerary.actual_starting_time = $('#actual_starting_time').data('date');
          })
        });
           $(function () {
          $('#est_completion_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_completion_time').on('change',function(){
            $scope.itinerary.est_completion_time = $('#est_completion_time').data('date');
          })
        });
            $(function () {
          $('#actual_completion_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_completion_time').on('change',function(){
            $scope.itinerary.actual_completion_time = $('#actual_completion_time').data('date');
          })
        });
        $(function () {
          $('#est_sailing_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_sailing_time').on('change',function(){
            $scope.itinerary.est_sailing_time = $('#est_sailing_time').data('date');
          })
        });
        $(function () {
          $('#actual_sailing_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_sailing_time').on('change',function(){
            $scope.itinerary.actual_sailing_time = $('#actual_sailing_time').data('date');
            console.log($scope.itinerary);
          })
        });
      











}])