var field_checked = false;
$(document).ready(function(){
	//debugger;
	$("#password_error_msg").hide();
	$("#retype_password_error_msg").hide();
	$("#email_error_message").hide();


	var error_password = false;
	var error_retype_password = false;

	$("#password1").focusout(function(){
		check_password();
	});

	$("#password2").focusout(function(){
		check_retype_password();
	});

	$("#email").focusout(function() {
		check_email();		
	});

	return field_checked;
});





	

	

	var check_password = function (){
		var password_length = $("#password1").val().length;

		if (password_length<8) {
			$("#password_error_msg").html("Password must be at least 8 characters.");
			$("#password_error_msg").show();
			error_password = true;
			field_checked = false;
		}else{
			$("#password_error_msg").hide();
			field_checked = true;

		};
	}


	var check_retype_password = function (){
		var password = $("#password1").val();
		var retype_password = $("#password2").val();

		if (password != retype_password) {
			$("#retype_password_error_msg").html("Password don't matched.");
			$("#retype_password_error_msg").show();
			error_retype_password = true;
			field_checked = false;
		}else{
			$("#retype_password_error_msg").hide();
			field_checked = true;
		};
	}


	var check_email = function () {

		var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
	
		if(pattern.test($("#email").val())) {
			$("#email_error_message").hide();
			field_checked = false;
		} else {
			$("#email_error_message").html("Invalid email address");
			$("#email_error_message").show();
			error_email = true;
			field_checked = true;
		}
	
	}


var check_form_validity = function(){
	return field_checked;
}