var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);
app.controller('calculator',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
	$scope.dropdown_for = "";
	$scope.test = 'Hello';
	$scope.ship = {};
	$scope.makeFile = function () {
		console.log($scope.ship);
	}

	/**/

	$scope.initialize = function(){
        $scope.header = "Income List";
        $scope.modalTitle = "Create Income";
        $scope.state      = "save";
        $scope.income = {};    
      }
    $scope.initialize();
    $scope.getShipList = function(){
        $http({
            url   :"calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ships = response.data;
            $scope.destinations = $scope.ships.filter(x => x.destination !== "");
            console.log($scope.destinations)
        });
    }
    $scope.getShipList();
    $scope.getCustomDropDownlists = function() {
    	$http({
            url   :"calculator/getCustomDropDownlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'custom_dropdown_data'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.allDropdowns = response.data;
            console.log($scope.allDropdowns)
        });
    }
    $scope.getCustomDropDownlists();
    $scope.saveActivity = function(dropdown_text) {
    	console.log(dropdown_text);
    	
    	 $http({
            url: "calculator/save_activity",
            method: 'POST',
            data: $httpParamSerializerJQLike({dropdown_text:dropdown_text, dropdown_for : $scope.dropdown_for}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);
                  if($scope.dropdown_for == 'activity'){
                  	$scope.allDropdowns = response.data;
                    $scope.ship.activity = response.data[0]; 
                    console.log($scope.ship.activity)	
                  }
                  
      });
    	console.log($scope.more_activity);
    }

    $scope.selectShipName = function() {
      console.log($scope.ship.ship_name);
      $scope.ship.loa = $scope.ship.ship_name.loa;
      $scope.ship.imo = $scope.ship.ship_name.imo;
      $scope.ship.dwt = $scope.ship.ship_name.dwt;
    }

    $scope.addNewDropdown = function(dropdown_for) {
		console.log(dropdown_for);
		if(dropdown_for == 'activity'){
			$scope.dropdown_for = dropdown_for;
			//$scope.dropdown_text = $scope.ship.activity;
		}
    }

    $scope.getAccountHeads = function(){
        $http({
          url :"server_side/Traveller/getList",
          method:"POST",
          data : $httpParamSerializerJQLike({table : 'accounts_head'}), 
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          }
      }).then(function(response){
        console.log(response);
        $scope.account_heads = response.data;
      });
    }

    $scope.getCustomerList = function(){
        $http({
            url   :"server_side/Traveller/getCustomerList",
            method:"GET" 
        }).then(function(response){
            console.log(response);
            $scope.customers = response.data;
        });
    }

    //$scope.getAccountHeads();
    //$scope.getCustomerList();
    $scope.create = function(){
      $scope.initialize();
    }
    
    $scope.save = function(){
      $("#spinner").append("<div id='loader'><div class='loader_inner'><span class='lodaing_sync'></span></div></div>");
                var url = 'server_side/Traveller/save';
        console.log($scope.income)
        $scope.income.account_head = $scope.income.acc_head.caption;
        delete $scope.income.acc_head;
        console.log($scope.income);
        if($scope.income.id){
          url = 'server_side/Traveller/update';
        }
        $http({
            url: url,
            method: 'POST',
            data: $httpParamSerializerJQLike({data:$scope.income,table:'income'}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);
                  $scope.incomes = response.data;
                  $("#spinner").children().remove();
      });
    }

    $scope.edit = function(income){
      console.log(income);
      $scope.income = income;
      var account_head = $filter('filter')($scope.account_heads, {caption:income.account_head});
      $scope.income.acc_head = account_head[0];
      console.log($scope.income.acc_head);
      $scope.income.amount = Number(income.amount);
      var date = new Date(income.date);
      $scope.income.date = date;
      console.log($scope.income)
      
    }
    $scope.delete = function(income){
        $http({
        url: 'server_side/Traveller/delete',
        method: 'POST',
        data: $httpParamSerializerJQLike({'id':income.id,'table':'income'}),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response) {
        console.log(response);
        $scope.incomes = response.data;
    });
    }
    /* Date Picker Code Starts */

    $scope.today = function() {
      //$scope.customer.dob = new Date();
    };
    $scope.today();
  
    $scope.clear = function() {
      $scope.dt = null;
    };
  
    $scope.inlineOptions = {
      customClass: getDayClass,
      minDate: new Date(),
      showWeeks: true
    };
  
    $scope.dateOptions = {
      dateDisabled: disabled,
      formatYear: 'yy',
      maxDate: new Date(2020, 5, 22),
      minDate: new Date(),
      startingDay: 1
    };
  
    // Disable weekend selection
    function disabled(data) {
      var date = data.date,
        mode = data.mode;
      return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }
  
    $scope.toggleMin = function() {
      $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
      $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };
  
    $scope.toggleMin();
  
    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };
  
    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };
  
    $scope.setDate = function(year, month, day) {
      $scope.dt = new Date(year, month, day);
    };
  
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];
  
    $scope.popup1 = {
      opened: false
    };
  
    $scope.popup2 = {
      opened: false
    };
  
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
      {
        date: tomorrow,
        status: 'full'
      },
      {
        date: afterTomorrow,
        status: 'partially'
      }
    ];
  
    function getDayClass(data) {
      var date = data.date,
        mode = data.mode;
      if (mode === 'day') {
        var dayToCheck = new Date(date).setHours(0,0,0,0);
  
        for (var i = 0; i < $scope.events.length; i++) {
          var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
  
          if (dayToCheck === currentDay) {
            return $scope.events[i].status;
          }
        }
      }
  
      return '';
    }
    /* Date Picker Code Ends */

    /* Dropdown Starts */

    var vm = this;

  vm.disabled = undefined;
  vm.searchEnabled = undefined;

  vm.setInputFocus = function (){
    $scope.$broadcast('UiSelectDemo1');
  };

  vm.enable = function() {
    vm.disabled = false;
  };

  vm.disable = function() {
    vm.disabled = true;
  };

  vm.enableSearch = function() {
    vm.searchEnabled = true;
  };

  vm.disableSearch = function() {
    vm.searchEnabled = false;
  };

  vm.clear = function() {
    vm.person.selected = undefined;
    vm.address.selected = undefined;
    vm.country.selected = undefined;
  };

  vm.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  vm.firstLetterGroupFn = function (item){
      return item.name[0];
  };

  vm.reverseOrderFilterFn = function(groups) {
    return groups.reverse();
  };


  vm.counter = 0;
  vm.onSelectCallback = function (item, model){
    vm.counter++;
    vm.eventResult = {item: item, model: model};
  };

  vm.removed = function (item, model) {
    vm.lastRemoved = {
        item: item,
        model: model
    };
  };

  vm.tagTransform = function (newTag) {
    var item = {
        name: newTag,
        email: newTag.toLowerCase()+'@email.com',
        age: 'unknown',
        country: 'unknown'
    };

    return item;
  };



  vm.appendToBodyDemo = {
    remainingToggleTime: 0,
    present: true,
    startToggleTimer: function() {
      var scope = vm.appendToBodyDemo;
      var promise = $interval(function() {
        if (scope.remainingTime < 1000) {
          $interval.cancel(promise);
          scope.present = !scope.present;
          scope.remainingTime = 0;
        } else {
          scope.remainingTime -= 1000;
        }
      }, 1000);
      scope.remainingTime = 3000;
    }
  };

  vm.address = {};
  vm.refreshAddresses = function(address) {
    var params = {address: address, sensor: false};
    return $http.get(
      'http://maps.googleapis.com/maps/api/geocode/json',
      {params: params}
    ).then(function(response) {
      vm.addresses = response.data.results;
    });
  };

  vm.addPerson = function(item, model){
    if(item.hasOwnProperty('isTag')) {
      delete item.isTag;
      vm.people.push(item);
    }
  }




    /* Dropdown Ends*/



	/**/


}])