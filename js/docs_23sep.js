var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);

app.controller('docsController',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
	$scope.name = "Mahbub Alam";
    $scope.current_directory_link = "itinery";
	$scope.current_directory = "itinery";
    $scope.current_folder = {'id':1,'parent_id':0,'name':'docs'};
    $scope.directory_queue = [$scope.current_folder];
	$scope.directory = "default_directory";
	$scope.parent_id = 1;
    $scope.image_files = [];
    $scope.others = [];
    $scope.items = [];
    $scope.pdfs = [];
    $scope.folder_path = "";
	$scope.initialize = function(){
		$scope.files = [];
	}
	$scope.initialize();

    $('#image').change(function() {
      console.log('********')  
      $scope.uploadFile();
    });
    $scope.selected_directory = function(){
        $scope.folder_path = "";
        angular.forEach($scope.directory_queue, function(value, key){
           $scope.folder_path = $scope.folder_path + value.name + "/";
        });
        console.log($scope.directory_queue);
    }
    $scope.distributeItems = function(){
        $scope.folders = $filter('filter')($scope.items,{'type_name':'folder'},true);
        $scope.jpgs = $filter('filter')($scope.items,{'type_name':'jpg'},true);
        $scope.jpegs = $filter('filter')($scope.items,{'type_name':'jpeg'},true);
        $scope.pngs = $filter('filter')($scope.items,{'type_name':'png'},true);
        $scope.image_files.push.apply($scope.image_files,$scope.jpgs);
        $scope.image_files.push.apply($scope.image_files,$scope.jpegs);
        $scope.image_files.push.apply($scope.image_files,$scope.pngs);
        $scope.pdfs = $filter('filter')($scope.items,{'type_name':'pdf'},true);
        $scope.others = $scope.items.filter( function( el ) {
          return $scope.folders.indexOf( el ) < 0;
        } );
        $scope.others = $scope.others.filter(function(el){
            return $scope.image_files.indexOf(el)<0;
        });
        $scope.others = $scope.others.filter(function(el){
            return $scope.pdfs.indexOf(el)<0;
        })
        console.log($scope.pdfs);
        console.log($scope.others,'**********');
    }
    $scope.clearItems = function(){
        $scope.folders = [];
        $scope.jpegs = [];
        $scope.jpegs = [];
        $scope.pngs = [];
        $scope.pdfs = [];
        $scope.image_files = [];
    }
	$scope.getRootFiles = function(){
        $http({
            url   :server+"docs/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({'parent_id' : $scope.parent_id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.items = response.data;
            $scope.clearItems();
            $scope.distributeItems();
        });
    }
    $scope.getRootFiles();

    $scope.makeDirectory = function(){
    	$scope.selected_directory();
        var folder_path = $scope.current_directory+"/"+$scope.directory;
    	console.log(folder_path);
        $scope.folder_path = $scope.folder_path+$scope.directory+"/";
        var data = {'folder_path' : $scope.folder_path,'directory_name':$scope.directory,'parent_id':$scope.parent_id};
    	$http({
            url   :server+"docs/makeDirectory",
            method:"POST",
            data : $httpParamSerializerJQLike(data), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            $scope.items = response.data;
            $scope.clearItems();
            $scope.distributeItems();
        });

    }

    $scope.uploadFile = function(){
    	var info = {};
    	info.parent_id = $scope.parent_id;
        $scope.selected_directory();
        info.directory = $scope.folder_path;
        console.log(info.directory,'***');
    	var fd = new FormData();
        var files = document.getElementById('image').files[0];
		fd.append('file',files);
		fd.append('info',JSON.stringify(info));
		$http({
            url   :server+"docs/uploadFile",
            method:"POST",
            data : fd, 
            headers: {
                'Content-Type': undefined
            }
        }).then(function(response){
            console.log(response);
            $scope.items = response.data;
            $scope.clearItems();
            $scope.distributeItems();
        });
    }

    $scope.findChilds = function(root){
            console.log(root);
            console.log($scope.current_folder.id);
            if(root.id === $scope.current_folder.id){
                if(root.id===1){
                    $scope.current_folder = root;    
                    $scope.parent_id = root.id;
                    console.log($scope.parent_id);
                    $scope.getRootFiles();    
                }
            }else{
                console.log($scope.current_folder,'******')
                $scope.current_folder = root;    
                $scope.parent_id = root.id;
                $scope.current_directory = root.name;
                var index = $scope.directory_queue.indexOf(root);
                var queue_length = $scope.directory_queue.length;

                if(index!=-1){
                    $scope.directory_queue.splice(index+1,queue_length-1);    
                }else{
                    $scope.directory_queue.push(root);
                    console.log($scope.directory_queue);
                }
                console.log(index);
                $scope.clearItems();
                $scope.getRootFiles();
            }
        
    }

}])