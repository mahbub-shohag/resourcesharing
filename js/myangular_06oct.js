var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);

app.controller('itinerary',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
  $scope.itineraries = [];
  $scope.itinerary = {};
  $scope.search = {};
  if(JSON.parse(sessionStorage.selectedRecord)){
      $('#dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
      $scope.selectedRecord = JSON.parse(sessionStorage.selectedRecord);
      console.log($scope.selectedRecord.vessel_id)
  }
  $scope.deleteObject = function (obj, arrayName) {
    console.log(arrayName);
    var indexOfDeletedItem = $scope[arrayName].indexOf(obj);
    $scope[arrayName].splice(indexOfDeletedItem, 1); 
  }

     $scope.addObject = function(position, arrayName){
      console.log(position);
       //$scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appt-time:''};
       $scope.deduction = {};
       $scope[arrayName].push($scope.deduction); 
       console.log($scope[arrayName]);          
     }

    $scope.getList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship_info'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.itineraries = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    $scope.getList();

    $scope.getPortList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'port'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ports = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    $scope.getPortList();



    $scope.savePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/save",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

     $scope.addShip = function(){
      $scope.ship = {};
     }

    
    
     $scope.editPort = function(port){
        delete port.$$hashKey;
        $scope.port = port;
        $scope.port.call_sign = Number(port.call_sign);
     }


     $scope.updatePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/update",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

    $scope.deletePort = function(table,id){
      $http({
            url   :"/chanul/calculator/delete",
            method:"POST",
            data : $httpParamSerializerJQLike({table : table,id : id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.ports = response.data;
            if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
        });
    }

}])













app.controller('calculatorEdit',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
  
  console.log('hello');
  $scope.dropdown_for = "";
  $scope.test = 'Hello';
  $scope.total_itinary = 0;
  $scope.ship = {};
  $scope.ships = [];
  $scope.deductions = [];
  $scope.activities = [];
  $scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appttime:'2018-07-22'};
  $scope.deductions.push($scope.deduction);
  $scope.deduction_descriptions = [];
  $scope.directory = 'itinery';
  if(JSON.parse(sessionStorage.selectedRecord)){
      $('#dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
  }

  $scope.saveShipInfo = function(){
    console.log($scope.ship);
    console.log($scope.directory);
    //$scope.directoryName = $scope.directory;
    $scope.ship.directoryName = $scope.directory;
    $scope.ship.fileName = $scope.fileName;
    console.log($scope.directory);
       $http({
            url: "savepdf",
            method: 'POST',
            data: $httpParamSerializerJQLike({info : $scope.ship}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response.data);
                  $scope.shipinfoId = response.data;
                  if($scope.shipinfoId){
                    redirect('/chanul/calculator/itineraries');
                  }                  
      });
  }

  $scope.makePdfFile = function () {
    console.log($scope.ship);
       $http({
            url: "infopdf",
            method: 'POST',
            data: $httpParamSerializerJQLike({info : $scope.ship,directoryName : $scope.directoryName}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);                  
      });
  }
  $scope.find = function(item){
    $scope.table_cargo = item;
    console.log(item);
    console.log($scope.ship.cargo)
  }
  /**/
    $scope.findactivity = function(){
      console.log($scope.ship.activity)
    }

    $scope.deleteObject = function (obj, arrayName) {
      console.log(arrayName);
      var indexOfDeletedItem = $scope[arrayName].indexOf(obj);
      $scope[arrayName].splice(indexOfDeletedItem, 1); 
    }


     $scope.addObject = function(position, arrayName){
      console.log(position);
       //$scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appt-time:''};
       $scope.deduction = {};
       $scope[arrayName].push($scope.deduction); 
       console.log($scope[arrayName]);          
     }
    
    $scope.selectDirectory = function(){
        console.log($scope.fileName);
        $scope.directoryName = $scope.directory;
        $scope.saveShipInfo();
    }
    $scope.menus = function(){
        $http({
        url   :"getlists",
        method:"POST",
        data : $httpParamSerializerJQLike({table : 'itinery_repository'}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        $scope.itinery_repositories = response.data;
        console.log($scope.itinery_repositories,'repositories');
    });
    }
    $scope.menus();
    $scope.allDropDownList = function(){
        $http({
        url   :"getlists",
        method:"POST",
        data : $httpParamSerializerJQLike({table : 'custom_dropdown_data'}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        console.log(response);
        var dropdowns = response.data;
        $scope.cargoes = $filter('filter')(dropdowns, {dropdown_for: 'cargo'}, true);
        console.log($scope.cargoes);
        $scope.activities = $filter('filter')(dropdowns, {dropdown_for: 'activity'}, true);
        console.log($scope.activities);
    });
    }
    $scope.allDropDownList();
    $scope.getDeductionDescriptionList = function(){
    $http({
        url   :"getlists",
        method:"POST",
        data : $httpParamSerializerJQLike({table : 'deduction_description'}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        console.log(response);
        $scope.deduction_descriptions = response.data;
        console.log($scope.deduction_descriptions)
    });
    }
    $scope.getDeductionDescriptionList();

    $scope.getShipList = function(){
        $http({
            url   :"getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ships = response.data;
            $scope.destinations = $scope.ships.filter(x => x.destination !== "");
        });
    }
    $scope.getShipList();


    $scope.populateObject = function(){
    var url_string = window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get("id");
     $http({
            url: "get_by_id",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table' : 'ship_info', 'id': id}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response.data[0]);
                  $scope.ship = response.data[0];
                  console.log($scope.ship);
                  $scope.ship.ship_name = $scope.ship.ship_name;

      });
 }
 $scope.populateObject();

    $scope.getActivityDropDownlists = function() {
      $http({
            url   :"getCustomDropDownlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'custom_dropdown_data'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.activities = response.data;
            console.log($scope.activities)
        });
    }
    //$scope.getActivityDropDownlists();
    $scope.saveActivity = function(dropdown_text) {
      console.log(dropdown_text);
      
       $http({
            url: "save_activity",
            method: 'POST',
            data: $httpParamSerializerJQLike({dropdown_text:dropdown_text, dropdown_for : $scope.dropdown_for}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);
                  if($scope.dropdown_for == 'activity'){
                    $scope.activities = response.data;  
                    $scope.ship.activity = response.data[0];
                  }
                  
      });
      console.log($scope.activities);
    }

    $scope.save_dropdown = function(dropdown_text) {
      console.log(dropdown_text);
      
       $http({
            url: "save_dropdown",
            method: 'POST',
            data: $httpParamSerializerJQLike({dropdown_text:dropdown_text, dropdown_for : $scope.dropdown_for}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);
                  if($scope.dropdown_for == 'activity'){
                    $scope.activities = response.data;  
                    $scope.ship.activity = response.data[0];
                  }else if($scope.dropdown_for == 'cargo'){
                    $scope.cargoes = response.data;
                  }else{

                  }
                  
      });
      console.log($scope.cargoes);
    }

    $scope.selectShipName = function() {
      console.log($scope.ship.ship_name);
      $scope.ship.loa = $scope.ship.ship_name.loa;
      $scope.ship.imo = $scope.ship.ship_name.imo;
      $scope.ship.dwt = $scope.ship.ship_name.dwt;
    }

    $scope.addNewDropdown = function(dropdown_for) {
      $scope.dropdown_for = dropdown_for;
      console.log($scope.dropdown_for);
    }

    /* Dropdown Starts */

    var vm = this;

  vm.disabled = undefined;
  vm.searchEnabled = undefined;

  vm.setInputFocus = function (){
    $scope.$broadcast('UiSelectDemo1');
  };

  vm.enable = function() {
    vm.disabled = false;
  };

  vm.disable = function() {
    vm.disabled = true;
  };

  vm.enableSearch = function() {
    vm.searchEnabled = true;
  };

  vm.disableSearch = function() {
    vm.searchEnabled = false;
  };

  vm.clear = function() {
    vm.person.selected = undefined;
    vm.address.selected = undefined;
    vm.country.selected = undefined;
  };

  vm.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  vm.firstLetterGroupFn = function (item){
      return item.name[0];
  };

  vm.reverseOrderFilterFn = function(groups) {
    return groups.reverse();
  };


  vm.counter = 0;
  vm.onSelectCallback = function (item, model){
    vm.counter++;
    vm.eventResult = {item: item, model: model};
  };

  vm.removed = function (item, model) {
    vm.lastRemoved = {
        item: item,
        model: model
    };
  };

  vm.tagTransform = function (newTag) {
    var item = {
        name: newTag,
        email: newTag.toLowerCase()+'@email.com',
        age: 'unknown',
        country: 'unknown'
    };

    return item;
  };



  vm.appendToBodyDemo = {
    remainingToggleTime: 0,
    present: true,
    startToggleTimer: function() {
      var scope = vm.appendToBodyDemo;
      var promise = $interval(function() {
        if (scope.remainingTime < 1000) {
          $interval.cancel(promise);
          scope.present = !scope.present;
          scope.remainingTime = 0;
        } else {
          scope.remainingTime -= 1000;
        }
      }, 1000);
      scope.remainingTime = 3000;
    }
  };

  vm.address = {};
  vm.refreshAddresses = function(address) {
    var params = {address: address, sensor: false};
    return $http.get(
      'http://maps.googleapis.com/maps/api/geocode/json',
      {params: params}
    ).then(function(response) {
      vm.addresses = response.data.results;
    });
  };

  vm.addPerson = function(item, model){
    if(item.hasOwnProperty('isTag')) {
      delete item.isTag;
      vm.people.push(item);
    }
  }
    /* Dropdown Ends*/



  /**/

}])



















app.controller('calculator',["$scope","$rootScope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope,$rootScope, $http,$timeout,$filter, $httpParamSerializerJQLike){
	console.log('hello');
  $scope.dropdown_for = "";
	$scope.test = 'Hello';
	$scope.ship = {};
  $scope.deductions = [];
  $scope.activities = [];
  $scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appttime:'2018-07-22'};
  $scope.deductions.push($scope.deduction);
  $scope.deduction_descriptions = [];
  $scope.directory = '';
  if(JSON.parse(sessionStorage.selectedRecord)){
      $('#dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
      var selectedRecord = JSON.parse(sessionStorage.selectedRecord);
      $scope.directory = selectedRecord.vessel_id;
  }
  console.log($scope.directory);
  $scope.saveShipInfo = function(){
    console.log($scope.ship);
    console.log($scope.directory);
    //$scope.directoryName = $scope.directory;
    $scope.ship.directoryName = $scope.directory;
    $scope.ship.fileName = $scope.fileName;
    console.log($scope.directory);
       $http({
            url: "savepdf",
            method: 'POST',
            data: $httpParamSerializerJQLike({info : $scope.ship}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response.data);
                  $scope.shipinfoId = response.data;
                  if($scope.shipinfoId){
                    redirect('/chanul/calculator/itineraries');
                  }                  
      });
  }

	$scope.makePdfFile = function () {
		console.log($scope.ship);
       $http({
            url: "infopdf",
            method: 'POST',
            data: $httpParamSerializerJQLike({info : $scope.ship,directoryName : $scope.directoryName}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);                  
      });
	}
  $scope.find = function(item){
    $scope.table_cargo = item;
    console.log(item);
    console.log($scope.ship.cargo)
  }
	/**/
    $scope.findactivity = function(){
      console.log($scope.ship.activity)
    }

    $scope.deleteObject = function (obj, arrayName) {
      console.log(arrayName);
      var indexOfDeletedItem = $scope[arrayName].indexOf(obj);
      $scope[arrayName].splice(indexOfDeletedItem, 1); 
    }


     $scope.addObject = function(position, arrayName){
      console.log(position);
       //$scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appt-time:''};
       $scope.deduction = {};
       $scope[arrayName].push($scope.deduction); 
       console.log($scope[arrayName]);          
     }
    
    $scope.selectDirectory = function(){
        console.log($scope.fileName);
        $scope.directoryName = $scope.directory;
        $scope.saveShipInfo();
    }
    $scope.menus = function(){
        $http({
        url   :"getlists",
        method:"POST",
        data : $httpParamSerializerJQLike({table : 'itinery_repository'}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        $scope.itinery_repositories = response.data;
        console.log($scope.itinery_repositories,'repositories');
    });
    }
    $scope.menus();
    $scope.allDropDownList = function(){
        $http({
        url   :"getlists",
        method:"POST",
        data : $httpParamSerializerJQLike({table : 'custom_dropdown_data'}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        console.log(response);
        var dropdowns = response.data;
        $scope.cargoes = $filter('filter')(dropdowns, {dropdown_for: 'cargo'}, true);
        console.log($scope.cargoes);
        $scope.activities = $filter('filter')(dropdowns, {dropdown_for: 'activity'}, true);
        console.log($scope.activities);
    });
    }
    $scope.allDropDownList();
    $scope.getDeductionDescriptionList = function(){
    $http({
        url   :"getlists",
        method:"POST",
        data : $httpParamSerializerJQLike({table : 'deduction_description'}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        console.log(response);
        $scope.deduction_descriptions = response.data;
        console.log($scope.deduction_descriptions)
    });
    }
    $scope.getDeductionDescriptionList();

    $scope.getShipList = function(){
        $http({
            url   :"getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ships = response.data;
            $scope.destinations = $scope.ships.filter(x => x.destination !== "");
            console.log($scope.destinations)
        });
    }
    $scope.getShipList();

    $scope.getPortList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'port'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data,'**********');
            $scope.ports = response.data;

            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    $scope.getPortList();

    $scope.getActivityDropDownlists = function() {
    	$http({
            url   :"getCustomDropDownlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'custom_dropdown_data'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.activities = response.data;
            console.log($scope.activities)
        });
    }
    //$scope.getActivityDropDownlists();
    $scope.saveActivity = function(dropdown_text) {
    	console.log(dropdown_text);
    	
    	 $http({
            url: "save_activity",
            method: 'POST',
            data: $httpParamSerializerJQLike({dropdown_text:dropdown_text, dropdown_for : $scope.dropdown_for}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);
                  if($scope.dropdown_for == 'activity'){
                  	$scope.activities = response.data;	
                    $scope.ship.activity = response.data[0];
                  }
                  
      });
    	console.log($scope.activities);
    }

    $scope.save_dropdown = function(dropdown_text) {
      console.log(dropdown_text);
      
       $http({
            url: "save_dropdown",
            method: 'POST',
            data: $httpParamSerializerJQLike({dropdown_text:dropdown_text, dropdown_for : $scope.dropdown_for}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);
                  if($scope.dropdown_for == 'activity'){
                    $scope.activities = response.data;  
                    $scope.ship.activity = response.data[0];
                  }else if($scope.dropdown_for == 'cargo'){
                    $scope.cargoes = response.data;
                  }else{

                  }
                  
      });
      console.log($scope.cargoes);
    }

    $scope.selectShipName = function() {
      console.log($scope.ship.ship_name);
      $scope.ship.loa = $scope.ship.ship_name.loa;
      $scope.ship.imo = $scope.ship.ship_name.imo;
      $scope.ship.dwt = $scope.ship.ship_name.dwt;
    }

    $scope.addNewDropdown = function(dropdown_for) {
			$scope.dropdown_for = dropdown_for;
      console.log($scope.dropdown_for);
    }

    /* Dropdown Starts */

    var vm = this;

  vm.disabled = undefined;
  vm.searchEnabled = undefined;

  vm.setInputFocus = function (){
    $scope.$broadcast('UiSelectDemo1');
  };

  vm.enable = function() {
    vm.disabled = false;
  };

  vm.disable = function() {
    vm.disabled = true;
  };

  vm.enableSearch = function() {
    vm.searchEnabled = true;
  };

  vm.disableSearch = function() {
    vm.searchEnabled = false;
  };

  vm.clear = function() {
    vm.person.selected = undefined;
    vm.address.selected = undefined;
    vm.country.selected = undefined;
  };

  vm.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  vm.firstLetterGroupFn = function (item){
      return item.name[0];
  };

  vm.reverseOrderFilterFn = function(groups) {
    return groups.reverse();
  };


  vm.counter = 0;
  vm.onSelectCallback = function (item, model){
    vm.counter++;
    vm.eventResult = {item: item, model: model};
  };

  vm.removed = function (item, model) {
    vm.lastRemoved = {
        item: item,
        model: model
    };
  };

  vm.tagTransform = function (newTag) {
    var item = {
        name: newTag,
        email: newTag.toLowerCase()+'@email.com',
        age: 'unknown',
        country: 'unknown'
    };

    return item;
  };



  vm.appendToBodyDemo = {
    remainingToggleTime: 0,
    present: true,
    startToggleTimer: function() {
      var scope = vm.appendToBodyDemo;
      var promise = $interval(function() {
        if (scope.remainingTime < 1000) {
          $interval.cancel(promise);
          scope.present = !scope.present;
          scope.remainingTime = 0;
        } else {
          scope.remainingTime -= 1000;
        }
      }, 1000);
      scope.remainingTime = 3000;
    }
  };

  vm.address = {};
  vm.refreshAddresses = function(address) {
    var params = {address: address, sensor: false};
    return $http.get(
      'http://maps.googleapis.com/maps/api/geocode/json',
      {params: params}
    ).then(function(response) {
      vm.addresses = response.data.results;
    });
  };

  vm.addPerson = function(item, model){
    if(item.hasOwnProperty('isTag')) {
      delete item.isTag;
      vm.people.push(item);
    }
  }




    /* Dropdown Ends*/



	/**/


}])







app.controller('dashboard',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
  console.log('Dashboard Controller');
  $scope.ships = [];
  $scope.ship = {};
  $scope.search = {};
  $scope.searchPort = {};
  $scope.ports = [];
  $scope.port = {};
  $scope.vessels = [];
  $scope.vessel = {};
  $scope.total_docs = 0;
  if(sessionStorage.selectedRecord){
      $('#dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
  }


  $scope.find = function(item){
    $scope.table_cargo = item;
    console.log(item);
    console.log($scope.ship.cargo)
  }

  $scope.activities = [{'name':"Load"},{'name':'Discharge'},{'name':'Bunkering'}];
  /**/

    $scope.selectShipName = function() {
      console.log($scope.vessel.vessel);
      $scope.vessel.loa = $scope.vessel.vessel.loa;
      $scope.vessel.imo = $scope.vessel.vessel.imo;
      $scope.vessel.vessel_type = $scope.vessel.vessel.ship_type;
      $scope.vessel.year = $scope.vessel.vessel.year;
    }
    
    $scope.getDocsByVesselId = function(id){
      $http({
            url   :server+"docs/get_by_vessel_id",
            method:"POST",
            data : $httpParamSerializerJQLike({'table':'docs','id':id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            sessionStorage.setItem('selectedVessel',JSON.stringify(response.data[0]));
        });
    }

    $scope.selectRecord = function(selected_vessel){
      console.log(selected_vessel);
      $('.dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      //$rootScope.selectedRecord = selected_vessel;
      sessionStorage.setItem('selectedRecord',JSON.stringify(selected_vessel));
      console.log(JSON.parse(sessionStorage.selectedRecord));
      $scope.getDocsByVesselId(selected_vessel.id);

    }

    

     $scope.makeVessel = function(){
        var folder_path = "docs/"+$scope.directory;
        console.log(folder_path);
        $scope.folder_path = $scope.folder_path+$scope.directory+"/";
        var data = {'folder_path' : $scope.folder_path,'directory_name':$scope.directory,'parent_id':$scope.parent_id, 'vessel_id':$scope.vessel_id};
        $http({
            url   :server+"docs/makeDirectory",
            method:"POST",
            data : $httpParamSerializerJQLike(data), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            $scope.items = response.data;
        });

    }

    $scope.saveVessel = function(){
        console.log($scope.vessel);
        $scope.directory = $scope.vessel.vessel_id;
        $scope.parent_id = 1;
        $data = {'vessel':$scope.vessel.vessel.name,'loa':$scope.vessel.loa,'year':$scope.vessel.year,'vessel_type':$scope.vessel.vessel_type,'imo':$scope.vessel.imo,'activity':$scope.vessel.activity.name,'port':$scope.vessel.port.name,'code':$scope.vessel.code,'vessel_id':$scope.vessel.vessel_id,'created_date':new Date(),'is_active':1}
      $http({
            url: "/chanul/calculator/save",
            method: 'POST',
            data: $httpParamSerializerJQLike({'data':$data,'table':'vessel'}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.vessels = response.data;
             $scope.vessel_id = $scope.vessels[0].id;
             console.log($scope.vessels);
             console.log($scope.vessel_id.id)
             $scope.makeVessel();

             if($scope.vessels){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }
    $scope.getVesselList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'vessel'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.vessels = response.data;
        });
    }
    $scope.getVesselList();

    $scope.findactivity = function(){
      console.log($scope.ship.activity)
    }

    $scope.deleteObject = function (obj, arrayName) {
      console.log(arrayName);
      var indexOfDeletedItem = $scope[arrayName].indexOf(obj);
      $scope[arrayName].splice(indexOfDeletedItem, 1); 
    }

     $scope.addObject = function(position, arrayName){
      console.log(position);
       //$scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appt-time:''};
       $scope.deduction = {};
       $scope[arrayName].push($scope.deduction); 
       console.log($scope[arrayName]);          
     }

    $scope.getShipList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ships = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    $scope.getShipList();

    $scope.getPortList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'port'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ports = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    $scope.getPortList();

    $scope.getAllItinaryById = function(){
    $http({
            url   :"/chanul/calculator/getAllItinaryById",
            method:"POST",
            data : $httpParamSerializerJQLike({'created_by' : 1}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.total_itinary = response.data;
            console.log($scope.total_itinary,'**********');
        });
 }
 $scope.getAllItinaryById();

 $scope.getAllDocsById = function(){
    $http({
            url   :"/chanul/calculator/getAllDocsById",
            method:"POST",
            data : $httpParamSerializerJQLike({'created_by' : 1}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.total_docs = response.data;
            console.log($scope.total_docs,'**********');
        });
 }
 $scope.getAllDocsById();

    $scope.saveShip = function() {
      console.log($scope.ship);
      $http({
            url: "/chanul/calculator/saveShip",
            method: 'POST',
            data: $httpParamSerializerJQLike($scope.ship),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ships = response.data;
             if($scope.ships){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }


    $scope.savePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/save",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

     $scope.addShip = function(){
      $scope.ship = {};
     }

     $scope.addPort = function(){
      $scope.port = {};
     }

     $scope.editShip = function(ship){
        delete ship.$$hashKey;
        $scope.ship = ship;
        $scope.ship.imo = Number(ship.imo);
        console.log(ship);
     }

     $scope.editPort = function(port){
        delete port.$$hashKey;
        $scope.port = port;
        $scope.port.call_sign = Number(port.call_sign);
     }

     $scope.updateShip = function() {
      console.log($scope.ship);
      $http({
            url: "/chanul/calculator/updateShip",
            method: 'POST',
            data: $httpParamSerializerJQLike($scope.ship),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ships = response.data;
             if($scope.ships){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

     $scope.updatePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/update",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }


    $scope.delete = function(table,id){
      $http({
            url   :"/chanul/calculator/delete",
            method:"POST",
            data : $httpParamSerializerJQLike({table : table,id : id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.ships = response.data;
            if($scope.ships){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
        });
    }

    $scope.deletePort = function(table,id){
      $http({
            url   :"/chanul/calculator/delete",
            method:"POST",
            data : $httpParamSerializerJQLike({table : table,id : id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.ports = response.data;
            if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
        });
    }

   

}])