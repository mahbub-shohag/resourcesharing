var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);

app.controller('itinerary',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
  $scope.itns = [];
  $scope.itn = {};
  $scope.itn_basic = {};
  //$scope.event = {'event_name':'event1','estimate_date':'','actual_date':''};

  $scope.events = [];
  $scope.itinerary = {};
  $scope.itinerary.cargo = '';
  $scope.search = {};
  if(JSON.parse(sessionStorage.selectedRecord)){
      $('.dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
      $scope.selectedRecord = JSON.parse(sessionStorage.selectedRecord);
      $scope.itinerary.vessel_id = $scope.selectedRecord.vessel_id;
      $scope.itinerary.vessel = $scope.selectedRecord;
      console.log($scope.selectedRecord,'******vesel*****')
  }

   


  $scope.getLastItinerary = function(){
    $http({
            url: "/chanul/calculator/getLastItinerary",
            method: 'POST',
            data: $httpParamSerializerJQLike({'vesselId':$scope.itinerary.vessel.id}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.itinerary_info = response.data;
             $scope.itinerary   = $scope.itinerary_info.itinerary_n;
             console.log($scope.itinerary);
             if($scope.itinerary==undefined){
                 $scope.itinerary = {};
                 $scope.itinerary.vessel = $scope.selectedRecord;
             }else{
                 $scope.itinerary.vessel = {'vessel_id':$scope.itinerary.vessel_folder,'vessel':$scope.itinerary.vessel,'port':$scope.itinerary.port,'id':$scope.itinerary.vesselId};
             } 
             
             $scope.itn_basic = $scope.itinerary_info.itinerary;
             console.log($scope.itn_basic,'****');
             $scope.events = $scope.itinerary_info.events;
              angular.forEach($scope.events,function(key,val){
              key.actual_date = key.actual_date;
              $scope.datepickerOptions = {
              minDate: new Date(key.actual_date),
              initDate: new Date(key.actual_date)
              };
          })
             console.log($scope.itinerary_info);
      });

         
  }
  $scope.getLastItinerary();

  $scope.getItineraryById = function(id){
    $http({
            url: "/chanul/calculator/getItineraryById/"+id,
            method: 'POST',
            data: $httpParamSerializerJQLike({'id':id},{'vesselId':$scope.itinerary.vessel.id}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.itinerary_info = response.data;
             console.log($scope.itinerary_info)
             $scope.itinerary   = $scope.itinerary_info.itinerary_n;
             console.log($scope.itinerary);
             $scope.itinerary.vessel = {'vessel_id':$scope.itinerary.vessel_folder,'vessel':$scope.itinerary.vessel,'port':$scope.itinerary.port,'id':$scope.itinerary.vesslId};
             $scope.itn_basic = $scope.itinerary_info.itinerary;
             console.log($scope.itn_basic,'****');
              angular.forEach($scope.events,function(key,val){
              key.actual_date = key.actual_date;
              $scope.datepickerOptions = {
              minDate: new Date(key.actual_date),
              initDate: new Date(key.actual_date)
              };
          })
             console.log($scope.itinerary_info);
      });

         
  }

  $scope.selectOldRecord = function(record) {
    console.log(record);
    $scope.getItineraryById(record.id);

  }

  $scope.itineraryList = function(){
      $http({
            url: "/chanul/calculator/getItinerarylistById",
            method: 'POST',
            data: $httpParamSerializerJQLike({'vesselId':$scope.itinerary.vessel.id}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.itns = response.data;
             console.log($scope.itns);
      });
  }
  $scope.itineraryList();
  $scope.deleteObject = function (obj, arrayName) {
    console.log($scope[arrayName]);
    var indexOfDeletedItem = $scope[arrayName].indexOf(obj);
    $scope[arrayName].splice(indexOfDeletedItem, 1); 
  }

     $scope.addObject = function(position, arrayName){
       console.log(position);
       $scope.event = {'event_name':'','estimate_date':'','actual_date':''};
       $scope[arrayName].push($scope.event); 
       console.log($scope[arrayName]);          
     }
     $scope.addObjectn = function(arrayName){
       
       $scope.event = {'event_name':'','estimate_date':'','actual_date':''};
       $scope[arrayName].push($scope.event);
       //$scope[arrayName].push($scope.event); 
       console.log($scope[arrayName]);          
     }

  $scope.saveItinerary = function() {
      console.log($scope.events);
      $scope.itinerary.events = $scope.events;
      console.log($scope.itinerary);
      $http({
            url: "/chanul/calculator/itnsave",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'itinerary','data':$scope.itinerary}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.itns = response.data;
             console.log($scope.itineraries)
             if($scope.itns){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }



    $scope.getList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship_info'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.itineraries = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    //$scope.getList();

    $scope.getPortList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'port'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ports = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    //$scope.getPortList();



    $scope.savePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/save",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

     $scope.addShip = function(){
      $scope.ship = {};
     }

    
    
     $scope.editPort = function(port){
        delete port.$$hashKey;
        $scope.port = port;
        $scope.port.call_sign = Number(port.call_sign);
     }


     $scope.updatePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/update",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

    $scope.deletePort = function(table,id){
      $http({
            url   :"/chanul/calculator/delete",
            method:"POST",
            data : $httpParamSerializerJQLike({table : table,id : id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.ports = response.data;
            if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
        });
    }





        $(function () {
          $('#est_arrival_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_arrival_time').on('change',function(){
          console.log($('#est_arrival_time').data('date'));
          $scope.itinerary.est_arrival_time = $('#est_arrival_time').data('date');
          console.log($scope.itinerary);
        })
    
      });

      $(function () {
          $('#itinerary_date').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#itinerary_date').on('change',function(){
            $scope.itinerary.itinerary_date = $('#itinerary_date').data('date');
          })
        });
      $(function () {
          $('#actual_arrival_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_arrival_time').on('change',function(){
            $scope.itinerary.actual_arrival_time = $('#actual_arrival_time').data('date');
          })
        });

       $(function () {
          $('#est_inspection_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_inspection_time').on('change',function(){
            $scope.itinerary.est_inspection_time = $('#est_inspection_time').data('date');
          })
        });
      $(function () {
          $('#actual_inspection_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_inspection_time').on('change',function(){
            $scope.itinerary.actual_inspection_time = $('#actual_inspection_time').data('date');
          })
        });
       $(function () {
          $('#est_bearthing_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_bearthing_time').on('change',function(){
            $scope.itinerary.est_bearthing_time = $('#est_bearthing_time').data('date');
          })
        });
        $(function () {
          $('#actual_bearthing_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_bearthing_time').on('change',function(){
            $scope.itinerary.actual_bearthing_time = $('#actual_bearthing_time').data('date');
          })
        });
         $(function () {
          $('#est_starting_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_starting_time').on('change',function(){
            $scope.itinerary.est_starting_time = $('#est_starting_time').data('date');
          })
        });
          $(function () {
          $('#actual_starting_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_starting_time').on('change',function(){
            $scope.itinerary.actual_starting_time = $('#actual_starting_time').data('date');
          })
        });
           $(function () {
          $('#est_completion_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_completion_time').on('change',function(){
            $scope.itinerary.est_completion_time = $('#est_completion_time').data('date');
          })
        });
            $(function () {
          $('#actual_completion_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_completion_time').on('change',function(){
            $scope.itinerary.actual_completion_time = $('#actual_completion_time').data('date');
          })
        });
        $(function () {
          $('#est_sailing_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_sailing_time').on('change',function(){
            $scope.itinerary.est_sailing_time = $('#est_sailing_time').data('date');
          })
        });
        $(function () {
          $('#actual_sailing_time').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#actual_sailing_time').on('change',function(){
            $scope.itinerary.actual_sailing_time = $('#actual_sailing_time').data('date');
            console.log($scope.itinerary);
          })
        });



        $(function () {
          $('.event-table #est_0').datetimepicker({
              format: 'YYYY-MM-DD HH:mm:ss'
          });
          $('#est_0').on('change',function(){
            $scope.event.actual_1 = $('#est_0').data('date');
            console.log($scope.itinerary);
          })
        });
      











}])