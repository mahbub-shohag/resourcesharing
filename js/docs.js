var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);

app.controller('docsController',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
	
    $scope.veselFolderId = {};
    $scope.sharedEmail = "";
    
    if(sessionStorage.length==0){

  }else{
    if(JSON.parse(sessionStorage.selectedRecord)){
      $('.dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
      $scope.veselFolderId = JSON.parse(sessionStorage.selectedRecord);
  }
  }
  
  
   if(sessionStorage.length==0){

  }else{
     if(sessionStorage.selectedVessel){
        var selectedVessel = JSON.parse(sessionStorage.selectedVessel);
        $scope.current_folder = {'id':selectedVessel.id,'parent_id':selectedVessel.parent_id,'name':selectedVessel.name};
        console.log(JSON.parse(sessionStorage.selectedVessel));
    }
  }
    
    
   
    $scope.name = "Mahbub Alam";
    
    $scope.current_directory_link = "itinery";
	$scope.current_directory = "itinery/"+$scope.veselFolderId.vessel_id+"/";
    //$scope.current_folder = {'id':1,'parent_id':0,'name':'docs'};
    $scope.directory_queue = [$scope.current_folder];
	$scope.directory = "default_directory";
	$scope.parent_id = $scope.current_folder.id;
    $scope.image_files = [];
    $scope.others = [];
    $scope.items = [];
    $scope.pdfs = [];
    $scope.folder_path = "";
    $scope.sharedItem = {};
	$scope.initialize = function(){
		$scope.files = [];
	}
	$scope.initialize();
    $('#image').change(function() {
      console.log('********')  
      $scope.uploadFile();
    });
    $scope.selected_directory = function(){
        $scope.folder_path = "";
        angular.forEach($scope.directory_queue, function(value, key){
           $scope.folder_path = $scope.folder_path + value.name + "/";
        });
        console.log($scope.directory_queue);
    }
    $scope.distributeItems = function(){
        $scope.folders = $filter('filter')($scope.items,{'type_name':'folder'},true);
        $scope.jpgs = $filter('filter')($scope.items,{'type_name':'jpg'},true);
        $scope.jpegs = $filter('filter')($scope.items,{'type_name':'jpeg'},true);
        $scope.pngs = $filter('filter')($scope.items,{'type_name':'png'},true);
        $scope.image_files.push.apply($scope.image_files,$scope.jpgs);
        $scope.image_files.push.apply($scope.image_files,$scope.jpegs);
        $scope.image_files.push.apply($scope.image_files,$scope.pngs);
        $scope.pdfs = $filter('filter')($scope.items,{'type_name':'pdf'},true);
        $scope.others = $scope.items.filter( function( el ) {
          return $scope.folders.indexOf( el ) < 0;
        } );
        $scope.others = $scope.others.filter(function(el){
            return $scope.image_files.indexOf(el)<0;
        });
        $scope.others = $scope.others.filter(function(el){
            return $scope.pdfs.indexOf(el)<0;
        })
        console.log($scope.pdfs);
        console.log($scope.others,'**********');
    }
    $scope.clearItems = function(){
        $scope.folders = [];
        $scope.jpegs = [];
        $scope.jpegs = [];
        $scope.pngs = [];
        $scope.pdfs = [];
        $scope.image_files = [];
    }

    $scope.getUserList = function(){
        $http({
            url   :"docs/getlistByTable",
            method:"POST",
            data : $httpParamSerializerJQLike({'table' : 'users'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.users = response.data;
            console.log($scope.users);
        });
    }

    $scope.getUserList();
    $scope.shareold = function(user){
        $scope.sharedItems = $filter('filter')($scope.items,{checked:true},true);
        if($scope.sharedItems.length<1){
            $scope.alertMessage = "Sorry no file is selected";
            $('#alertMessage').show().delay(2500).fadeOut();
        }else{
            $scope.shareItem(user);
        }
    }


    $scope.findUserAndShare = function(sharedEmail){
        $http({
            url :"docs/findEmailExists",
            method : "POST",
            data : $httpParamSerializerJQLike({'email' : sharedEmail}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            $scope.sharedUserInfo = response.data;
            if($scope.sharedUserInfo.length==0){
                $scope.messageclass = "danger";
                $scope.message      = "Sorry this user is not exists in the system";
                $('#emailExistMessage').show().delay(2500).fadeOut();
            }else{
                $scope.shareItem($scope.sharedUserInfo[0]);            
            }
        })
    }

    $scope.share = function(itemDetail){
        $scope.sharedItems = [];
        $scope.sharedItems.push(itemDetail);
        console.log($scope.sharedItems)
    }
    $scope.shareItem = function(user){
        var data = {'user_id':user.id,'docs':$scope.sharedItems};
        console.log('Shared User ',user)
        $http({
            url   :"docs/shareItemUserNew",
            method:"POST",
            data : $httpParamSerializerJQLike({'table' : 'share_docs_user','data':data}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            if(response.data==0){
                $scope.message = response.data + " Item was shared.May be Item alredy shared";
                $scope.messageclass = "danger";
                $('#emailExistMessage').show().delay(2500).fadeOut();
            }else{
                $scope.message = response.data + " Items Shared Successfully";
                $scope.messageclass = "success";
                $("#emailExistMessage").show().delay(1200).fadeOut();    
            }
            
        });
    }

	$scope.getRootFiles = function(){
        $http({
            url   :"docs/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({'parent_id' : $scope.parent_id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.items = response.data;
            //$scope.clearItems();
            //$scope.distributeItems();
        });
    }
    $scope.getRootFiles();

    $scope.getSharedFileList = function(){
        $http({
            url   :"docs/getSharedFileList",
            method:"POST",
            data : $httpParamSerializerJQLike(), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.SharedItems = response.data;
            console.log($scope.SharedItems);
        });   
    }
    $scope.getSharedFileList();

    $scope.makeDirectory = function(){
    	$scope.selected_directory();
        var folder_path = $scope.current_directory+"/"+$scope.directory;
    	console.log(folder_path);
        $scope.folder_path = $scope.folder_path+$scope.directory+"/";
        var data = {'folder_path' : $scope.folder_path,'directory_name':$scope.directory,'parent_id':$scope.parent_id, 'created_by':user_id};
    	$http({
            url   :"docs/makeDirectory",
            method:"POST",
            data : $httpParamSerializerJQLike(data), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            $scope.items = response.data;
        });

    }

    $scope.uploadFile = function(){
    	var info = {};
    	info.parent_id = $scope.parent_id;
        $scope.selected_directory();
        info.directory = $scope.folder_path;
        info.created_by = user_id;
        console.log(info.directory,'***');
    	var fd = new FormData();
        var files = document.getElementById('image').files[0];
		fd.append('file',files);
		fd.append('info',JSON.stringify(info));
		$http({
            url   :"docs/uploadFile",
            method:"POST",
            data : fd, 
            headers: {
                'Content-Type': undefined
            }
        }).then(function(response){
            console.log(response);
            $scope.items = response.data;
            //$scope.clearItems();
            //$scope.distributeItems();
        });
    }

    $scope.findChilds = function(root){
            console.log(root);
            console.log($scope.current_folder.id);
            if(root.id === $scope.current_folder.id){
                if(root.id===1){
                    $scope.current_folder = root;    
                    $scope.parent_id = root.id;
                    console.log($scope.parent_id);
                    $scope.getRootFiles();    
                }
            }else{
                console.log($scope.current_folder,'******')
                $scope.current_folder = root;    
                $scope.parent_id = root.id;
                $scope.current_directory = root.name;
                var index = $scope.directory_queue.indexOf(root);
                var queue_length = $scope.directory_queue.length;

                if(index!=-1){
                    $scope.directory_queue.splice(index+1,queue_length-1);    
                }else{
                    $scope.directory_queue.push(root);
                    console.log($scope.directory_queue);
                }
                console.log(index);
                $scope.clearItems();
                $scope.getRootFiles();
            }
        
    }

    $scope.delete = function(){
        console.log($scope.items);
        $scope.sharedItems = $filter('filter')($scope.items,{checked:true},true);
        console.log($scope.items);
        if($scope.sharedItems.length<1){
            $scope.alertMessage = "Sorry no file is selected";
            $('#alertMessage').show().delay(2500).fadeOut();
        }else{
            $http({
            url   :"docs/delete",
            method:"POST",
            data : $httpParamSerializerJQLike({'docs':$scope.sharedItems}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            $scope.items = response.data;
        });
        }
    }


    $scope.copy = function(){
        console.log($scope.items);
        $scope.copiedItems = $filter('filter')($scope.items,{checked:true},true);
        if($scope.copiedItems.length<1){
            $scope.alertMessage = "Sorry no file is selected";
            $('#alertMessage').show().delay(2500).fadeOut();
        }
    }

    $scope.move = function(){
        console.log($scope.items);
        $scope.movedItems = $filter('filter')($scope.items,{checked:true},true);
        if($scope.movedItems.length<1){
            $scope.alertMessage = "Sorry no file is selected";
            $('#alertMessage').show().delay(2500).fadeOut();
        }
    }

    $scope.cutPaste = function(){
        $scope.movedItemIds = [];
        angular.forEach($scope.movedItems,function(key,val){
            key.parent_id = $scope.parent_id;
            $scope.movedItemIds.push(key.id);
            delete key.checked;
            delete key.id;
            delete key.$$hashKey;
            
        })
        //Delete Items First
        $http({
            url : "docs/softDeleteItems",
            method : "POST",
            data : $httpParamSerializerJQLike({'docIds': $scope.movedItemIds}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
        });

        $http({
            url   :"docs/paste",
            method:"POST",
            data : $httpParamSerializerJQLike({'docs':$scope.movedItems}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            $scope.movedItems = [];
        });
    }

    $scope.paste = function(){
        angular.forEach($scope.copiedItems,function(key,val){
            key.parent_id = $scope.parent_id;
            delete key.checked;
            delete key.id;
            delete key.$$hashKey;
        })

        $http({
            url   :"docs/paste",
            method:"POST",
            data : $httpParamSerializerJQLike({'docs':$scope.copiedItems}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response.data);
            $scope.copiedItems = [];
        });
    }

}])