var app = angular.module('travellerApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);
app.directive('ckEditor', function() {
  return {
    require: '?ngModel',
    link: function(scope, elm, attr, ngModel) {
      var ck = CKEDITOR.replace(elm[0]);

      if (!ngModel) return;

      ck.on('pasteState', function() {
        scope.$apply(function() {
          ngModel.$setViewValue(ck.getData());
        });
      });

      ngModel.$render = function(value) {
        ck.setData(ngModel.$viewValue);
      };
    }
  };
});



app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

/*Number to word convert starts*/

app.filter('words', function() {
  function isInteger(x) {
        return x % 1 === 0;
    }

  
  return function(value) {
    if (value && isInteger(value))
      return  toWords(value);
    
    return value;
  };

});


var th = ['','thousand','million', 'billion','trillion'];
var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine']; 
var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen'];
var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety']; 


function toWords(s)
{  
    s = s.toString(); 
    s = s.replace(/[\, ]/g,''); 
    if (s != parseFloat(s)) return 'not a number'; 
    var x = s.indexOf('.'); 
    if (x == -1) x = s.length; 
    if (x > 15) return 'too big'; 
    var n = s.split(''); 
    var str = ''; 
    var sk = 0; 
    for (var i=0; i < x; i++) 
    {
        if ((x-i)%3==2) 
        {
            if (n[i] == '1') 
            {
                str += tn[Number(n[i+1])] + ' '; 
                i++; 
                sk=1;
            }
            else if (n[i]!=0) 
            {
                str += tw[n[i]-2] + ' ';
                sk=1;
            }
        }
        else if (n[i]!=0) 
        {
            str += dg[n[i]] +' '; 
            if ((x-i)%3==0) str += 'hundred ';
            sk=1;
        }


        if ((x-i)%3==1)
        {
            if (sk) str += th[(x-i-1)/3] + ' ';
            sk=0;
        }
    }
    if (x != s.length)
    {
        var y = s.length; 
        str += 'point '; 
        for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';
    }
    return str.replace(/\s+/g,' ');
}

window.toWords = toWords;

/*Number to word convert ends*/


app.config(function($routeProvider){
	$routeProvider
	.when(
		"/package",{
			templateUrl:"modules/package/view.html"		
	})
	.when(
		"/package/create",{
			templateUrl:"modules/package/create.html"		
	})
	.when(
		"/package/edit",{
			templateUrl:"modules/package/edit.html"		
	})
	.when(
		"/customer",{
			templateUrl:"modules/customer/view.html"
    }).when(
      "/invoice",{
        templateUrl:"modules/invoice/view.html"
      })
    .when(
      "/commun_page",{
        templateUrl:"modules/commun_page/view.html"
      }).when(
        "/message",{
          templateUrl:"modules/message/view.html"
        }).when(
          "/gallery",{
            templateUrl:"modules/gallery/view.html"
          }).when(
            "/income",{
              templateUrl:"modules/income/view.html"
            }).when(
              "/expense",{
                templateUrl:"modules/expense/view.html"
              }).when(
                "/accounts_head",{
                  templateUrl:"modules/accounts_head/view.html"
                }).when(
          "/preregistration",{
            templateUrl:"modules/pre_registration/view.html"
          });
});

app.controller('travellerController',function($scope){
	
});