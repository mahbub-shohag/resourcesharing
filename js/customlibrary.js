"use strict"

var rmn = function(el){
    return document.querySelector(el);
};

var log = function(str){
    console.log(str);
}

var rmnInit = function(r, t, h, hp, l){

	r.initMessage = function(){
		r.msgs = [{type: "", val: ""}];	
	};

	r.initModal = function(){
        r.modalStatus = {show : false, showBody: false};
    };

    r.hideModal = function(v){        
        v.showBody = false;
        t(function() {
            v.show = false;            
        }, 299);
    };

    
    r.showModal = function(v){
        v.show = true;
        t(function() {
            v.showBody = true;
        }, 11);
    };	

   	r.clearMessage = function(msg){
        if(msg.type !='success'){
   			r.msgs.splice(r.msgs.indexOf(msg), 1);
   		}
   	};

    r.showMessage = function(msg, type){
        var msg = {type: type, val: msg};
		r.msgs.push(msg);

		if(type == 'success'){			
			t(function() {
				r.msgs.splice(r.msgs.indexOf(msg), 1);	
			}, 1888);
		}
	};

    r.focusTo = function(id){
        t(function() {
                document.getElementById(id).focus();
            }, 99);
    };

    r.focusToId = function(id,index){
        t(function() {
                var ids = id + index;
                document.getElementById(ids).focus();
            }, 99);
    };

    r.showProcessing = function(){
        r.isProcessing = true;
    };

    r.hideProcessing = function(){
        r.isProcessing = false;
    };

    r.initMessage();    
    r.initModal();
    r.hideProcessing();
};

// r.$watch(function() { 
    //   return l.path(); 
    // },
    // function(a){  
    //     h.get(setup.serverPath + "/userInfo").success(function(response){
    //         if(response == "anonymous" && a != "/login"){
    //             r.loggedUser = null;
    //             window.location.href = setup.webURL + "/#/login";            
    //         } else{
    //             r.loggedUser = response;
    //         }
    //     });
    // });
