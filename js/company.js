$(document).ready(function () {
   initAll();
})

var initAll = function () {
    $('#isSubsidiary').on('click', function () {
        if($(this).prop("checked") == true){
            $('#parentId').attr('required', '');
        }
        else{
            $('#parentId').removeAttr('required');
        }
    });

    $('.companyForm').on('submit', function (event) {
        debugger;
        event.preventDefault();
        var thisForm = $(this);
        $.ajax({
            method:"POST",
            url: "../company/save",
            data:$(this).serialize(),
            success:function (response) {
                if(response.txt == "Company created successfully"){
                    alert('Company Created');
                    thisForm[0].reset();
                    $('#companyId').append("<option value='"+response.id+"'>"+response.caption+"</option>");
                    $('#companyId').val(response.id);
                    $('#companyModal .close').click();
                }
            }
        });

    });
}