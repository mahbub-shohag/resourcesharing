var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);

app.controller('profileController',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
	
    $scope.veselFolderId = {};
    $scope.alertMessage  = "";
    
    if(sessionStorage.length==0){

  }else{
    if(JSON.parse(sessionStorage.selectedRecord)){
      $('.dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
      $scope.veselFolderId = JSON.parse(sessionStorage.selectedRecord);
  }
  }
  
  
   if(sessionStorage.length==0){

  }else{
     if(sessionStorage.selectedVessel){
        var selectedVessel = JSON.parse(sessionStorage.selectedVessel);
        $scope.current_folder = {'id':selectedVessel.id,'parent_id':selectedVessel.parent_id,'name':selectedVessel.name};
        console.log(JSON.parse(sessionStorage.selectedVessel));
    }
  }

  $scope.profile = {};
  $scope.password = {};
  $scope.password_matched = false;

  $scope.getProfileInfo = function(){
        $http({
        url   :"getProfileInfo",
        method:"POST",
        data : $httpParamSerializerJQLike(), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        $scope.profile = response.data;
        if($scope.profile.status==1){
          $scope.profile.status = "Active";
        }else{
          $scope.profile.status = "Inactive";
        }
        console.log($scope.profile,'repositories');
    });
  }
  $scope.getProfileInfo();

  $scope.updateProfileBasic = function(){
    console.log($scope.profile);
    if($scope.profile.status== "Active"){
          $scope.profile.status = 1;
        }else{
          $scope.profile.status = 0;
        }
    $http({
        url   :"updateProfile",
        method:"POST",
        data : $httpParamSerializerJQLike({'data':$scope.profile}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
      console.log(response.data);
        $scope.profile = response.data;
        if($scope.profile.status==1){
          $scope.profile.status = "Active";
        }else{
          $scope.profile.status = "Inactive";
        }
        if($scope.profile){
          $scope.alertClass = "success";
          $scope.alertMessage = "Profile Updated Successfully";
          $('#alertMessage').show().delay(3000).fadeOut();
        }else{
           $scope.alertClass = "danger";
           $scope.alertMessage = "Sorry Email Or Password was not matched";
          $('#alertMessage').show().delay(3000).fadeOut();
        }
        console.log($scope.profile,'repositories');
    });
  }

  $scope.checkPassMatch = function(){
    if($scope.password.new == $scope.password.confirm){
      $scope.password_matched = true;
    }
    console.log($scope.password.new)
    console.log($scope.password.confirm)
  }

   $scope.passwordUpdate = function(){
    console.log($scope.password);
    console.log($scope.profile);
    $http({
        url   :"passwordUpdate",
        method:"POST",
        data : $httpParamSerializerJQLike({'profile':$scope.profile,'password':$scope.password}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        
        if(response.data == 1){
          $scope.alertClass = "success";
          $scope.alertMessage = "Password Updated Successfully";
          $('#alertMessage').show().delay(3000).fadeOut();
        }else{
          $scope.alertClass = "danger";
          $scope.alertMessage = "Sorry Email Or Password was not matched";
          $('#alertMessage').show().delay(3000).fadeOut();
        }
    });
  }
  
  $scope.getEndorsersById = function(){
     $http({
        url   :"getEndorsersById",
        method:"POST",
        data : $httpParamSerializerJQLike(), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        console.log(response.data);
        $scope.endorsers = response.data;
    });
  }

  $scope.getEndorsersById();
 
  $scope.terminateEndorser = function(endorserId){
    $http({
        url   :"terminateEndorser",
        method:"POST",
        data : $httpParamSerializerJQLike({'endorserId':endorserId}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        if(response.data==1){
          $scope.alertClass = "success";
          $scope.alertMessage = "Endorsement Termination Successfull!";
          $scope.getEndorsersById();
          $('#alertMessage').show().delay(3000).fadeOut();
        }else{
          $scope.alertClass = "danger";
          $scope.alertMessage = "Endorsement Termination failed!";
          $('#alertMessage').show().delay(3000).fadeOut();

        }
    });
  }

}])