var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);
app.controller('calculator',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
	console.log('hello');
  $scope.dropdown_for = "";
	$scope.test = 'Hello';
	$scope.ship = {};
  $scope.deductions = [];
  $scope.activities = [];
  $scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appttime:'2018-07-22'};
  $scope.deductions.push($scope.deduction);
  $scope.deduction_descriptions = [];

  $scope.saveShipInfo = function(){
    console.log($scope.ship);
       $http({
            url: "calculator/savepdf",
            method: 'POST',
            data: $httpParamSerializerJQLike({info : $scope.ship}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response.data);
                  $scope.shipinfoId = response.data;                  
      });
  }

	$scope.makePdfFile = function () {
		console.log($scope.ship);
       $http({
            url: "calculator/infopdf",
            method: 'POST',
            data: $httpParamSerializerJQLike({info : $scope.ship}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);                  
      });
	}
  $scope.find = function(item){
    $scope.table_cargo = item;
    console.log(item);
    console.log($scope.ship.cargo)
  }
	/**/
    $scope.findactivity = function(){
      console.log($scope.ship.activity)
    }

    $scope.deleteObject = function (obj, arrayName) {
      console.log(arrayName);
      var indexOfDeletedItem = $scope[arrayName].indexOf(obj);
      $scope[arrayName].splice(indexOfDeletedItem, 1); 
    }


     $scope.addObject = function(position, arrayName){
      console.log(position);
       //$scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appt-time:''};
       $scope.deduction = {};
       $scope[arrayName].push($scope.deduction); 
       console.log($scope[arrayName]);          
     }
    
    $scope.allDropDownList = function(){
        $http({
        url   :"calculator/getlists",
        method:"POST",
        data : $httpParamSerializerJQLike({table : 'custom_dropdown_data'}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        console.log(response);
        var dropdowns = response.data;
        $scope.cargoes = $filter('filter')(dropdowns, {dropdown_for: 'cargo'}, true);
        console.log($scope.cargoes);
        $scope.activities = $filter('filter')(dropdowns, {dropdown_for: 'activity'}, true);
        console.log($scope.activities);
    });
    }
    $scope.allDropDownList();
    $scope.getDeductionDescriptionList = function(){
    $http({
        url   :"calculator/getlists",
        method:"POST",
        data : $httpParamSerializerJQLike({table : 'deduction_description'}), 
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function(response){
        console.log(response);
        $scope.deduction_descriptions = response.data;
        console.log($scope.deduction_descriptions)
    });
    }
    $scope.getDeductionDescriptionList();

    $scope.getShipList = function(){
        $http({
            url   :"calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ships = response.data;
            $scope.destinations = $scope.ships.filter(x => x.destination !== "");
            console.log($scope.destinations)
        });
    }
    $scope.getShipList();

    $scope.getActivityDropDownlists = function() {
    	$http({
            url   :"calculator/getCustomDropDownlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'custom_dropdown_data'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.activities = response.data;
            console.log($scope.activities)
        });
    }
    //$scope.getActivityDropDownlists();
    $scope.saveActivity = function(dropdown_text) {
    	console.log(dropdown_text);
    	
    	 $http({
            url: "calculator/save_activity",
            method: 'POST',
            data: $httpParamSerializerJQLike({dropdown_text:dropdown_text, dropdown_for : $scope.dropdown_for}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);
                  if($scope.dropdown_for == 'activity'){
                  	$scope.activities = response.data;	
                    $scope.ship.activity = response.data[0];
                  }
                  
      });
    	console.log($scope.activities);
    }

    $scope.save_dropdown = function(dropdown_text) {
      console.log(dropdown_text);
      
       $http({
            url: "calculator/save_dropdown",
            method: 'POST',
            data: $httpParamSerializerJQLike({dropdown_text:dropdown_text, dropdown_for : $scope.dropdown_for}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
                  console.log(response);
                  if($scope.dropdown_for == 'activity'){
                    $scope.activities = response.data;  
                    $scope.ship.activity = response.data[0];
                  }else if($scope.dropdown_for == 'cargo'){
                    $scope.cargoes = response.data;
                  }else{

                  }
                  
      });
      console.log($scope.cargoes);
    }

    $scope.selectShipName = function() {
      console.log($scope.ship.ship_name);
      $scope.ship.loa = $scope.ship.ship_name.loa;
      $scope.ship.imo = $scope.ship.ship_name.imo;
      $scope.ship.dwt = $scope.ship.ship_name.dwt;
    }

    $scope.addNewDropdown = function(dropdown_for) {
			$scope.dropdown_for = dropdown_for;
      console.log($scope.dropdown_for);
    }

    /* Dropdown Starts */

    var vm = this;

  vm.disabled = undefined;
  vm.searchEnabled = undefined;

  vm.setInputFocus = function (){
    $scope.$broadcast('UiSelectDemo1');
  };

  vm.enable = function() {
    vm.disabled = false;
  };

  vm.disable = function() {
    vm.disabled = true;
  };

  vm.enableSearch = function() {
    vm.searchEnabled = true;
  };

  vm.disableSearch = function() {
    vm.searchEnabled = false;
  };

  vm.clear = function() {
    vm.person.selected = undefined;
    vm.address.selected = undefined;
    vm.country.selected = undefined;
  };

  vm.someGroupFn = function (item){

    if (item.name[0] >= 'A' && item.name[0] <= 'M')
        return 'From A - M';

    if (item.name[0] >= 'N' && item.name[0] <= 'Z')
        return 'From N - Z';

  };

  vm.firstLetterGroupFn = function (item){
      return item.name[0];
  };

  vm.reverseOrderFilterFn = function(groups) {
    return groups.reverse();
  };


  vm.counter = 0;
  vm.onSelectCallback = function (item, model){
    vm.counter++;
    vm.eventResult = {item: item, model: model};
  };

  vm.removed = function (item, model) {
    vm.lastRemoved = {
        item: item,
        model: model
    };
  };

  vm.tagTransform = function (newTag) {
    var item = {
        name: newTag,
        email: newTag.toLowerCase()+'@email.com',
        age: 'unknown',
        country: 'unknown'
    };

    return item;
  };



  vm.appendToBodyDemo = {
    remainingToggleTime: 0,
    present: true,
    startToggleTimer: function() {
      var scope = vm.appendToBodyDemo;
      var promise = $interval(function() {
        if (scope.remainingTime < 1000) {
          $interval.cancel(promise);
          scope.present = !scope.present;
          scope.remainingTime = 0;
        } else {
          scope.remainingTime -= 1000;
        }
      }, 1000);
      scope.remainingTime = 3000;
    }
  };

  vm.address = {};
  vm.refreshAddresses = function(address) {
    var params = {address: address, sensor: false};
    return $http.get(
      'http://maps.googleapis.com/maps/api/geocode/json',
      {params: params}
    ).then(function(response) {
      vm.addresses = response.data.results;
    });
  };

  vm.addPerson = function(item, model){
    if(item.hasOwnProperty('isTag')) {
      delete item.isTag;
      vm.people.push(item);
    }
  }




    /* Dropdown Ends*/



	/**/


}])