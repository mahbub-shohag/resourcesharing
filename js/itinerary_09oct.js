var app = angular.module('myApp',['ngRoute','ngAnimate','ngSanitize','ui.bootstrap','ui.select']);

app.controller('itinerary',["$scope","$http","$timeout","$filter", "$httpParamSerializerJQLike",function($scope, $http,$timeout,$filter, $httpParamSerializerJQLike){
  $scope.itineraries = [];
  $scope.itinerary = {};
  $scope.search = {};
  $scope.itn = {};
  if(JSON.parse(sessionStorage.selectedRecord)){
      $('.dashboard_nav_ul').removeClass('disable');
      $('#activation_message').addClass('display_none');
      console.log(JSON.parse(sessionStorage.selectedRecord));
      $scope.selectedRecord = JSON.parse(sessionStorage.selectedRecord);
      console.log($scope.selectedRecord.vessel_id)
  }

  $scope.showData = function(){
    console.log($scope.itinerary);
    console.log($scope.itn);
  }
  $scope.saveItinerary = function() {
      console.log($scope.itinerary);
      $http({
            url: "/chanul/calculator/save",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'itinerary','data':$scope.itinerary}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.itineraries = response.data;
             if($scope.itineraries){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }



  $scope.deleteObject = function (obj, arrayName) {
    console.log(arrayName);
    var indexOfDeletedItem = $scope[arrayName].indexOf(obj);
    $scope[arrayName].splice(indexOfDeletedItem, 1); 
  }

     $scope.addObject = function(position, arrayName){
      console.log(position);
       //$scope.deduction = {date:'2018-07-22',from:'',to:'',description:'',appt-time:''};
       $scope.deduction = {};
       $scope[arrayName].push($scope.deduction); 
       console.log($scope[arrayName]);          
     }

    $scope.getList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'ship_info'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.itineraries = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    $scope.getList();

    $scope.getPortList = function(){
        $http({
            url   :"/chanul/calculator/getlists",
            method:"POST",
            data : $httpParamSerializerJQLike({table : 'port'}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            console.log(response);
            $scope.ports = response.data;
            //$scope.ships = $scope.ships.filter(x => x.destination !== "");
            //console.log($scope.ships,'****ships***')
        });
    }
    $scope.getPortList();



    $scope.savePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/save",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

     $scope.addShip = function(){
      $scope.ship = {};
     }

    
    
     $scope.editPort = function(port){
        delete port.$$hashKey;
        $scope.port = port;
        $scope.port.call_sign = Number(port.call_sign);
     }


     $scope.updatePort = function() {
      console.log($scope.port);
      $http({
            url: "/chanul/calculator/update",
            method: 'POST',
            data: $httpParamSerializerJQLike({'table':'port','data':$scope.port}),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
          }).then(function(response) {    
             $scope.ports = response.data;
             if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
      });
    }

    $scope.deletePort = function(table,id){
      $http({
            url   :"/chanul/calculator/delete",
            method:"POST",
            data : $httpParamSerializerJQLike({table : table,id : id}), 
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function(response){
            $scope.ports = response.data;
            if($scope.ports){
              $("#success_message").show().delay(1200).fadeOut();
            }else{
              $("#error_message").show().delay(1200).fadeOut();
            }
        });
    }


    $(function () {
                $('#datetimepicker1').datetimepicker();
              });

    $("#datetimepicker1").on("dp.blur", function() {

        $scope.itinerary.est_arrival_time = $("#datetimepicker1").val();
        console.log($scope.itinerary.est_arrival_time);

    });
            $(function () {
                $('#datetimepicker2').datetimepicker();
              });
            $(function () {
                $('#datetimepicker3').datetimepicker();
              });
            $(function () {
                $('#datetimepicker4').datetimepicker();
              });
            $(function () {
                $('#datetimepicker5').datetimepicker();
              });
            $(function () {
                $('#datetimepicker6').datetimepicker();
              });$(function () {
                $('#datetimepicker7').datetimepicker();
              });$(function () {
                $('#datetimepicker8').datetimepicker();
              });
              $(function () {
                $('#datetimepicker9').datetimepicker();
              });
              $(function () {
                $('#datetimepicker10').datetimepicker();
              });
              $(function () {
                $('#datetimepicker11').datetimepicker();
              });
              $(function () {
                $('#datetimepicker12').datetimepicker();
              });












}])